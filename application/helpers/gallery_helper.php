<?php

function getGalleryAsSlider($id = 0) {
    $ci = & get_instance();
    $ci->db->where('user_id', $id);
    $photos = $ci->db->get('gallery')->result();
    if ($photos) {
        $thumbs = '<div class="carousel-inner">';
        $sl = 0;
        foreach ($photos as $photo) {
            ++$sl;
            $active = ($sl == 1) ? 'active' : '';
            $thumbs .= "<div class=\"item {$active}\">";
            $thumbs .= "<img src=\"{$photo->photo}\"/>";
            $thumbs .= "<div class=\"slide-text\">";
//            $thumbs .= "<h1>" .$photo->title. "</h1>";
//            $thumbs .= "<p>" .$photo->description. "</p>";
            $thumbs .= "</div>";
            $thumbs .= '</div>';
        }
        $thumbs .= '</div>';

        $sl = 0;
        $slider = '<ol class="carousel-indicators">';
        foreach ($photos as $photo) {
            $slider .= '<li data-target="#myCarousel" data-slide-to="' . $sl++ . '">';
            $slider .= "<img class=\"img-responsive\" src=\"{$photo->photo}\"/>";
            $slider .= '</li>';
        }
        $slider .= '</ol>';


        $output = '<div class="listing-slide">';
        $output .= '<div id="myCarousel" class="carousel slide">';
        $output .= $thumbs;
        $output .= $slider;
        $output .= '<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                    <i class="glyphicon glyphicon-chevron-left">
                    </i>
                </a>
                <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                    <i class="glyphicon glyphicon-chevron-right">
                    </i>
                </a>';
        $output .= '</div></div>';
        return $output;
    }
}

function getGalleryAlbumSlide(){
    $ci = &get_instance();
    $albums = $ci->db->where('parent_id', 0)->where('status', 'Active')->get('gallery_albums')->result();
    if($albums) {
        $html = '';        
        $html .= '<div id="albumSlide" class="owl-carousel owl-theme">';
        foreach($albums as $album){
            $html .= '<div class="item">';
            $html .= '<a class="" 
                         href="photo-gallery?album_id=' . $album->id. '" >';
            $html .= getAlbumPhoto($album->thumb);
            $html .= '<p class="text-center">' . $album->name . '</p>';;
            $html .= '</a>';
            $html .= '</div>';
        }        
        $html .= '</div>';
        return $html;
    } else {
        return null;
    }    
}

function getGalleryAlbum($album_id = 0){
    $ci =& get_instance();
    $albums = $ci->db->where('parent_id', $album_id)->where('status', 'Active')->get('gallery_albums')->result();
    if($albums) {
        $html = '';        
        $html .= '<div class="album-list">';
        $html .= '<div class="row">';
        foreach($albums as $album){
            $html .= '<div class="col-md-3 col-sm-3">';
            $html .= '<a class="" 
                         href="photo-gallery?album_id=' . $album->id. '" >';
            $html .= getAlbumPhoto($album->thumb);
            $html .= '<p class="text-center">' . $album->name . '</p>';;
            $html .= '</a>';
            $html .= '</div>';
        }        
        $html .= '</div>';
        $html .= '</div>';
        return $html;
    } else {
        return null;
    }    
}
function getGalleryHome($limit=6){
    $CI = &get_instance();    
    $CI->db->order_by('id', 'DESC');
    $CI->db->limit($limit);
    $results = $CI->db->get('gallery')->result();

    if(!$results) { 
        return '<div class="alert alert-warning" role="alert">No Photo Found!</div>';        
    } 
    
    
    $table = '';
    $table .= '<link rel="stylesheet" href="'. base_url('assets/lib/plugins/lightbox/css/lightbox.min.css') .'">';       
    $table .= '<ul class="photoGallery clearfix" id="album">';

    foreach ($results as $result) {
        $img = getGalleryPhoto($result->photo);
        
        $table .= '<li class="col-md-2 col-sm-4 col-xs-6">';
        $table .= '<a class="example-image-link" 
                         href="'.$result->photo.'" 
                         data-lightbox="example-set" 
                         data-title="'.$result->description.'">';
//        $table .= "<img src=\"{$img}\" class=\"img-responsive\"/>";
        $table .= $img;
//        $table .= '<p>'. $result->title.'</p>';
        $table .= '</a>';
        //$link = getPhoto($result->photo);
        $table .= '</li>' . "\r\n";
    }

    $table .= '</ul>' . "\r\n";
    $table .='<div class="text-center">';
    
    $table .= '</div>';
    
    return $table;
}
function getGallery(){
    $CI = &get_instance();
    $page       = (int) $CI->input->get('p');
    $target     = build_pagination_url('photo-gallery','p', true);
    $total      = $CI->db->where('type','Photo')->count_all_results('gallery');
    $limit      = 12;

    $start      = startPointOfPagination($limit,$page);


    $CI->db->limit($limit,$start);
    $CI->db->order_by('id', 'DESC');
    $CI->db->where('type','Photo');
    $results = $CI->db->get('gallery')->result();

    if(!$results) {
        return '<div class="alert alert-warning" role="alert">No Photo Found!</div>';
    }

    $table = '';
    $table .= '<link rel="stylesheet" href="'. base_url('assets/lib/plugins/lightbox/css/lightbox.min.css') .'">';
    $table .= '<div class="photoGallery">';

    foreach ($results as $result) {
        $img = getGalleryPhoto($result->photo);

        $table .= '<div class="col-md-3 col-sm-4 col-xs-6">';
        $table .= '<a class="example-image-link" 
                         href="'.$result->photo.'" 
                         data-lightbox="example-set" 
                         data-title="'.$result->description.'">';
        $table .= '<div class="gallery-thumb">'.$img.'</div>';
        $table .= '</a>';
        $table .= '</div>' . "\r\n";
    }

    $table .= '</div>' . "\r\n";
    $table .='<div class="text-center">';
    $table .= getPaginator($total, $page, $target, $limit);
    $table .= '</div>';

    return $table;
}

function getAlbumPhoto($photo) {
    return resizePhotoWithTimThumb( $photo );
//    $filename   = dirname(BASEPATH) . '/' . $photo;
//    $photo_url  = base_url($photo);
//    $no_photo   = base_url('uploads/no-photo.jpg');
//    if ($photo && file_exists($filename)) {
//        return "<img src=\"timthumb.php?src={$photo_url}&w=480&h=335&zc=1\" class=\"img-responsive\">";
//    } else {
//        return "<img src=\"timthumb.php?src={$no_photo}&w=480&h=335&zc=1\" class=\"img-responsive\">";
//    }
}

function getGalleryPhoto($photo) {    
    return resizePhotoWithTimThumb( $photo );
}

function resizePhotoWithTimThumb($photo_src, $w = 480,  $h = 335 ) {    
    
    //$photo      = str_replace( site_url() , '', $photo_src);
    $filename   = dirname(BASEPATH) . '/' . $photo_src;
    $photo_url  = site_url($photo_src);
    if (empty($photo_src) && !file_exists($filename)) {
        $photo_url = base_url('uploads/no-photo.jpg');;
    } 
    
    return "<img src=\"timthumb.php?src={$photo_url}&w={$w}&h={$h}&zc=1\" class=\"img-responsive\">";
}
