<?php

defined('BASEPATH') OR exit('No direct script access allowed');

function getShortContent($long_text = '', $show = 100) {
    $filtered_text = strip_tags($long_text);
    if ($show < strlen($filtered_text)) {
        return substr($filtered_text, 0, $show) . '...';
    } else {
        return $filtered_text;
    }
}

function dd($data, $file = array()) {
    echo '<pre>';
    print_r($data);
    if ($file) {
        print_r($file);
    }
    echo '</pre>';
    exit;
}

function pp($data) {
    echo '<pre>';
    print_r($data);
    echo '</pre>';
    exit();
}

function getLoginUserData($key = '') {
    //key: user_id, user_mail, role_id, name, photo
    $data 	=& get_instance();
    $prefix = $data->config->item('cookie_prefix');     
	$global = json_decode(base64_decode($data->input->cookie("{$prefix}login_data", false)));
    return isset($global->$key) ? $global->$key : null;
}

function numericDropDown($i = 0, $end = 12, $incr = 1, $selected = 0) {
    $option = '';
    for ($i; $i <= $end; $i += $incr) {
        $option .= '<option value="' . sprintf('%02d', $i) . '"';
        $option .= ( $selected == $i) ? ' selected' : '';
        $option .= '>' . sprintf('%02d', $i) . '</option>';
    }
    return $option;
}

function numericDropDown2($i = 0, $end = 12, $incr = 1, $selected = 0) {
    $option = '';
    for ($i; $i <= $end; $i += $incr) {
        $option .= '<option value="' . sprintf('%02d', $i) . '"';
        $option .= ( $selected == sprintf('%02d', $i)) ? ' selected' : '';
        $option .= '>' . sprintf('%02d', $i) . '</option>';
    }
    return $option;
}

function htmlRadio($name = 'input_radio', $selected = '', $array = ['Male' => 'Male', 'Female' => 'Female'], $attr = '') {
    $radio = '';
    $id = 0;
    // $radio .= '<div style="padding-top:8px;">';
    if (count($array)) {
        foreach ($array as $key => $value) {
            $id++;
            $radio .= '<label>';
            $radio .= '<input type="radio" name="' . $name . '" id="' . $name . '_' . $id . '" ' . $attr;
            $radio .= ( trim($selected) == $key) ? ' checked ' : '';
            $radio .= 'value="' . $key . '" />&nbsp;' . $value;
            $radio .= '&nbsp;&nbsp;&nbsp;</label>';
        }
    }
    // $radio .= '</div>';
    return $radio;
}

function selectOptions($selected = '', $array = null) {
    $options = '';
    if (count($array)) {
        foreach ($array as $key => $value) {
            $options .= '<option value="' . $key . '" ';
            $options .= ($key == $selected ) ? ' selected="selected"' : '';
            $options .= '>' . $value . '</option>';
        }
    }
    return $options;
}

function load_module_asset($module = null, $type = 'css', $script = null) {
    $file = ($type == 'css') ? 'style.css.php' : 'script.js.php';
    if ($script) {
        $file = $script;
    }

    $path = APPPATH . '/modules/' . $module . '/assets/' . $file;
    if ($module && file_exists($path)) {
        include ($path);
    }
}

function startPointOfPagination($limit = 25, $page = 0) {
    return ($page) ? ($page - 1) * $limit : 0;
}

function getPaginator($total_row = 100, $currentPage = 1, $targetpath = '#&p', $limit = 25) {
    $stages = 2;
    $page = intval($currentPage);
    $start = ($page) ? ($page - 1) * $limit : 0;

    // Initial page num setup
    $page = ($page == 0) ? 1 : $page;
    $prev = $page - 1;
    $next = $page + 1;

    $lastpage = ceil($total_row / $limit);
    $LastPagem1 = $lastpage - 1;
    $paginate = '';

    if ($lastpage > 1) {
        $paginate .= '<div class="row">';
        $paginate .= '<div class="col-md-12">';
        $paginate .= '<ul class="pagination low-margin">';
        $paginate .= '<li class="disabled"><a>Total: ' . $total_row . '</a></li>';

        // Previous
        $paginate .= ($page > 1) ? "<li><a href='$targetpath=$prev'>&lt; Pre</a></li>" : "<li class='disabled'><a> &lt; Pre</a></li>";
        // Pages
        if ($lastpage < 7 + ($stages * 2)) {
            for ($counter = 1; $counter <= $lastpage; $counter++) {
                $paginate .= ($counter == $page) ? "<li class='active'><a>$counter</a></li>" : "<li><a href='$targetpath=$counter'>$counter</a></li>";
            }
        } elseif ($lastpage > 5 + ($stages * 2)) {
            // Beginning only hide later pages
            if ($page < 1 + ($stages * 2)) {
                for ($counter = 1; $counter < 4 + ($stages * 2); $counter++) {
                    $paginate .= ($counter == $page) ? "<li class='active'><a>$counter</a></li>" : "<li><a href='$targetpath=$counter'>$counter</a></li>";
                }
                $paginate .= "<li class='disabled'><a>...</a></li>";
                $paginate .= "<li><a href='$targetpath=$LastPagem1'>$LastPagem1</a></li>";
                $paginate .= "<li><a href='$targetpath=$lastpage'>$lastpage</a></li>";
            }

            // Middle hide some front and some back
            elseif ($lastpage - ($stages * 2) > $page && $page > ($stages * 2)) {
                $paginate .= "<li><a href='$targetpath=1'>1</a></li>";
                $paginate .= "<li><a href='$targetpath=2'>2</a></li>";
                $paginate .= "<li><a>...</a></li>";
                for ($counter = $page - $stages; $counter <= $page + $stages; $counter++) {
                    $paginate .= ($counter == $page) ? "<li class='active'><a>$counter</a></li>" : "<li><a href='$targetpath=$counter'>$counter</a></li>";
                }

                $paginate .= "<li><a>...</a></li>";
                $paginate .= "<li><a href='$targetpath=$LastPagem1'>$LastPagem1</a></li>";
                $paginate .= "<li><a href='$targetpath=$lastpage'>$lastpage</a><li>";
            } else {

                // End only hide early pages
                $paginate .= "<li><a href='$targetpath=1'>1</a></li>";
                $paginate .= "<li><a href='$targetpath=2'>2</a></li>";
                $paginate .= "<li><a>...</a></li>";

                for ($counter = $lastpage - (2 + ($stages * 2)); $counter <= $lastpage; $counter++) {
                    $paginate .= ($counter == $page) ? "<li class='active'><a>$counter</a></li>" : "<li><a href='$targetpath=$counter'>$counter</a></li>";
                }
            }
        }
        // Next

        $paginate .= ($page < $counter - 1) ? "<li><a href='$targetpath=$next'>Next &gt;</a></li>" : "<li class='disabled'><a>Next &gt;</a></li>";

        $paginate .= "</ul>";
        $paginate .= "<div class='clearfix'></div>";
        $paginate .= "</div>";
        $paginate .= "</div>";
    }
    return $paginate;
}

function getAjaxPaginator($total_row = 100, $currentPage = 2, $targetpath = '#&p', $limit = 25) {
    //$page         = 2; //isset($_GET['p']) ? intval($_GET['p']) : 0;
    //$page         = intval($currentPage);	
    $stages = 1; // 0, 1, 2
    $page = intval($currentPage);
    $start = ($page) ? ($page - 1) * $limit : 0;
    // Initial page num setup
    $page = ($page == 0) ? 1 : $page;
    $prev = $page - 1;
    $next = $page + 1;
    $lastpage = ceil($total_row / $limit);
    $LastPagem1 = $lastpage - 1;
    $paginate = '';

    if ($lastpage > 1) {
        $paginate .= '<ul class="pagination">';
        $paginate .= '<li class="disabled"><a>Total: ' . $total_row . '</a></li>';
        // Previous
        $paginate .= ($page > 1) ? "<li><span onclick='getResult(\"$targetpath=$prev\");'>&lt; Pre</span></li>" : "<li class='disabled'><a> &lt; Pre</a></li>";

        // Pages	
        if ($lastpage < 7 + ($stages * 2)) {
            for ($counter = 1; $counter <= $lastpage; $counter++) {
                $paginate .= ($counter == $page) ? "<li class='active'><a>$counter</a></li>" : "<li><span onclick='getResult(\"$targetpath=$counter\");'>$counter</a></li>";
            }
        } elseif ($lastpage > 5 + ($stages * 2)) {

            // Beginning only hide later pages
            if ($page < 1 + ($stages * 2)) {
                for ($counter = 1; $counter < 4 + ($stages * 2); $counter++) {
                    $paginate .= ($counter == $page) ? "<li class='active'><a>$counter</a></li>" : "<li><span onclick='getResult(\"$targetpath=$counter\");'>$counter</a></li>";
                }
                $paginate .= "<li class='disabled'><a>...</a></li>";
                $paginate .= "<li><span onclick='getResult(\"$targetpath=$LastPagem1\");'>$LastPagem1</a></li>";
                $paginate .= "<li><span onclick='getResult(\"$targetpath=$lastpage\");'>$lastpage</a></li>";
            }
            // Middle hide some front and some back
            elseif ($lastpage - ($stages * 2) > $page && $page > ($stages * 2)) {
                $paginate .= "<li><span onclick='getResult(\"$targetpath=1\");'>1</a></li>";
                $paginate .= "<li><span onclick='getResult(\"$targetpath=2\");'>2</a></li>";
                $paginate .= "<li><a>...</a></li>";
                for ($counter = $page - $stages; $counter <= $page + $stages; $counter++) {
                    $paginate .= ($counter == $page) ? "<li class='active'><a>$counter</a></li>" : "<li><span onclick='getResult(\"$targetpath=$counter\");'>$counter</a></li>";
                }
                $paginate .= "<li><a>...</a></li>";
                $paginate .= "<li><span onclick='getResult(\"$targetpath=$LastPagem1\");'>$LastPagem1</a></li>";
                $paginate .= "<li><span onclick='getResult(\"$targetpath=$lastpage\");'>$lastpage</a><li>";
            } else {

                // End only hide early pages
                $paginate .= "<li><span onclick='getResult(\"$targetpath=1\");'>1</a></li>";
                $paginate .= "<li><span onclick='getResult(\"$targetpath=2\");'>2</a></li>";
                $paginate .= "<li><a>...</a></li>";

                for ($counter = $lastpage - (2 + ($stages * 2)); $counter <= $lastpage; $counter++) {
                    $paginate .= ($counter == $page) ? "<li class='active'><a>$counter</a></li>" : "<li><span onclick='getResult(\"$targetpath=$counter\");'>$counter</a></li>";
                }
            }
        }
        // Next
        $paginate .= ($page < $counter - 1) ? "<li><span onclick='getResult(\"$targetpath=$next\");'>Next &gt;</a></li>" : "<li class='disabled'><a>Next &gt;</a></li>";
        $paginate .= "</ul>";
    }
    return $paginate;
}

function ageCalculator($date = null) {
    if ($date) {
        $tz = new DateTimeZone('Europe/London');
        $age = DateTime::createFromFormat('Y-m-d', $date, $tz)->diff(new DateTime('now', $tz))->y;
        return $age . ' years';
    } else {
        return 'Unknown';
    }
}

function sinceCalculator($date = null) {
    if ($date) {
        $date = date('Y-m-d', strtotime($date));
        $tz = new DateTimeZone('Europe/London');
        $age = DateTime::createFromFormat('Y-m-d', $date, $tz)
                ->diff(new DateTime('now', $tz));

        $result = '';
        $result .= ($age->y) ? $age->y . 'y ' : '';
        $result .= ($age->m) ? $age->m . 'm ' : '';
        $result .= ($age->d) ? $age->d . 'd ' : '';
        $result .= ($age->h) ? $age->h . 'h ' : '';
        return $result;
    } else {
        return 'Unknown';
    }
}

function password_encription($string = '') {
    return password_hash($string, PASSWORD_BCRYPT);
}

function initGoogleMap($lat = 0, $lng = 0, $divID = 'map-container', $title = 'Not Defiend') {
    $script = '';
    if ($lat && $lng) {
        $script .= '<div style="height: 280px;" id="' . $divID . '"></div>' . "\r\n";
        $script .= <<<EOT
            <script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyBOKy0BTRCMXke5lOw6YhaPmVy4L8d1xq0"></script> 
            <script type="text/javascript">
            function init_map() {
                var var_location = new google.maps.LatLng($lat, $lng);

                var var_mapoptions = {
                    center: var_location,
                    zoom: 14
                };

                var var_marker = new google.maps.Marker({
                position: var_location,
                map: var_map,
                title: "{$title}"});

                var var_map = new google.maps.Map(document.getElementById("$divID"),
                        var_mapoptions);
                var_marker.setMap(var_map);
            }
            google.maps.event.addDomListener(window, 'load', init_map);

        </script>        

EOT;
    } else {
        $script .= '<noscript> Lat, and lng are empty </noscript>';
        $script .= $title;
    }
    return $script;
}

function initGoogleMapMulti($locations, $divID = 'map', $title = 'Not Defiend') {
    $script = '';
    $count = 0;
    $js_location = '';


    if ($locations) {

        foreach ($locations as $location) {
            $count++;
            $js_location .= "['{$location->location}', {$location->lat}, {$location->lng}, {$count}]," . "\n";
        }


        $script .= '<div style="height: 280px;" id="' . $divID . '"></div>' . "\r\n";
        $script .= <<<EOT
            <script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyBOKy0BTRCMXke5lOw6YhaPmVy4L8d1xq0"></script> 
            <script type="text/javascript">            
                var locations = [$js_location];
                
                window.map = new google.maps.Map(document.getElementById('map'), {
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                });
                var infowindow = new google.maps.InfoWindow();
                var bounds = new google.maps.LatLngBounds();
                for (i = 0; i < locations.length; i++) {
                    marker = new google.maps.Marker({
                        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                        map: map
                    });
                    bounds.extend(marker.position);
                    google.maps.event.addListener(marker, 'click', (function (marker, i) {
                        return function () {
                            infowindow.setContent(locations[i][0]);
                            infowindow.open(map, marker);
                        }
                    })(marker, i));
                }
                map.fitBounds(bounds);
                var listener = google.maps.event.addListener(map, "idle", function () {
                    map.setZoom(6);
                    google.maps.event.removeListener(listener);
                });

            </script>            

EOT;
    } else {
        $script .= '<noscript> Lat, and lng are empty </noscript>';
        $script .= $title;
    }
    return $script;
}

function get_admin_email() {
    return getSettingItem('OutgoingEmail');
}

function getSettingItem($setting_key = null) {
    $ci = & get_instance();
    $setting = $ci->db->get_where('settings', ['label' => $setting_key])->row();
    return isset($setting->value) ? $setting->value : false;
}

function getRoleName($role_id = 0) {
    $ci = & get_instance();
    $role = $ci->db
            ->select('role_name')
            ->get_where('roles', ['id' => $role_id])
            ->row();
    return $role->role_name;
}

function getCountryName($country_id = 0) {
    $ci = & get_instance();
    $country = $ci->db
            ->select('name')
            ->get_where('countries', ['id' => $country_id])
            ->row();
    if ($country) {
        return $country->name;
    } else {
        return 'Unknown';
    }
}

function getDropDownCountries($country_id = 0) {
    $ci = & get_instance();
    $query = $ci->db->get_where('countries', ['type' => '1', 'parent_id' => '0']);
    echo $ci->db->last_query();
    $options = '<option value="">--Select Country--</option>';
    foreach ($query->result() as $row) {
        $options .= '<option value="' . $row->id . '" ';
        $options .= ($row->id == $country_id ) ? 'selected="selected"' : '';
        $options .= '>' . $row->name . '</option>';
    }
    return $options;
}

function bdDateFormat($data = '0000-00-00') {
    return ($data == '0000-00-00') ? 'Unknown' : date('d/m/y', strtotime($data));
}

function isCheck($checked = 0, $match = 1) {
    $checked = ($checked);
    return ($checked == $match) ? 'checked="checked"' : '';
}

function getCurrency($selected = '&pound') {
    $codes = [
        '&pound' => "&pound; GBP",
        '&dollar' => "&dollar; USD",
        '&nira' => "&#x20A6; NGN"
    ];
    $row = '<select name="data[Setting][Currency]" class="form-control">';
    foreach ($codes as $key => $option) {
        $row .= '<option value="' . htmlentities($key) . '"';
        $row .= ($selected == $key) ? ' selected' : '';
        $row .= '>' . $option . '</option>';
    }
    $row .= '</select>';
    return $row;
}

function getTimeZone($name, $selected = '&pound') {
    $codes = [
        'Pacific/Midway' => "(GMT-11:00) Midway Island",
        'US/Samoa' => "(GMT-11:00) Samoa",
        'US/Hawaii' => "(GMT-10:00) Hawaii",
        'US/Alaska' => "(GMT-09:00) Alaska",
        'US/Pacific' => "(GMT-08:00) Pacific Time (US &amp; Canada)",
        'America/Tijuana' => "(GMT-08:00) Tijuana",
        'US/Arizona' => "(GMT-07:00) Arizona",
        'US/Mountain' => "(GMT-07:00) Mountain Time (US &amp; Canada)",
        'America/Chihuahua' => "(GMT-07:00) Chihuahua",
        'America/Mazatlan' => "(GMT-07:00) Mazatlan",
        'America/Mexico_City' => "(GMT-06:00) Mexico City",
        'America/Monterrey' => "(GMT-06:00) Monterrey",
        'Canada/Saskatchewan' => "(GMT-06:00) Saskatchewan",
        'US/Central' => "(GMT-06:00) Central Time (US &amp; Canada)",
        'US/Eastern' => "(GMT-05:00) Eastern Time (US &amp; Canada)",
        'US/East-Indiana' => "(GMT-05:00) Indiana (East)",
        'America/Bogota' => "(GMT-05:00) Bogota",
        'America/Lima' => "(GMT-05:00) Lima",
        'America/Caracas' => "(GMT-04:30) Caracas",
        'Canada/Atlantic' => "(GMT-04:00) Atlantic Time (Canada)",
        'America/La_Paz' => "(GMT-04:00) La Paz",
        'America/Santiago' => "(GMT-04:00) Santiago",
        'Canada/Newfoundland' => "(GMT-03:30) Newfoundland",
        'America/Buenos_Aires' => "(GMT-03:00) Buenos Aires",
        'Greenland' => "(GMT-03:00) Greenland",
        'Atlantic/Stanley' => "(GMT-02:00) Stanley",
        'Atlantic/Azores' => "(GMT-01:00) Azores",
        'Atlantic/Cape_Verde' => "(GMT-01:00) Cape Verde Is.",
        'Africa/Casablanca' => "(GMT) Casablanca",
        'Europe/Dublin' => "(GMT) Dublin",
        'Europe/Lisbon' => "(GMT) Lisbon",
        'Europe/London' => "(GMT) London",
        'Africa/Monrovia' => "(GMT) Monrovia",
        'Europe/Amsterdam' => "(GMT+01:00) Amsterdam",
        'Europe/Belgrade' => "(GMT+01:00) Belgrade",
        'Europe/Berlin' => "(GMT+01:00) Berlin",
        'Europe/Bratislava' => "(GMT+01:00) Bratislava",
        'Europe/Brussels' => "(GMT+01:00) Brussels",
        'Europe/Budapest' => "(GMT+01:00) Budapest",
        'Europe/Copenhagen' => "(GMT+01:00) Copenhagen",
        'Europe/Ljubljana' => "(GMT+01:00) Ljubljana",
        'Europe/Madrid' => "(GMT+01:00) Madrid",
        'Europe/Paris' => "(GMT+01:00) Paris",
        'Europe/Prague' => "(GMT+01:00) Prague",
        'Europe/Rome' => "(GMT+01:00) Rome",
        'Europe/Sarajevo' => "(GMT+01:00) Sarajevo",
        'Europe/Skopje' => "(GMT+01:00) Skopje",
        'Europe/Stockholm' => "(GMT+01:00) Stockholm",
        'Europe/Vienna' => "(GMT+01:00) Vienna",
        'Europe/Warsaw' => "(GMT+01:00) Warsaw",
        'Europe/Zagreb' => "(GMT+01:00) Zagreb",
        'Europe/Athens' => "(GMT+02:00) Athens",
        'Europe/Bucharest' => "(GMT+02:00) Bucharest",
        'Africa/Cairo' => "(GMT+02:00) Cairo",
        'Africa/Harare' => "(GMT+02:00) Harare",
        'Europe/Helsinki' => "(GMT+02:00) Helsinki",
        'Europe/Istanbul' => "(GMT+02:00) Istanbul",
        'Asia/Jerusalem' => "(GMT+02:00) Jerusalem",
        'Europe/Kiev' => "(GMT+02:00) Kyiv",
        'Europe/Minsk' => "(GMT+02:00) Minsk",
        'Europe/Riga' => "(GMT+02:00) Riga",
        'Europe/Sofia' => "(GMT+02:00) Sofia",
        'Europe/Tallinn' => "(GMT+02:00) Tallinn",
        'Europe/Vilnius' => "(GMT+02:00) Vilnius",
        'Asia/Baghdad' => "(GMT+03:00) Baghdad",
        'Asia/Kuwait' => "(GMT+03:00) Kuwait",
        'Africa/Nairobi' => "(GMT+03:00) Nairobi",
        'Asia/Riyadh' => "(GMT+03:00) Riyadh",
        'Asia/Tehran' => "(GMT+03:30) Tehran",
        'Europe/Moscow' => "(GMT+04:00) Moscow",
        'Asia/Baku' => "(GMT+04:00) Baku",
        'Europe/Volgograd' => "(GMT+04:00) Volgograd",
        'Asia/Muscat' => "(GMT+04:00) Muscat",
        'Asia/Tbilisi' => "(GMT+04:00) Tbilisi",
        'Asia/Yerevan' => "(GMT+04:00) Yerevan",
        'Asia/Kabul' => "(GMT+04:30) Kabul",
        'Asia/Karachi' => "(GMT+05:00) Karachi",
        'Asia/Tashkent' => "(GMT+05:00) Tashkent",
        'Asia/Kolkata' => "(GMT+05:30) Kolkata",
        'Asia/Kathmandu' => "(GMT+05:45) Kathmandu",
        'Asia/Yekaterinburg' => "(GMT+06:00) Ekaterinburg",
        'Asia/Almaty' => "(GMT+06:00) Almaty",
        'Asia/Dhaka' => "(GMT+06:00) Dhaka",
        'Asia/Novosibirsk' => "(GMT+07:00) Novosibirsk",
        'Asia/Bangkok' => "(GMT+07:00) Bangkok",
        'Asia/Jakarta' => "(GMT+07:00) Jakarta",
        'Asia/Krasnoyarsk' => "(GMT+08:00) Krasnoyarsk",
        'Asia/Chongqing' => "(GMT+08:00) Chongqing",
        'Asia/Hong_Kong' => "(GMT+08:00) Hong Kong",
        'Asia/Kuala_Lumpur' => "(GMT+08:00) Kuala Lumpur",
        'Australia/Perth' => "(GMT+08:00) Perth",
        'Asia/Singapore' => "(GMT+08:00) Singapore",
        'Asia/Taipei' => "(GMT+08:00) Taipei",
        'Asia/Ulaanbaatar' => "(GMT+08:00) Ulaan Bataar",
        'Asia/Urumqi' => "(GMT+08:00) Urumqi",
        'Asia/Irkutsk' => "(GMT+09:00) Irkutsk",
        'Asia/Seoul' => "(GMT+09:00) Seoul",
        'Asia/Tokyo' => "(GMT+09:00) Tokyo",
        'Australia/Adelaide' => "(GMT+09:30) Adelaide",
        'Australia/Darwin' => "(GMT+09:30) Darwin",
        'Asia/Yakutsk' => "(GMT+10:00) Yakutsk",
        'Australia/Brisbane' => "(GMT+10:00) Brisbane",
        'Australia/Canberra' => "(GMT+10:00) Canberra",
        'Pacific/Guam' => "(GMT+10:00) Guam",
        'Australia/Hobart' => "(GMT+10:00) Hobart",
        'Australia/Melbourne' => "(GMT+10:00) Melbourne",
        'Pacific/Port_Moresby' => "(GMT+10:00) Port Moresby",
        'Australia/Sydney' => "(GMT+10:00) Sydney",
        'Asia/Vladivostok' => "(GMT+11:00) Vladivostok",
        'Asia/Magadan' => "(GMT+12:00) Magadan",
        'Pacific/Auckland' => "(GMT+12:00) Auckland",
        'Pacific/Fiji' => "(GMT+12:00) Fiji"
    ];
    $row = '<select name="data[Setting][' . $name . ']" class="form-control">';
    foreach ($codes as $key => $option) {
        $row .= '<option value="' . htmlentities($key) . '"';
        $row .= ($selected == $key) ? ' selected' : '';
        $row .= '>' . $option . '</option>';
    }
    $row .= '</select>';
    return $row;
}

function globalDateTimeFormat($datetime = '0000-00-00 00:00:00') {
    if ($datetime == '0000-00-00 00:00:00' or $datetime == '0000-00-00' or $datetime == '') {
        return 'Unknown';
    }
    return date('d/m/y h:i A', strtotime($datetime));
}

function globalDateFormat($datetime = '0000-00-00 00:00:00') {
    if ($datetime == '0000-00-00 00:00:00' or $datetime == '0000-00-00' or $datetime == null) {
        return 'Unknown';
    }
    return date('d M y', strtotime($datetime));
}

function globalTimeOnly($datetime = '0000-00-00 00:00:00') {
    if ($datetime == '0000-00-00 00:00:00' or $datetime == '0000-00-00' or $datetime == null) {
        return 'Unknown';
    }
    return date('h:i A', strtotime($datetime));
}

function returnJSON($array = []) {
    return json_encode($array);
}

function ajaxRespond($status = 'FAIL', $msg = 'Fail! Something went wrong') {
    return returnJSON(['Status' => strtoupper($status), 'Msg' => $msg]);
}

function ajaxAuthorized() {
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
        return true;
    } else {
        $html = '';
        $html .= '<center>';
        $html .= '<h1 style="color:red;">Access Denied !</h1>';
        $html .= '<hr>';
        $html .= '<p>It seems that you might come here via an unauthorised way</p>';
        $html .= '</center>';
        die($html);
    }
}

function globalCurrencyFormat($amount = 0, $sign = '&pound;') {
    if($sign != '&pound;'){
        $prefix = getSettingItem('Currency');
    } else {
        $prefix = $sign;
    }
    
    if (is_null($amount) or empty($amount)) {
        return 0;
    } else {
        return $prefix . number_format($amount, 2);
    }
}

function bdContactNumber($contact = null) {
    if ($contact && strlen($contact) == 11) {
        return substr($contact, 0, 5) . '-' . substr($contact, 5, 3) . '-' . substr($contact, 8, 3);
    } else {
        return $contact;
    }
}

function getPaginatorLimiter($selected = 100) {
    $range = [100, 500, 1000, 2000, 5000];
    $option = '';
    foreach ($range as $limit) {
        $option .= '<option';
        $option .= ( $selected == $limit) ? ' selected' : '';
        $option .= '>' . $limit . '</option>';
    }
    return $option;
}

function getUserNameByID($user_id = 0) {
    $username = '<label class="label label-danger">User Deleted</label>';
    if ($user_id) {
        $ci = & get_instance();
        $user = $ci->db->get_where('users', ['id' => $user_id])->row();
        if ($user) {
            $username = $user->first_name . ' ' . $user->last_name;
        }
    }
    return $username;
}
function getUserNameEmailByID($user_id = 0) {
    $username = '<label class="label label-danger">User Deleted</label>';
    if ($user_id) {
        $ci = & get_instance();
        $user = $ci->db->get_where('users', ['id' => $user_id])->row();
        if ($user) {
            $username = $user->first_name . ' ' . $user->last_name . ' ('.$user->email.')';
        }
    }
    return $username;
}

function getPhoneCode($selected = '') {
    $codes = [
        '' => '-- Select Code --',
        '213' => 'Algeria (+213)',
        '376' => 'Andorra (+376)',
        '244' => 'Angola (+244)',
        '1264' => 'Anguilla (+1264)',
        '1268' => 'Antigua &amp; Barbuda (+1268)',
        '54' => 'Argentina (+54)',
        '374' => 'Armenia (+374)',
        '297' => 'Aruba (+297)',
        '61' => 'Australia (+61)',
        '43' => 'Austria (+43)',
        '994' => 'Azerbaijan (+994)',
        '1242' => 'Bahamas (+1242)',
        '973' => 'Bahrain (+973)',
        '880' => 'Bangladesh (+880)',
        '1246' => 'Barbados (+1246)',
        '375' => 'Belarus (+375)',
        '32' => 'Belgium (+32)',
        '501' => 'Belize (+501)',
        '229' => 'Benin (+229)',
        '1441' => 'Bermuda (+1441)',
        '975' => 'Bhutan (+975)',
        '591' => 'Bolivia (+591)',
        '387' => 'Bosnia Herzegovina (+387)',
        '267' => 'Botswana (+267)',
        '55' => 'Brazil (+55)',
        '673' => 'Brunei (+673)',
        '359' => 'Bulgaria (+359)',
        '226' => 'Burkina Faso (+226)',
        '257' => 'Burundi (+257)',
        '855' => 'Cambodia (+855)',
        '237' => 'Cameroon (+237)',
        '1' => 'Canada (+1)',
        '238' => 'Cape Verde Islands (+238)',
        '1345' => 'Cayman Islands (+1345)',
        '236' => 'Central African Republic (+236)',
        '56' => 'Chile (+56)',
        '86' => 'China (+86)',
        '57' => 'Colombia (+57)',
        '269' => 'Comoros (+269)',
        '242' => 'Congo (+242)',
        '682' => 'Cook Islands (+682)',
        '506' => 'Costa Rica (+506)',
        '385' => 'Croatia (+385)',
        '53' => 'Cuba (+53)',
        '90392' => 'Cyprus North (+90392)',
        '357' => 'Cyprus South (+357)',
        '42' => 'Czech Republic (+42)',
        '45' => 'Denmark (+45)',
        '253' => 'Djibouti (+253)',
        '1809' => 'Dominica (+1809)',
        '1809' => 'Dominican Republic (+1809)',
        '593' => 'Ecuador (+593)',
        '20' => 'Egypt (+20)',
        '503' => 'El Salvador (+503)',
        '240' => 'Equatorial Guinea (+240)',
        '291' => 'Eritrea (+291)',
        '372' => 'Estonia (+372)',
        '251' => 'Ethiopia (+251)',
        '500' => 'Falkland Islands (+500)',
        '298' => 'Faroe Islands (+298)',
        '679' => 'Fiji (+679)',
        '358' => 'Finland (+358)',
        '33' => 'France (+33)',
        '594' => 'French Guiana (+594)',
        '689' => 'French Polynesia (+689)',
        '241' => 'Gabon (+241)',
        '220' => 'Gambia (+220)',
        '7880' => 'Georgia (+7880)',
        '49' => 'Germany (+49)',
        '233' => 'Ghana (+233)',
        '350' => 'Gibraltar (+350)',
        '30' => 'Greece (+30)',
        '299' => 'Greenland (+299)',
        '1473' => 'Grenada (+1473)',
        '590' => 'Guadeloupe (+590)',
        '671' => 'Guam (+671)',
        '502' => 'Guatemala (+502)',
        '224' => 'Guinea (+224)',
        '245' => 'Guinea - Bissau (+245)',
        '592' => 'Guyana (+592)',
        '509' => 'Haiti (+509)',
        '504' => 'Honduras (+504)',
        '852' => 'Hong Kong (+852)',
        '36' => 'Hungary (+36)',
        '354' => 'Iceland (+354)',
        '91' => 'India (+91)',
        '62' => 'Indonesia (+62)',
        '98' => 'Iran (+98)',
        '964' => 'Iraq (+964)',
        '353' => 'Ireland (+353)',
        '972' => 'Israel (+972)',
        '39' => 'Italy (+39)',
        '1876' => 'Jamaica (+1876)',
        '81' => 'Japan (+81)',
        '962' => 'Jordan (+962)',
        '7' => 'Kazakhstan (+7)',
        '254' => 'Kenya (+254)',
        '686' => 'Kiribati (+686)',
        '850' => 'Korea North (+850)',
        '82' => 'Korea South (+82)',
        '965' => 'Kuwait (+965)',
        '996' => 'Kyrgyzstan (+996)',
        '856' => 'Laos (+856)',
        '371' => 'Latvia (+371)',
        '961' => 'Lebanon (+961)',
        '266' => 'Lesotho (+266)',
        '231' => 'Liberia (+231)',
        '218' => 'Libya (+218)',
        '417' => 'Liechtenstein (+417)',
        '370' => 'Lithuania (+370)',
        '352' => 'Luxembourg (+352)',
        '853' => 'Macao (+853)',
        '389' => 'Macedonia (+389)',
        '261' => 'Madagascar (+261)',
        '265' => 'Malawi (+265)',
        '60' => 'Malaysia (+60)',
        '960' => 'Maldives (+960)',
        '223' => 'Mali (+223)',
        '356' => 'Malta (+356)',
        '692' => 'Marshall Islands (+692)',
        '596' => 'Martinique (+596)',
        '222' => 'Mauritania (+222)',
        '269' => 'Mayotte (+269)',
        '52' => 'Mexico (+52)',
        '691' => 'Micronesia (+691)',
        '373' => 'Moldova (+373)',
        '377' => 'Monaco (+377)',
        '976' => 'Mongolia (+976)',
        '1664' => 'Montserrat (+1664)',
        '212' => 'Morocco (+212)',
        '258' => 'Mozambique (+258)',
        '95' => 'Myanmar (+95)',
        '264' => 'Namibia (+264)',
        '674' => 'Nauru (+674)',
        '977' => 'Nepal (+977)',
        '31' => 'Netherlands (+31)',
        '687' => 'New Caledonia (+687)',
        '64' => 'New Zealand (+64)',
        '505' => 'Nicaragua (+505)',
        '227' => 'Niger (+227)',
        '234' => 'Nigeria (+234)',
        '683' => 'Niue (+683)',
        '672' => 'Norfolk Islands (+672)',
        '670' => 'Northern Marianas (+670)',
        '47' => 'Norway (+47)',
        '968' => 'Oman (+968)',
        '680' => 'Palau (+680)',
        '507' => 'Panama (+507)',
        '675' => 'Papua New Guinea (+675)',
        '595' => 'Paraguay (+595)',
        '51' => 'Peru (+51)',
        '63' => 'Philippines (+63)',
        '48' => 'Poland (+48)',
        '351' => 'Portugal (+351)',
        '1787' => 'Puerto Rico (+1787)',
        '974' => 'Qatar (+974)',
        '262' => 'Reunion (+262)',
        '40' => 'Romania (+40)',
        '7' => 'Russia (+7)',
        '250' => 'Rwanda (+250)',
        '378' => 'San Marino (+378)',
        '239' => 'Sao Tome &amp; Principe (+239)',
        '966' => 'Saudi Arabia (+966)',
        '221' => 'Senegal (+221)',
        '381' => 'Serbia (+381)',
        '248' => 'Seychelles (+248)',
        '232' => 'Sierra Leone (+232)',
        '65' => 'Singapore (+65)',
        '421' => 'Slovak Republic (+421)',
        '386' => 'Slovenia (+386)',
        '677' => 'Solomon Islands (+677)',
        '252' => 'Somalia (+252)',
        '27' => 'South Africa (+27)',
        '34' => 'Spain (+34)',
        '94' => 'Sri Lanka (+94)',
        '290' => 'St. Helena (+290)',
        '1869' => 'St. Kitts (+1869)',
        '1758' => 'St. Lucia (+1758)',
        '249' => 'Sudan (+249)',
        '597' => 'Suriname (+597)',
        '268' => 'Swaziland (+268)',
        '46' => 'Sweden (+46)',
        '41' => 'Switzerland (+41)',
        '963' => 'Syria (+963)',
        '886' => 'Taiwan (+886)',
        '7' => 'Tajikstan (+7)',
        '66' => 'Thailand (+66)',
        '228' => 'Togo (+228)',
        '676' => 'Tonga (+676)',
        '1868' => 'Trinidad &amp; Tobago (+1868)',
        '216' => 'Tunisia (+216)',
        '90' => 'Turkey (+90)',
        '7' => 'Turkmenistan (+7)',
        '993' => 'Turkmenistan (+993)',
        '1649' => 'Turks &amp; Caicos Islands (+1649)',
        '688' => 'Tuvalu (+688)',
        '256' => 'Uganda (+256)',
        '44' => 'UK (+44)',
        '380' => 'Ukraine (+380)',
        '971' => 'United Arab Emirates (+971)',
        '598' => 'Uruguay (+598)',
        '1' => 'USA (+1)',
        '7' => 'Uzbekistan (+7)',
        '678' => 'Vanuatu (+678)',
        '379' => 'Vatican City (+379)',
        '58' => 'Venezuela (+58)',
        '84' => 'Vietnam (+84)',
        '84' => 'Virgin Islands - British (+1284)',
        '84' => 'Virgin Islands - US (+1340)',
        '681' => 'Wallis &amp; Futuna (+681)',
        '969' => 'Yemen (North)(+969)',
        '967' => 'Yemen (South)(+967)',
        '260' => 'Zambia (+260)',
        '263' => 'Zimbabwe (+263)',
    ];

    $row = '';
    foreach ($codes as $key => $option) {
        $row .= '<option value="' . htmlentities($key) . '"';
        $row .= ($selected == $key) ? ' selected' : '';
        $row .= '>' . $option . '</option>';
    }
    return $row;
}

function user_title($value = null) {
    $names = array('Mr.', 'Miss.', 'Mrs.', 'Ms.');
    $options = '';
    $options .= '<option value="0">--Title--</option>';

    foreach ($names as $key) {
        $options .= '<option value="' . $key . '"';
        $options .= ($value == $key) ? ' selected' : '';
        $options .= '>' . $key . '</option>';
    }

    return $options;
}

function getCompanyIncomeGroupName($id = 0) {
    $ci = & get_instance();
    $result = $ci->db
            ->select('name')
            ->get_where('company_income_group', ['id' => $id])
            ->row();
    if ($result) {
        return $result->name;
    } else {
        return false;
    }
}


function companyIncomeGroup($id = 0) {

    $ci = & get_instance();
    $query = $ci->db->get('company_income_group');

    $options = '<option value="">--Select Group--</option>';

    foreach ($query->result() as $row) {
        $options .= '<option value="' . $row->id . '" ';
        $options .= ($row->id == $id ) ? 'selected="selected"' : '';
        $options .= '>' . $row->name . '</option>';
    }
    return $options;
}

function statusLevel($status = null) {
    if ($status == 'Publish') {
        return '<span class="label label-success">Publish</span>';
    } elseif ($status == 'Draft') {
        echo '<span class="label label-warning">Draft</span>';
    }
}

function two_date_diff($form, $today) {
    $date1 = new DateTime($form);
    $date2 = new DateTime($today);
    $diff = $date1->diff($date2);

    if ($diff->d == 0) {
        return 'Same Day';
    } else {
        return $diff->d . ' Day ago';
    }
}

function numeric_dropdown($i = 0, $end = 12, $incr = 1, $selected = 0) {
    $option = '';
    for ($i; $i <= $end; $i += $incr) {
        $option .= '<option value="' . $i . '"';
        $option .= ( $selected == $i) ? ' selected' : '';
        $option .= '>' . sprintf('%02d', $i) . '</option>';
    }
    return $option;
}

function encode($string, $key = 'my_encript_key') {
    $key = sha1($key);
    $strLen = strlen($string);
    $keyLen = strlen($key);
    $j = null;
    $hash = null;
    for ($i = 0; $i < $strLen; $i++) {
        $ordStr = ord(substr($string, $i, 1));
        if ($j == $keyLen) {
            $j = 0;
        }
        $ordKey = ord(substr($key, $j, 1));
        $j++;
        $hash .= strrev(base_convert(dechex($ordStr + $ordKey), 16, 36));
    }
    return $hash;
}

function decode($string, $key = 'my_encript_key') {
    $key = sha1($key);
    $strLen = strlen($string);
    $keyLen = strlen($key);
    $j = null;
    $hash = null;
    for ($i = 0; $i < $strLen; $i += 2) {
        $ordStr = hexdec(base_convert(strrev(substr($string, $i, 2)), 36, 16));
        if ($j == $keyLen) {
            $j = 0;
        }
        $ordKey = ord(substr($key, $j, 1));
        $j++;
        $hash .= chr($ordStr - $ordKey);
    }
    return $hash;
}

function getMyEncodeKey($string, $salt, $key) {
    $deocode = decode($string, $salt);
    $json_data = json_decode($deocode);
    if (isset($json_data->$key)) {
        return $json_data->$key;
    } else {
        return false;
    }
}

function removeImage($photo = null) {
    $filename = dirname(APPPATH) . '/' . $photo;
    if ($photo && file_exists($filename)) {
        unlink($filename);
    }
    return TRUE;
}

function slugify($text) {
    $filter1 = strtolower(strip_tags(trim($text)));
    $filter2 = html_entity_decode($filter1);
    $filter3 = iconv('utf-8', 'us-ascii//TRANSLIT', $filter2);
    $filter4 = preg_replace('~[^ a-z0-9_.]~', ' ', $filter3);
    $filter5 = preg_replace('~ ~', '-', $filter4);
    $return = preg_replace('~-+~', '-', $filter5);
    if (empty($return)) {
        return 'auto-' . time() . rand(0, 99);
    } else {
        return $return;
    }
}

function getLocationList($selected = 0, $type = 0, $parent_id = 0) {
    $ci = & get_instance();

    $ci->db->where('type', $type);

    if ($parent_id) {
        $ci->db->where('parent_id', $parent_id);
    }

    $results = $ci->db->get('countries')->result();

    $options = '';
    foreach ($results as $row) {
        $options .= '<option value="' . $row->id . '" ';
        $options .= ($row->id == $selected ) ? 'selected="selected"' : '';
        $options .= '>' . $row->name . '</option>';
    }
    return $options;
}

function uploadPhoto($FILE = array(), $path = '', $name = '') {
    $handle = new upload($FILE);
    if ($handle->uploaded) {
        $handle->file_new_name_body = $name;
        $handle->file_force_extension = true;
        $handle->file_new_name_ext = 'jpg';
        $handle->process($path);
        if ($handle->processed) {
            return stripslashes($handle->file_dst_pathname);
        } else {
            return '';
        }
    }
    return '';
}

function build_pagination_url($link = 'listing', $page = 'page', $ext = false) {
    $array = $_GET;
    $url = $link . '?';

    unset($array[$page]);
    unset($array['_']);

    if ($array) {
        $url .= \http_build_query($array);
    }
    if ($ext) {  $url .= "&{$page}";  }
    return $url;
}

function more_text($text, $id = 1, $limit = 200) {
    $html = '';
    $plain_txt = strip_tags($text);
    $leanth = strlen($plain_txt);
    $short_txt = substr($plain_txt, 0, $limit);

    if ($leanth >= $limit) {
        $html .= '<span id="less' . $id . '">';
        $html .= str_replace("\n", "<br/>", $short_txt);
        $html .= '....&nbsp;<span style="color:#f60; cursor:pointer;" onClick="view_full_text(\'' . $id . '\');">more&rarr;</span>';
        $html .= '</span>';

        $html .= '<span id="more' . $id . '" style="display:none">';
        $html .= $text;
        $html .= '&nbsp;<span style="color:#f60; cursor:pointer;" onClick="view_full_text(\'' . $id . '\');">&larr;Less</span>';
        $html .= '</span>';
    } else {
        return $html .= $text;
    }
    return $html;

//    js need
//    function view_full_text(id){	
//        $('#less'+id).toggle();
//        $('#more'+id).toggle();
//    }
}

function timePassed($date_time = '0000-00-00 00:00:00') {
    $return = '';

    if ($date_time == '0000-00-00 00:00:00') {
        $return = '';
    } else {

        $timestamp = (int) strtotime($date_time);
        $current_time = time();
        $diff = $current_time - $timestamp;

        $intervals = array(
            'year' => 31556926, 'month' => 2629744, 'week' => 604800, 'day' => 86400, 'hour' => 3600, 'minute' => 60
        );
        if ($diff == 0) {
            $return = 'just now';
        }
        if ($diff < 60) {
            $return = $diff == 1 ? $diff . ' second ago' : $diff . ' seconds ago';
        }
        if ($diff >= 60 && $diff < $intervals['hour']) {
            $diff = floor($diff / $intervals['minute']);
            $return = $diff == 1 ? $diff . ' minute ago' : $diff . ' minutes ago';
        }
        if ($diff >= $intervals['hour'] && $diff < $intervals['day']) {
            $diff = floor($diff / $intervals['hour']);
            $return = $diff == 1 ? $diff . ' hour ago' : $diff . ' hours ago';
        }
        if ($diff >= $intervals['day'] && $diff < $intervals['week']) {
            $diff = floor($diff / $intervals['day']);
            $return = $diff == 1 ? $diff . ' day ago' : $diff . ' days ago';
        }
        if ($diff >= $intervals['week'] && $diff < $intervals['month']) {
            $diff = floor($diff / $intervals['week']);
            $return = $diff == 1 ? $diff . ' week ago' : $diff . ' weeks ago';
        }
        if ($diff >= $intervals['month'] && $diff < $intervals['year']) {
            $diff = floor($diff / $intervals['month']);
            $return = $diff == 1 ? $diff . ' month ago' : $diff . ' months ago';
        }
        if ($diff >= $intervals['year']) {
            $diff = floor($diff / $intervals['year']);
            $return = $diff == 1 ? $diff . ' year ago' : $diff . ' years ago';
        }
    }
    return $return;
}

function time_elapsed_string($datetime, $full = false) {
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) {
        $string = array_slice($string, 0, 1);
    }
    return $string ? implode(', ', $string) . ' ago' : 'just now';
}

function getCurrencyCode() {
    $prefix = getSettingItem('Currency');
    switch ($prefix) {
        case '&pound;':
            return 'GBP';
            break;
        case '&#x9f3;':
            return 'BDT';
            break;
        case '&dollar;':
            return 'USD';
            break;
        case '&euro;':
            return 'EUR';
            break;
        default:
            return 'GBP';
    }
}

function getPhotoNew($config = []) {
    $default = [
        'class' => 'img-responsive',
        'attr'  => '',
        'photo' => '',
        'no_photo_size' => 'default',
    ];
    $setting =  $config + $default;
    
    $filename = dirname(APPPATH) .'/'. $setting['photo'];
    if ($setting['photo'] && file_exists($filename)) {
        return '<img class="'. $setting['class'] .'" src="'.$setting['photo'].'" '.$setting['attr'].'>';
    } else {
        return '<img class="no_photo '.$setting['class'].'" src="uploads/no_photo_'.$setting['no_photo_size'].'.jpg" ' .$setting['attr']. '>';
    }
}

function getEmailById($user_id = 0){
    $CI = & get_instance();
    $user = $CI->db->select('email')->where('id', $user_id)->get('users')->row();
    return ($user) ? $user->email : null;
}

function featured_status($data_id,$status){
	if($status == 'Yes'){
		return "<span id=\"fea_$data_id\"><span title='Click to unfeature' class=\"ajax_active\" onClick=\"change_featured_status($data_id,$status);\"><i class=\"fa fa-retweet\"></i> Yes</span></span>";
	} else {
		return "<span id=\"fea_$data_id\"><span title='Click to Feature' class=\"ajax_inactive\" onClick=\"change_featured_status($data_id,$status);\"><i class=\"fa fa-retweet\"></i> No</span></span>";
	}	
}


function getHomeFeaturedListings(){
    $ci = & get_instance();
    $ci->db->select('id,first_name,last_name,email,company_name,contact,business_hours');
    $ci->db->select('add_line1,add_line2,city,web,about,profile_photo');
    $ci->db->where('role_id', 4);                
    $ci->db->where('status', 'Active'); 
    $ci->db->limit(9); 
    $ci->db->order_by('id', 'DESC');
    $results = $ci->db->get('users')->result(); 
    
    $output = '';
    foreach($results as $list){
//        $slug = slugify($list->company_name);
        $logo = getPhoto($list->profile_photo);
        $link = getURL($list->id,$list->company_name);
        $output .= '<div class="col-md-4">';
        $output .= '<div class="box-list">';
        $output .= "<div class='thumb'><a href=\"{$link}\"><img src=\"{$logo}\" class=\"img-responsive\" alt=\"Logo\"></a></div>";
        $output .= "<h3><a href=\"{$link}\">{$list->company_name}</a></h3>";
        $output .= "<p><i class=\"fa fa-phone\"></i> <strong>{$list->contact}</strong></p>";
        $output .= "<p class='location'><i class=\"fa fa-map-marker\"></i> " .getShortContent($list->add_line1,30).  "</p>"; //getShortContent($list->add_line2,10).
        $output .= '<p><i class="fa fa-clock-o"></i> '.getBusinessHoursHome($list->business_hours). '</p>';
        $output .= '<div class="bottom-btn">';
        $output .= avarage_review_star($list->id);
//        $output .= '<button type="button" class="btn btn-warning" onclick="sendReview('.$list->id.');">Write Reviews</button>';
        $output .= '<button class="btn btn-success pull-right" onclick="sendEnquiry('.$list->id.');"> Send Enquiry</button>';
        $output .= '</div>';
        $output .= '</div>';
        $output .= '</div>';
    }
    return $output;
}
function getUserData($user_id = 0, $filde_name = 'id'){
    $CI =& get_instance();
    if($user =$CI->db->select($filde_name)->from('users')->where('id',$user_id)->get()->row()){
        return $user->$filde_name;
    }else{
        return $id = 0;
    }
}
function getURL($id=0,$company=''){
     return site_url().'company/'.slugify($company).'/'.$id;
}

function getRecentEmail($limit = 10){
    $ci =& get_instance();
    $html = '';
    $mails = $ci->db->limit($limit)->order_by('id', 'DESC')->get_where('mails',['status' => 'Unread'])->result();
    if($mails){
        foreach ($mails as $mail) {
          $html .= '<tr>';
                $html .= '<td>'.$mail->mail_type.'</td>';
                $html .= '<td><a href="'.base_url('admin/mailbox/read/'.$mail->id).'">'.getShortContent($mail->subject, 30).'</a></td>';
                $html .= '<td><span class="label label-warning">'.$mail->status.'</span></td>';
                $html .= '<td><a href="'.base_url('admin/mailbox/read/'.$mail->id).'"><i class=" fa fa-external-link"></i></a></td>';
          $html .= '</tr>';
        }
        
    }else{
        $html .= '<tr>';
                $html .= '<td colspan="4">No Incoming Mail.</td>';
        $html .= '</tr>';
    }
    
    return $html;
    
}

function getRecentSubscribers($limit = 10){
    $ci =& get_instance();
    $html = '';
    $subscribers = $ci->db->limit($limit)->order_by('id', 'DESC')->get_where('newsletter_subscribers',['status' => 'Subscribe'])->result();
    if($subscribers){
        foreach ($subscribers as $subscribe) {
          $name = ($subscribe->name) ? $subscribe->name  : '--' ;
          $html .= '<tr>';
                $html .= '<td>'.$subscribe->email.'</td>';
                $html .= '<td>'.$name.'</td>';
                $html .= '<td><span class="label label-success">'.$subscribe->status.'</span></td>';
                $html .= '<td>'.globalDateFormat($subscribe->created).'</td>';
          $html .= '</tr>';
        }
        
    }else{
        $html .= '<tr>';
                $html .= '<td colspan="4">No Subscribers Found.</td>';
        $html .= '</tr>';
    }
    
    return $html;
    
}

function getTotalSubscriber(){
    $ci =& get_instance();
    $subscriber = $ci->db->get_where('newsletter_subscribers')->num_rows();
    if($subscriber){
        return $subscriber;
    }else{
        return 0;
    }
}

function getPhoto($photo, $name = '', $noPhotoWidth = '150', $noPhotoHeight = '150') {
    $filename = dirname(BASEPATH) . '/' . $photo;
    if ($photo && file_exists($filename)) {
        return $photo;
    } else {
        if($name){
            $text = firstLetterOfEachWord($name);
        } else {
            $text = getSettingItem('NoPhotoText');
        }
        return 'holder.js/'.$noPhotoWidth.'x'.$noPhotoHeight.'?size=14&text='.$text;
    }
}