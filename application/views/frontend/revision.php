<?php // pp($past_data); ?>
<?php // pp($current_data); ?>

<div class="page-content" style="padding: 15px 0px;">
    <div class="container">
        <div class="row text-center">
            <div class="col-md-6">
                <h2>CURRENT</h2>
                <hr>
            </div>
            <div class="col-md-6">
                <h2>OLD</h2>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <h3><?php echo $current_data->post_title; ?></h3>
            </div>
            <div class="col-md-6">
                <h3><?php echo $past_data->post_title; ?></h3>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <?php echo $current_data->content; ?>
            </div>
            <div class="col-md-6">
                <?php echo $past_data->content; ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <p><strong>SEO Title: </strong><?php echo $current_data->seo_title; ?></p>
                <p><strong>SEO Keyword: </strong><?php echo $current_data->seo_keyword; ?></p>
                <p><strong>SEO Description: </strong><?php echo $current_data->seo_description; ?></p>
            </div>
            <div class="col-md-6">
                <p><strong>SEO Title: </strong><?php echo $past_data->seo_title; ?></p>
                <p><strong>SEO Keyword: </strong><?php echo $past_data->seo_keyword; ?></p>
                <p><strong>SEO Description: </strong><?php echo $past_data->seo_description; ?></p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <?php echo getCMSPhoto($current_data->thumb, 'full'); ?>  
            </div>
            <div class="col-md-6">
                <?php echo getCMSPhoto($past_data->thumb, 'full'); ?>  
            </div>
        </div>
    </div>
</div>