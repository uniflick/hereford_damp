<div class="container upcomingevents">
    <div class="page-title">
        <h1><?php echo $title; ?></h1>
    </div>
    <img src="<?php echo getPhoto($thumb); ?>" class="img-responsive" />
    <div class="post-content"><?php echo $content; ?></div>
</div>