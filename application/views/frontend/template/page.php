<?php if($thumb): ?>
<div class="page-banner hidden-xs">
    <img src="<?php echo $thumb; ?>" class="img-responsive">
</div>
<?php endif; ?>

<div class="page-content">
    <?php echo $content; ?>
</div>