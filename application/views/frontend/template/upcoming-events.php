<div class="page-banner">   
    <img src="assets/theme/images/event-banner.jpg" class="img-responsive">
</div>
<!-- <div class="container">
    <div class="page-title">
        <h1><?php //echo $post_title; ?></h1>
    </div>
</div> -->
<div class="page-content">
<div class="container">
    <?php if( $posts_data ){ ?>
    <?php
    //dd($posts_data);
    foreach ( $posts_data as $post ) :  ?>
    <div class="news-list">
        <div class="row">
            <div class="col-md-3">
                <?php echo getCMSFeaturedThumb($post->thumb,'260','200'); ?>
            </div>
            <div class="col-md-9">
                <h3><?php echo $post->post_title; ?></h3>
                <div class="date"><?php echo globalDateFormat($post->event_date); ?></div>
                <p><?php echo getShortContent($post->content, 230); ?></p>
                <p><a class="donation" href="<?php echo $post->post_url; ?>">Read More</a></p>
            </div>
        </div>                
        <div class="clearfix"></div>
    </div>
    <?php endforeach; ?>
    <?php echo getPaginator($total, $page, $targetpath, $limit); ?>
    <?php } else{ echo '<p class="alert alert-danger">Post Not Found!</p>'; } ?>   
    
</div>
</div>

