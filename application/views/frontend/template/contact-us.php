<div class="contact-page">
<div class="container">
    <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-12">
            <?php echo $content; ?>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <form class="contact-form" id="contactForm" method="post" onsubmit="return contactForm(event);">
                <h4>Contact  us</h4>
                
                <div class="row">
                
                <div class="col-md-12">                   
                    <div class="form-group">
                       <input type="text" class="form-control" id="cf_name" name="name" placeholder="Name">
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                       <input type="email" class="form-control" id="cf_email" name="email" placeholder="Email">
                    </div>  
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                       <input type="tel" class="form-control" id="cf_contact_no" name="contact" placeholder="Phone">
                    </div>
                  </div>
                    <div class="col-md-12">
                    <div class="form-group">
                       <textarea id="cf_message" class="form-control" name="cf_message" placeholder="Message" rows="5"></textarea>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                       <input class="btn btn-primary" type="submit" value="Send Message">
                    </div>
                  </div>
                  <div class="col-md-12">  
                    <div id="cf_ajax_respond"></div>
                  </div>
                </div>
               <div class="clearfix"></div>
                
            </form>
        </div>
        
    </div>
</div>
</div>
<div class="contact-map">
  <img class="img-responsive" src="assets/theme/images/contact-map.jpg">
</div>





<script>
   function contactForm(e) {
       e.preventDefault();
       var formData = jQuery('#contactForm').serialize();
       var error = 0;
       var name = jQuery('#cf_name').val();
       if (!name) {
           jQuery('#cf_name').addClass('required');
           error = 1;
       } else {
           jQuery('#cf_name').removeClass('required');
       }
       var email = jQuery('#cf_email').val();
       if (!email) {
           jQuery('#cf_email').addClass('required');
           error = 1;
       } else {
           jQuery('#cf_email').removeClass('required');
       }
       var phone = jQuery('#cf_contact_no').val();
       if (!name) {
           jQuery('#cf_contact_no').addClass('required');
           error = 1;
       } else {
           jQuery('#cf_contact_no').removeClass('required');
       }
       

       if (!error) {
           jQuery.ajax({
               type: "POST",
               url: "mail/contact_us",
               dataType: 'json',
               data: formData,
               beforeSend: function () {
                   jQuery('#cf_ajax_respond').html('<p class="ajax_processing">Sending...</p>');
               },
               success: function (jsonData) {
                   jQuery('#cf_ajax_respond').html(jsonData.Msg);
                   if (jsonData.Status === 'OK') {
                       document.getElementById("contactForm").reset();
                       setTimeout(function () {                           
                           $('#cf_ajax_respond').html('');
                           $('#cf_ajax_respond').css('display', 'block');
                       }, 2000);
                   }
               }
           });
       }
   }
   
</script>