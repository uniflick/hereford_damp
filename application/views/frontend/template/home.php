<?php echo view_slideshow(2); ?>


<div class="whychosse-home">
  <div class="container">
    <div class="row">
    <div class="col-md-5 whychossebanner">
        <img src="assets/theme/images/about.png" class="img-responsive">
      </div>
      <div class="col-md-7">
        <h1>welcome to DAMP and MOULD CONTROL</h1>
        <p>Does your property suffer from rising damp, penetrating damp or condensation? </p>

<p>If so you'll know how frustrating damp problems can be. Whatever the cause, the symptoms are the same; expensive and unsightly damage to your décor. Even worse, ultimately damp problems can cause fungal decay in flooring, skirting boards and other joinery timbers.</p>

<p>Damp and Mould Control are consultants and experts in the diagnosis and treatment of Condensation Damp, Rising Damp, Penetrating Damp, Timber Treatments services. We provide free damp and timber surveys for 
houseowners, letting agents and builders/developers.</p>

<p>Damp and Mould Control covers all damp, mould and decay problems in the Forest of Dean , Wye Valley, Gloucestershire, Herefordshire, Worcestershire, Gwent and Powys</p>
          
      </div>
      
    </div>
  </div>
</div>


<div class="text-center special-services">
    <div class="container">
      <h1 class="wellcome">WORKING WITH EXCELLENT OUR SPECIAL SERVICES</h1>
      <h3>Don’t hesitate, contact us for better help and services. <a href="#">Explore all services</a></h3>
 </div>
 </div>  	
 
<div class="service-box-g">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-12">
                  <div class="service-box">
                    <a href="condensation-damp"><img src="assets/theme/images/homebox1.jpg" class="img-responsive">
                      <h4>Condensation Damp</h4>
                    </a>
                  </div>
              </div>
            <div class="col-md-3  col-sm-12">
                  <div class="service-box">
                    <a href="rising-damp"><img src="assets/theme/images/homebox2.jpg" class="img-responsive">
                      <h4>Rising Damp</h4>
                    </a>

                  </div>
              </div>
            <div class="col-md-3 col-sm-12">
                  <div class="service-box">
                    <a href="paneterting-damp"><img src="assets/theme/images/homebox3.jpg" class="img-responsive">
                      <h4>Penetrating Damp</h4>
                    </a>

                  </div>
              </div>
            <div class="col-md-3 col-sm-12">
                  <div class="service-box">
                    <a href="timber-treatments"><img src="assets/theme/images/homebox4.jpg" class="img-responsive">
                      <h4>Timber Treatments</h4>
                    </a>

                  </div>
              </div>
        </div>
    </div>
</div>

<div class="client-section">
    <div class="carousel-reviews broun-block">
        <div class="container"> 
                <div class="col-md-12 ">
                    <h1>What people say about us</h1>
                    <?php echo getReviewsHome(); ?>
              </div>
        </div>
    </div>
</div>




