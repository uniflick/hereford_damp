<?php if($thumb): ?>
    <div class="page-banner hidden-xs">
        <img src="<?php echo $thumb; ?>" class="img-responsive">
    </div>
<?php endif; ?>
<div class="page-banner visible-xs"><img src="assets/theme/images/about-m.jpg" class="img-responsive"></div>

<div class="page-content">
    <?php echo $content; ?>
</div>
<div class="client-section">
    <div class="carousel-reviews broun-block">
        <div class="container">
            <div class="col-md-7 col-md-offset-5">
                <h1>What people say about us</h1>
                <?php echo getReviewsHome(); ?>
            </div>
        </div>
    </div>
</div>