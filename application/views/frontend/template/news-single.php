<div class="container">
    <div class="page-title">
        <h1><?php echo $post_title; ?></h1>
    </div>
    <img src="<?php echo getPhoto($thumb); ?>" class="img-responsive" />
    <div class="post-content"><?php echo $content; ?></div>
</div>