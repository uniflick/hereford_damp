<?php if($thumb): ?>
<div class="page-banner hidden-xs">   
    <img src="<?php echo $thumb; ?>" class="img-responsive">
</div>
<?php endif; ?>

<div class="page-content">
    <div class="container">
        <h1>Photo Gallery</h1>
        <?php echo $content;?>
        <div class="row"><?php echo getGallery(); ?></div>
    </div>
</div>


<script src="assets/lib/plugins/lightbox/js/lightbox-plus-jquery.min.js"></script>;
<script> jQuery.noConflict();
    lightbox.option({ 'resizeDuration': 200, 'wrapAround': true  }) ;
</script>
