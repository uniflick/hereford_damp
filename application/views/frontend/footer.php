<div class="footer">
  <section class="footer-middle">
    <div class="container">
    <div class="row">
      <div class="col-md-6 col-sm-6 col-xs-12">
        <h3>Get in touch</h3>
          <form class="getInTouch" id="contactForm" method="post" onsubmit="return getInTouch(event);">
          <fieldset>
            <div class="row pb15">
              <div class="col-md-6">
                <input id="" name="" type="text" placeholder="Name" class="form-control input-md" required="">
              </div>
              <div class="col-md-6">
                <input id="" name="" type="text" placeholder="Email" class="form-control input-md" required="">
              </div>
            </div>
            <div class="row pb15 ">
              <div class="col-md-12">
                <input id="" name="" type="text" placeholder="Telephone" class="form-control input-md" required="">
              </div>
            </div>
            <div class="row pb15">
              <div class="col-md-12">
                <textarea class="form-control" id="" name="Message">Message</textarea>
              </div>
            </div>
            <p><a class="btn btn-primary pull-right" href="contact-us">SUBMIT</a></p>
          </fieldset>
        </form>
      </div>
      <div class="col-md-offset-1 col-md-5 col-sm-6 col-xs-12">
            <h3>Contact & Info</h3>
            <p class="f_phone"><i class="fa fa-phone"></i> Office: <a href="tel:<?php echo getSettingItem('PhoneNumber') ?>"><?php echo getSettingItem('PhoneNumber') ?></a></p>
            <p class="f_mobile"><i class="fa fa-mobile-phone"></i> Mobile: <a href="tel:<?php echo getWidget('Mobile')?>"><?php echo getWidget('Mobile')?></a></p>
            <p class="f_email"><i class="fa fa-envelope"></i> <a href="mailto:<?php echo getWidget('Email')?>"><?php echo getWidget('Email')?></a></p>
            <?php echo viewSocialLinksImg(); ?>
      </div>
    </div>
    </div>
  </section>
  <section class="copyright">
    <div class="container">
      <div class="row">
        <div class="col-md-8 col-sm-6 col-xs-12">
          <div class="text-left">Copyright © <?php echo date('Y') . ' ' . getSettingItem('SiteTitle') ?>. | All Rights Reserved.</div>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="text-right"><a target="_blank" href="//www.flickmedialtd.com/">Web Design Company</a> : Flick Media</div>
        </div>
      </div>
    </div>
  </section>
</div>
<script src="assets/theme/js/owl.carousel.min.js"></script> 
<script src="assets/theme/js/scripts.js"></script> 
<script src="assets/lib/common.js"></script> 
<script src="assets/lib/plugins/lightbox/js/lightbox-plus-jquery.min.js"></script> 
<script src="assets/lib/loader/jquery.loading.min.js" type="text/javascript"></script>



<script>
    function getInTouch(e) {
        e.preventDefault();
        var formData = jQuery('#getInTouch').serialize();
        var error = 0;
        var name = jQuery('#cf_name').val();
        if (!name) {
            jQuery('#cf_name').addClass('required');
            error = 1;
        } else {
            jQuery('#cf_name').removeClass('required');
        }
        var email = jQuery('#cf_email').val();
        if (!email) {
            jQuery('#cf_email').addClass('required');
            error = 1;
        } else {
            jQuery('#cf_email').removeClass('required');
        }
        var phone = jQuery('#cf_contact_no').val();
        if (!name) {
            jQuery('#cf_contact_no').addClass('required');
            error = 1;
        } else {
            jQuery('#cf_contact_no').removeClass('required');
        }


        if (!error) {
            jQuery.ajax({
                type: "POST",
                url: "mail/contact_us",
                dataType: 'json',
                data: formData,
                beforeSend: function () {
                    jQuery('#cf_ajax_respond').html('<p class="ajax_processing">Sending...</p>');
                },
                success: function (jsonData) {
                    jQuery('#cf_ajax_respond').html(jsonData.Msg);
                    if (jsonData.Status === 'OK') {
                        document.getElementById("contactForm").reset();
                        setTimeout(function () {
                            $('#cf_ajax_respond').html('');
                            $('#cf_ajax_respond').css('display', 'block');
                        }, 2000);
                    }
                }
            });
        }
    }

</script>


<?php echo cookie(); ?>
</body>
</html>