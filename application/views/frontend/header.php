<!doctype html>
<html lang="en">
    <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="<?php echo ($GLOBALS['General']['HideFromSearchEngine'] == 'No')?'index, follow':'noindex, nofollow'; ?>">
    <title>
    <?php
            $title = isset($title) ? $title . ' | ' : '';
            echo (isset($meta_title) && !empty($meta_title)) ? $meta_title : $title . getSettingItem('SiteTitle'); ?>
    </title>
    <meta name="description" content="<?php echo isset($meta_description) ? $meta_description : ''; ?>" />
    <meta name="keywords" content="<?php echo isset($meta_keywords) ? $meta_keywords : ''; ?>" />
    <base href="<?php echo base_url(); ?>" />
    <link rel="icon" href="assets/theme/images/favicon.png">
    <link href="https://fonts.googleapis.com/css?family=Fjalla+One&display=swap&subset=latin-ext" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="assets/lib/bootstrap/css/bootstrap.min.css" type='text/css' media='all' />
    <link rel="stylesheet" href='assets/lib/font-awesome/font-awesome.min.css' type='text/css' media='all' />
    <link rel="stylesheet" href="assets/theme/css/layout.css">
    <link rel="stylesheet" href="assets/theme/css/style.css">
    <link rel="stylesheet" href="assets/theme/css/responsive.css">
    <link rel="stylesheet" href="assets/lib/ajax.css">
    <link rel="stylesheet" href="assets/lib/loader/jquery.loading.min.css">
    <link rel="stylesheet" type="text/css" href="assets/theme/css/owl.carousel.min.css">
    <link rel="stylesheet" type="text/css" href="assets/theme/css/owl.theme.default.min.css">
    <script src="assets/lib/plugins/jQuery/jquery-2.2.3.min.js" type="text/javascript"></script>
    <script src="assets/lib/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <link href="assets/lib/plugins/cycle2/default.css" rel="stylesheet" type="text/css" />
    <script src="assets/lib/plugins/cycle2/jquery.cycle2.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/holder/2.9.6/holder.min.js" type="text/javascript"></script>
    <?php if($GLOBALS['General']['GoogleAnalyticsCode']): ?>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=<?php echo $GLOBALS['General']['GoogleAnalyticsCode']; ?>"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());
        gtag('config', '<?php echo $GLOBALS['General']['GoogleAnalyticsCode']; ?>');
    </script>
    <?php endif; ?>
    </head>

    <body>
<?php if (in_array(getLoginUserData('role_id'), [1, 2])) : ?>
<section class="admin-menu">
      <nav class="navbar navbar-default">
    <div class="container-fluid"> 
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
        <a class="navbar-brand" href="#"><?php echo getSettingItem('SiteTitle'); ?></a> </div>
          
          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav">
              <?php
                                if (isset($edit_url)) {
                                    echo '<li><a href="' . $edit_url . '"><i class="fa fa-edit"></i> Edit</a></li>';
                                }
                                ?>
              <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-plus"></i> New <span class="caret"></span></a>
            <ul class="dropdown-menu">
                  <li><a href="<?php echo site_url(Backend_URL . 'cms/create') ?>">Page</a></li>
                  <li><a href="<?php echo site_url(Backend_URL . 'cms/new_post') ?>">Post</a></li>
                </ul>
          </li>
            </ul>
        <ul class="nav navbar-nav navbar-right">
              <li><a href="<?php echo site_url(Backend_URL); ?>">Hello, <?php echo getUserNameByID(getLoginUserData('user_id')); ?></a></li>
            </ul>
      </div>
          <!-- /.navbar-collapse --> 
        </div>
    <!-- /.container-fluid --> 
  </nav>
</section>
<?php endif; ?>

<section class="top_header">
   <div class="container">
        <div class="pull-right">
            <div class="top-email pull-left"><i class="fa fa-envelope"></i> <a href="mailto:<?php echo getWidget('Email')?>"><?php echo getWidget('Email')?></a></div>
            <div class="top-mobile pull-left"><i class="fa fa-mobile-phone"></i> <a href="tel:<?php echo getWidget('Mobile')?>"><?php echo getWidget('Mobile')?></a></div>
            <div class="top-phone pull-left"><i class="fa fa-phone"></i> <a href="tel:<?php echo getSettingItem('PhoneNumber') ?>"><?php echo getSettingItem('PhoneNumber') ?></a></div>
        </div>
    </div>
</section>

<section class="header">
      <div class="container">
    <div class="row">
          <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="logo"> <a href="<?php echo site_url(); ?>"> <img src="assets/theme/images/logo.png" class="img-responsive"> </a> </div>
      </div>
      
          <div class="col-md-9 col-sm-12 col-xs-12 no-padding">
          
        	<nav class="navbar navbar-primary no-padding pull-right">
              
            	<div class="row">
                  <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar"> 
                        <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> 
                    </button>
              	  </div>
                 	 <?php echo getNavigationMenu(1); ?> 
                </div>
          	
           </nav>
          
      </div>
    </div>
  </div>
</section>
