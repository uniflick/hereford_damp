        </div>
    </div> 
    <!-- Body Content End -->

    <!-- ./wrapper -->

    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            Loading Time <b>{elapsed_time}</b> seconds. Flick CMS v1.0 & CI v<?php echo CI_VERSION; ?>      
        </div>
        <b>Copyright &copy; 2008-<?php echo date('Y'); ?> <a target="_blank" href="http://flickmedialtd.com/">flickmedialtd.com</a>.</b> All rights reserved.
    </footer>

 
    <!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>

<!--<script type="text/javascript" src="assets/lib/validator.min.js"></script>-->
<script src="assets/lib/plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- AdminLTE App -->
<script src="assets/admin/dist/js/app.min.js"></script>

<!-- AdminLTE for demo purposes -->
<script src="assets/admin/dist/js/demo.js"></script>
<script src="assets/lib/common.js" type="text/javascript"></script>
<script>
    jQuery(document).ready(function() {
        jQuery('.js_datepicker').datepicker({
            format: "yyyy-mm-dd",
            autoclose: true,
            todayHighlight: true            
        });                      
    });
</script>
</body>
</html>