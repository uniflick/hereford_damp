<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Frontend_controller extends MX_Controller {

    public $user_id;
    public $role_id;

    public function __construct() {
        parent::__construct();        

        $this->user_id = getLoginUserData('user_id');
        $this->role_id = getLoginUserData('role_id');
    }

    public function index() {
        $PageSlug   = empty($this->uri->segment(1)) ? 'home' : $this->uri->segment(1);

        $cms        = $this->db->get_where('cms', ['post_url' => $PageSlug])->row_array();
        $post_type  = $cms['post_type'];
        
        //dd( $GLOBALS );

        $categorySlug = $this->uri->segment(2);
        if ($PageSlug === 'category' && !empty($categorySlug)) {
            $this->getCategoryPage($categorySlug, $cms);
        } elseif ($post_type == 'page') {
            $this->getCmsPage($cms, $PageSlug);
        } elseif ($post_type == 'post') {
            $this->news_detail($cms);
        } else {
            $this->viewFrontContent('frontend/404', $cms);
        }
    }

    public function signin(){
        $this->viewFrontContent('frontend/template/login');
    }
    
    public function reset_pass(){        
        $this->viewFrontContent('frontend/template/reset_pass' );
    }
    
    private function getCategoryPage($categorySlug = '') {
        $category = $this->db->get_where('cms_options', ['url' => $categorySlug, 'type' => 'category'])->row();
        $page = (int) $this->input->get('p');
        $limit = 5;
        $start = startPointOfPagination($limit, $page);
        $targetpath = build_pagination_url('category/' . $categorySlug, 'p', true);

        $total = $this->db->get_where('cms', ['post_type' => 'post', 'parent_id' => $category->id])->num_rows();
        $this->db->limit($limit, $start);
        $this->db->order_by('id', 'DESC');
        $post_data = $this->db->get_where('cms', ['post_type' => 'post', 'parent_id' => $category->id])->result();

        //$post_data = $this->db->order_by('id', 'DESC')->get_where('cms', ['post_type' => 'post', 'parent_id' => $category->id])->result();

        if ($category) {
            if ($category->template) {
                $PageSlug = substr($category->template, 0, -4);
            }

            $viewTeamplatePath = APPPATH . '/views/frontend/category_template/' . $PageSlug . '.php';

            $viewPath = (file_exists($viewTeamplatePath)) ? ('category_template/' . $PageSlug) : 'category_template/default';

            $cms_page = [];
            $cms_page['posts_data'] = $post_data;

            $cms_page['total'] = $total;
            $cms_page['targetpath'] = $targetpath;
            $cms_page['limit'] = $limit;
            $cms_page['page'] = $page;

            $cms_page['category_id'] = $category->id;
            $cms_page['category_thumb'] = $category->thumb;
            $cms_page['category_name'] = $category->name;
            $cms_page['category_parent_id'] = $category->url;
            $cms_page['category_description'] = $category->description;

            $this->viewFrontContent('frontend/' . $viewPath, $cms_page);
        } else {
            $this->viewFrontContent('frontend/404');
        }
    }

    private function getCmsPage($cms, $PageSlug = '') {
        if ($cms['template']) {
            $PageSlug = substr($cms['template'], 0, -4);
        }

        $viewTeamplatePath = APPPATH . '/views/frontend/template/' . $PageSlug . '.php';
        $viewPath = (file_exists($viewTeamplatePath)) ? ('template/' . $PageSlug ) : 'template/page';

        $data = [
            'id' => $cms['id'],
            'title' => $cms['post_title'],
            'content' => $cms['content'],
            'meta_title' => $cms['seo_title'],
            'meta_description' => getShortContent($cms ['seo_description'], 120),
            'meta_keywords' => $cms['seo_keyword'],
            'parent_id' => $cms['parent_id'],
            'thumb' => $cms['thumb'],
            'edit_url' => site_url(Backend_URL.'cms/update/'.$cms['id']),
            //'General' => $cms['General'],
            //'Widget' => $cms['Widget']
        ];

        $this->viewFrontContent('frontend/' . $viewPath, $data);
    }
   
    public function viewFrontContent($view, $data = []) {
        $GLOBALS    = $this->getAllSetting();
        $this->load->view('frontend/header', $data);
        $this->load->view($view, $data);
        $this->load->view('frontend/footer');
    }

    private function getAllSetting(){
        $this->db->select('category,label,value');
        $settings = $this->db->get('settings')->result();
        $return = [];
        foreach($settings as $setting ){
            $return[$setting->category][$setting->label] = $setting->value;
        }
        return $return;
    } 
    
    /*
    private function viewMemberContent($view, $data = []) {
        $data['widget'] = json_decode(getSettingItem('Widget'));
        $active_tab = $this->input->get('tab');

        $this->load->view('frontend/header', $data);
        $this->load->view('my_account/menu', ['active' => $active_tab]);
        $this->load->view($view, $data);
        $this->load->view('frontend/footer');
    }     
    */
    
}
