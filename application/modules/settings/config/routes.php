<?php defined('BASEPATH') OR exit('No direct script access allowed');

$route['admin/settings']                  = 'settings';
$route['admin/settings/create']           = 'settings/create';
$route['admin/settings/create_action']    = 'settings/create_action';
$route['admin/settings/update']           = 'settings/update';


