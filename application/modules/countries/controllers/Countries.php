<?php defined('BASEPATH') OR exit('No direct script access allowed');
 
/* Author: Khairul Azam
 * Date : 2016-11-16
 */

class Countries extends Admin_controller{
    function __construct(){
        parent::__construct();
        $this->load->model('Countries_model');
        $this->load->helper('countries');
        $this->load->library('form_validation');
    }

    public function index(){
        $country    = intval($this->input->get('country', TRUE));
        $state      = intval($this->input->get('state', TRUE));
        
        $start      = intval($this->input->get('start'));
        
        if ($country || $state || $start ) {
            $config['base_url'] = Backend_URL . 'countries/?country='.$country.'&state='.$state;
            $config['first_url'] = Backend_URL . 'countries/?country='.$country.'&state='.$state;
        } else {
            $config['base_url'] = Backend_URL . 'countries/';
            $config['first_url'] = Backend_URL . 'countries/';
        }

        $config['per_page'] = 25;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Countries_model->total_rows($country, $state);
        $countries = $this->Countries_model->get_limit_data($config['per_page'], $start, $country, $state);

        
        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'countries_data' => $countries,
            'pagination'    => $this->pagination->create_links(),
            'total_rows'    => $config['total_rows'],
            'start'         => $start,
            'country'       => $country,
            'state'         => $state            
        );
        $this->viewAdminContent('countries/index', $data);
    }
    
    public function create(){
        $data = array(
            'button' => 'Create',
            'action' => site_url( Backend_URL . 'countries/create_action'),
	    'id' => set_value('id'),
	    'parent_id' => set_value('parent_id'),
	    'name' => set_value('name'),
	    'type' => set_value('type'),
	    'status' => set_value('status'),
	);
        $this->viewAdminContent('countries/form', $data);
    }
    
    public function create_action(){
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'parent_id' => $this->input->post('parent_id',TRUE),
		'name'      => $this->input->post('name',TRUE),
		'type'      => $this->input->post('type',TRUE),
		'status'    => $this->input->post('status',TRUE),
	    );

            $this->Countries_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url( Backend_URL. 'countries'));
        }
    }
    
    public function update($id){
        $row = $this->Countries_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button'    => 'Update',
                'action'    => site_url( Backend_URL . 'countries/update_action'),
		'id'        => set_value('id', $row->id),
		'parent_id' => set_value('parent_id', $row->parent_id),
		'name'      => set_value('name', $row->name),
		'type'      => set_value('type', $row->type),
		'status'    => set_value('status', $row->status),
	    );
            $this->viewAdminContent('countries/form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url( Backend_URL. 'countries'));
        }
    }
    
    public function update_action(){
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'parent_id' => $this->input->post('parent_id',TRUE),
		'name' => $this->input->post('name',TRUE),
		'type' => $this->input->post('type',TRUE),
		'status' => $this->input->post('status',TRUE),
	    );

            $this->Countries_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url( Backend_URL. 'countries'));
        }
    }
    
    public function delete($id){
        $row = $this->Countries_model->get_by_id($id);

        if ($row) {
            $this->Countries_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url( Backend_URL. 'countries'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url( Backend_URL. 'countries'));
        }
    }

    public function _rules(){
	$this->form_validation->set_rules('parent_id', 'parent id', 'trim|required');
	$this->form_validation->set_rules('name', 'name', 'trim|required');	
	$this->form_validation->set_rules('status', 'status', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }
    
    public function location_list() {
        ajaxAuthorized();
        
        $country_id = $this->input->post('country_id', TRUE);
        $state_id   = $this->input->post('state_id', TRUE);
        $city_id    = $this->input->post('city_id', TRUE);
        $type       = $this->input->post('type', TRUE);
        
        $this->db->where('type', $type);    
        
        if($state_id){
            $this->db->where('parent_id', $state_id);
        }elseif($country_id){
            $this->db->where('parent_id', $country_id);
        }

        $results = $this->db->get('countries')->result();
                
        $options = '';
        if (count($results) == 0) {
            $options .= '<option value="0">-- No Found --</option>';
        } elseif($state_id == 0) {
            $options .= '<option value="0">-- Select State/Region --</option>';
        } else{
            $options .= '<option value="0">-- Select City --</option>';
        }

        foreach ($results as $result) {
            $options .= '<option value="' . $result->id . '" ';
            $options .= '>' . $result->name . '</option>';
        }
        echo $options;
    }
    
}