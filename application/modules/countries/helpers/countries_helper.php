<?php defined('BASEPATH') OR exit('No direct script access allowed');

function getLocationListX( $selected = 0, $type = 0, $parent_id = 0 ) {
    $ci = & get_instance();
    
    $ci->db->where('type', $type);    
    if($parent_id){ $ci->db->where('parent_id', $parent_id); }    
    $results = $ci->db->get('countries')->result();
    
    $options = '';
    foreach ($results as $row) {
        $options .= '<option value="' . $row->id . '" ';
        $options .= ($row->id == $selected ) ? 'selected="selected"' : '';
        $options .= '>' . $row->name . '</option>';
    }
    return $options;
}

function getCountryTree( $parent_id = 0, $selected = 0 ) {
    $ci = & get_instance();            
    if($parent_id){
        $ci->db->where('parent_id', $parent_id);
    }    
    $results = $ci->db->get('countries')->result();

    $list = array();    
    foreach($results as $row ){
        $list[$row->parent_id][] = array(
            'id'        => $row->id, 
            'name'      => $row->name, 
            'parent_id' => $row->parent_id
        );
    }        
    return buildParentLocationTree($list,$parent_id,0,$selected);        
}

function buildParentLocationTree($array_data,$parentID = 0, $level = 0, $selected =0 ){
      
    $output = '';
    if(isset($array_data[$parentID]) && count($array_data[$parentID])){ 
        $type_id = $level + 2;
        foreach($array_data[$parentID] as $child) {
            $output .= '<option value="'.$child['id'].'" data-type="'. $type_id .'"';
            $output .= ($selected == $child['id']) ? ' selected="selected">' : '>';                        
            $output .=  bildPagentLocationHelper($level);            
            $output .=  $child['name'];            
            $output .= '</option>';
            $output .= buildParentLocationTree($array_data, $child['id'], $level+1, $selected);                        
        }        
    }    
    return $output;
}

function bildPagentLocationHelper( $label = 1){
    if($label == 1){
        return '&nbsp;&nbsp;&nbsp;|_&nbsp;';
    } elseif($label == 2){
        return '&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;|_&nbsp;';
    }elseif($label == 3){
        return '&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;|_&nbsp;';
    } else {
        return '';
    }
}

function getLocationType($type = 0){
     if($type == 1){
         return 'Country';
     }elseif ($type == 2) {
         return 'State/Region';
    }elseif ($type == 3) {
        return 'City/County';
    }elseif ($type == 4) {
        return 'Area';
    }else{
        return 'Unknown';
    }
 }