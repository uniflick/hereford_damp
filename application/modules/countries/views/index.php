<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<section class="content-header">
    <h1> Country/Location  <small>Control panel</small></h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo Backend_URL; ?>"><i class="fa fa-dashboard"></i> Admin</a></li>
        <li class="active">Countries</li>
    </ol>
</section>

<section class="content">
    <div class="row">    
        <div class="col-sm-4 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <i class="fa fa-user-plus" aria-hidden="true"></i> Add New
                    </h3>
                </div>

                <div class="panel-body">                   
                    <form action="<?php echo Backend_URL; ?>countries/create_action" method="post">
                        <input type="hidden" name="type" id="type_id" value="1"/>
                        <div class="form-group">
                            <label for="parent_id">Select Parent:</label>
                            <select class="form-control js_select2" name="parent_id" id="parent_id">
                                <option value="0">--Root Country--</option>
                                <?php echo getCountryTree(0);?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="name">Name:</label>
                            <input type="text" class="form-control" name="name" id="name" placeholder="Name" />
                        </div>
                        
                        <div class="form-group">
                            <label for="status">Status: </label>
                            <?php echo htmlRadio('status', 'Active', array(
                                'Active' => 'Publish', 'Inactive' => 'Draft'
                            ));?>
                        </div>                        
                        <button type="submit" class="btn btn-primary">Save New</button> 
                        <button type="reset" class="btn btn-default">Reset</button>                         
                    </form>
                </div>		
            </div>
        </div>
        
        <div class="col-sm-8 col-xs-12">
            <div class="panel panel-default">            
                <div class="panel-heading" style="height: 55px">					 
                    <div class="col-md-12 no-padding">
                        <form action="" method="get">
                            <div class="row">                                                                    
                                <div class="col-md-5">                                                                                                            
                                    <div class="input-group">
                                        <span class="input-group-addon">Filter:</span>
                                        <select class="form-control" id="country" name="country" onChange="get_state_list();">
                                            <option value="0">-- Any Country --</option>
                                            <?php echo getLocationList($country, 1); ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <select class="form-control" id="state" name="state" onChange="get_city_list();">
                                        <option value="0">--Any Region--</option>
                                        <?php echo getLocationList( $state, 2, $country ); ?>
                                    </select>
                                </div>                                
                                <div class="col-md-3 no-padding">
                                    <button type="submit" class="btn btn-primary"><i class="fa fa-filter"></i> Filter</button>
                                    <a href="<?php echo Backend_URL.'countries'; ?>" class="btn btn-default">Reset</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="panel-body">
                    <?php if($countries_data){ ?>
                    <table class="table table-striped table-bordered table-condensed" style="margin-bottom: 10px">
                        <thead>
                            <tr>
                                <th width="40">SL</th>
                                <th>Name</th>
                                <th>Type</th>
                                <th>Status</th>
                                <th width="90">Action</th>
                            </tr>
                        </thead>
                        
                        <?php foreach ($countries_data as $countries) { ?>
                            <tr>
                                <td><?php echo ++$start ?></td>
                                <td><?php echo $countries->name ?></td>
                                <td><?php echo getLocationType($countries->type); ?></td>
                                <td><?php echo $countries->status ?></td>
                                <td>
                                    <?php                                    
                                    echo anchor(
                                            site_url(Backend_URL . 'countries/update/' . $countries->id), 
                                            '<i class="fa fa-fw fa-edit"></i>', 
                                            'class="btn btn-xs btn-primary" title="Edit/Update"'
                                        );
                                    
                                    echo anchor(
                                            site_url(Backend_URL . 'countries/delete/' . $countries->id), 
                                            '<i class="fa fa-fw fa-trash"></i>', 
                                            'class="btn btn-xs btn-danger" title="Delete" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'
                                        );
                                    ?>
                                </td>
                            </tr>
                        <?php } ?>
                    </table>

                    <div class="row" style="padding-top: 10px;">
                        <div class="col-md-3">
                            <span class="btn btn-primary">Total Record : <?php echo $total_rows ?></span>
                        </div>
                        <div class="col-md-9 text-right">
                            <?php echo $pagination ?>
                        </div>
                    </div>
                    
                    <?php }else{ echo '<p class="ajax_notice">Data not found!</p>'; } ?>
                    
                </div>
            </div>
        </div>
    </div>
</section>
<?php load_module_asset('countries', 'js'); ?>
