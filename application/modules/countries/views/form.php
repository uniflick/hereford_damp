<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<section class="content-header">
    <h1> Country  <small><?php echo $button ?></small> <a href="<?php echo site_url('admin/countries') ?>" class="btn btn-default">Back</a> </h1>
    <ol class="breadcrumb">
        <li><a href="admin/"><i class="fa fa-dashboard"></i> Admin</a></li>
        <li><a href="admin/countries">Countries</a></li>
        <li class="active">Update</li>
    </ol>
</section>

<section class="content">       
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Update</h3>
        </div>

        <div class="box-body">
            <form class="form-horizontal" action="<?php echo $action; ?>" method="post">
                <input type="hidden" name="type" id="type_id" value="1"/>
                <div class="form-group">
                    <label  class="col-sm-2 control-label" for="parent_id">Select Parent:</label>
                    <div class="col-sm-10">
                        <select class="form-control js_select2" name="parent_id" id="parent_id">
                            <option value="0">--Root Country--</option>
                            <?php echo getCountryTree(0, $parent_id); ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">Name :</label>
                    <div class="col-sm-10">                    
                        <input type="text" class="form-control" name="name" id="name" placeholder="Name" value="<?php echo $name; ?>" />
                        <?php echo form_error('name') ?>
                    </div>
                </div>	    
                <div class="form-group">
                    <label for="status" class="col-sm-2 control-label">Status :</label>
                    <div class="col-md-10">
                    <?php
                    echo htmlRadio('status', 'Active', array(
                        'Active' => 'Publish', 'Inactive' => 'Draft'
                    ));
                    ?>
                    </div>
                </div>
                <div class="col-md-12 text-right">
                    <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
                    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
                    <a href="<?php echo site_url('admin/countries') ?>" class="btn btn-default">Cancel</a>
                </div>
            </form>
        </div>
    </div>
</section>
<?php load_module_asset('countries', 'js'); ?>