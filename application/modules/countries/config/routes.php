<?php defined('BASEPATH') OR exit('No direct script access allowed');

$route['admin/countries']                  = 'countries';
$route['admin/countries/create']           = 'countries/create';
$route['admin/countries/update/(:any)']    = 'countries/update/$1';
$route['admin/countries/read/(:any)']      = 'countries/read/$1';
$route['admin/countries/delete/(:any)']    = 'countries/delete/$1';
$route['admin/countries/create_action']    = 'countries/create_action';
$route['admin/countries/update_action']    = 'countries/update_action';

$route['admin/countries/location_list']    = 'countries/location_list';


