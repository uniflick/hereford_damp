<script>
        
    $(document).ready(function(){
        $('#parent_id').on('change',function(){
            var value = $(this).find(':selected').data('type');             
            $('#type_id').val(value);                      
        });        
    });
    
    function get_state_list() {
        var type        = '2';
        var country     = $('#country').val();
        var state       = $('#state').val();
        var city        = $('#city').val();
        
        jQuery.ajax({
            url: 'admin/countries/location_list',
            type: "POST",
            dataType: "text",
            data: { country_id: country, state_id: state, city: city, type: type },
            beforeSend: function () {
                jQuery('#state').html('<option value="0">Loading...</option>');
            },
            success: function (response) {
                jQuery('#state').html(response);
            }
        });
        //get_city_list();
    }
    function get_city_list() {
        var type        = '3';
        var country     = $('#country').val();
        var state       = $('#state').val();
        var city        = $('#city').val();
        
        jQuery.ajax({
            url: 'admin/countries/location_list',
            type: "POST",
            dataType: "text",
            data: { country_id: country, state_id: state, city_id: city, type: type },
            beforeSend: function () {
                jQuery('#city').html('<option value="0">Loading...</option>');
            },
            success: function (response) {
                jQuery('#city').html(response);
            }
        });
    }
</script>