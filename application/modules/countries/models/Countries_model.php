<?php 
defined('BASEPATH') OR exit('No direct script access allowed');


class Countries_model extends Fm_model
{

    public $table = 'countries';
    public $id = 'id';
    public $order = 'ASC';

    function __construct(){
        parent::__construct();
    }
  
    
    // get total rows
    function total_rows($country = 0, $state = 0) {
        
        if($state){
            $this->db->where('parent_id', $state);
            $this->db->where('type', 2);
        }elseif($country){
            $this->db->where('parent_id', $country);
            $this->db->where('type', 2);
        }                 
        
	$this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $country = 0, $state = 0) {        
        $this->db->order_by('name', 'ASC');
        if($state){
            $this->db->where('parent_id', $state);
            $this->db->where('type', 3);
        }elseif($country){
            $this->db->where('parent_id', $country);
            $this->db->where('type', 2);
        }                        
	$this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

}