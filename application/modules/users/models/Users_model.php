<?php 
defined('BASEPATH') OR exit('No direct script access allowed');


class Users_model extends Fm_model
{

    public $table = 'users';
    public $id = 'id';
    public $order = 'ASC';

    function __construct(){
        parent::__construct();
    }

  
    
    // get total rows
    function total_rows($q = NULL , $status = NULL , $role_id = 0) {
        if($q){                        
            $this->db->where("(`first_name` LIKE '%{$q}%' ESCAPE '!' "
                . "OR `last_name` LIKE '%{$q}%' ESCAPE '!' "
                . "OR `email` LIKE '%{$q}%' ESCAPE '!' "
                . "OR `contact` LIKE '%{$q}%' ESCAPE '!' "
                . "OR `city` LIKE '%{$q}%' ESCAPE '!' "
                . "OR `state` LIKE '%{$q}%' ESCAPE '!' "
                . "OR `postcode` LIKE '%{$q}%' ESCAPE '!')");
        }
        
        if($role_id != 0){
            $this->db->where('role_id', $role_id );
        }
        
        if($status){
            $this->db->where('status', $status);
        }
	$this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL , $status = NULL , $role_id = 0) {
        $this->db->order_by($this->id, $this->order);
        
        if($q){            
            $this->db->where("(`first_name` LIKE '%{$q}%' ESCAPE '!' "
            . "OR `last_name` LIKE '%{$q}%' ESCAPE '!' "
            . "OR `email` LIKE '%{$q}%' ESCAPE '!' "
            . "OR `contact` LIKE '%{$q}%' ESCAPE '!' "
            . "OR `city` LIKE '%{$q}%' ESCAPE '!' "
            . "OR `state` LIKE '%{$q}%' ESCAPE '!' "
            . "OR `postcode` LIKE '%{$q}%' ESCAPE '!')");
        }
        
        if($role_id != 0){
            $this->db->where('role_id', $role_id );
        }
        
        if($status){
            $this->db->where('status', $status);
        }
        
	$this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }
    
}