<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php load_module_asset('users', 'css'); ?>
<?php load_module_asset('gallery', 'css'); ?>
<section class="content-header">
    <h1>Photo Gallery</h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo Backend_URL ?>"><i class="fa fa-dashboard"></i> Admin</a></li>                
        <li class="active">Gallery</li>
    </ol>
</section>

<section class="content">
    <?php echo makeGalleryTab('gallery/ordering'); ?>
    <div class="box no-border">
        <div class="box-header with-border">
            <div class="row">
                <div class="col-md-6"><h3 class="box-title">Photo List</h3></div>
                <div class="col-md-6 text-right">
                    <button class="btn btn-primary" onclick="save_order();">
                        <i class="fa fa-save"></i> Save Order
                    </button>
                </div>
            </div>            
        </div>
        <div class="box-body">
            <div id="ajaxRespond"></div>            
            <ul id="grid-view">
                <?php  foreach ($photos as $photo): ?>
                    <li id="ids_<?php echo $photo->id; ?>">                        
                        <img src="<?php echo getPhoto($photo->photo); ?>" class="img-responsive"/>
                        <?php echo getShortContent($photo->title, 10); ?>                    
                    </li>
                <?php endforeach; ?>
            </ul>                        
        </div> 
        <div class="box-footer">
            <div class="row">
                <div class="col-md-6">
                    <p class="text-red text-bold">Max Photo Order Capacity 500</p>
                </div>
                <div class="col-md-6 text-right">
                    <button class="btn btn-primary" onclick="save_order();">
                        <i class="fa fa-save"></i> Save Order
                    </button>
                </div>
            </div>   
            
        </div>
    </div>
</section>
<script>
    
    
$(document).ready(function(){        
    $("ul#grid-view").sortable({ tolerance: 'pointer', cursor: "move" });
    $("ul#grid-view").disableSelection();       
});

function save_order(){
    var data = $('#grid-view').sortable('serialize');
    $.ajax({
        data: data,
        type: 'POST',
        url: 'admin/gallery/save_order',
        success: function(respond) {
            $('#ajaxRespond').html(respond);
        }
    });
}
</script>
