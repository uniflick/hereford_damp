<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    load_module_asset('gallery', 'css');
?>
<section class="content-header">
    <h1>Photo Gallery <small>Upload Photo</small> </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo Backend_URL; ?>"><i class="fa fa-dashboard"></i> Admin</a></li>
        <li><a href="<?php echo Backend_URL; ?>gallery"><i class="fa fa-dashboard"></i> Gallery</a></li>
        <li class="active">Add Photo</li>
    </ol>
</section>

<section class="content">
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Upload New Photo</h3>
        </div>
        <form class="form-horizontal" action="<?php echo $action; ?>"
                method="post" enctype="multipart/form-data">
            <div class="box-body">
                <div id="ajax_respon"></div>
                            
                <div class="form-group">      
                    <div class="col-md-2 control-label text-bold" for="album_id">(Optional)Select Album</div>
                    <div class="col-md-8">
                        <select name="album_id" id="album_id" class="form-control">
                            <option value="0">Select Album</option>
                            <?php echo getDropDownAlbums($album_id, 'Photo') ?>
                        </select>
                    </div>
                </div>



                <div class="form-group">                    
                    <div class="col-md-12">
                        <div class="upload_area">                            
                            <input type="file" accept="image/*" name="photo[]" multiple id="gallery-photo-add">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <ul class="preview row"></ul>
                    </div>
                </div>

                           
            </div>
            <div class="box-footer text-center with-border">
                <a href="<?php echo Backend_URL . 'gallery'; ?>" class="btn btn-default"> <i
                        class="fa fa-long-arrow-left"></i> Back to List</a>
                <button type="submit" class="btn btn-primary">
                    <i class="fa fa-cloud-upload"></i> 
                    Start Upload Photo
                </button>
            </div>
            </form>
    </div>
</section>
<script>
$(function() {        
    var imagesPreview = function(input, PreviewBeforeUpload){
        if (input.files) {
            var filesAmount = input.files.length;
            for (i = 0; i < filesAmount; i++) {
                var reader = new FileReader();
                reader.onload = function(event){                                        
                   var img = `<li>
                                <img src="${event.target.result}"/>
                                <b class="btn btn-danger btn-xs remove">x</b>
                            </li>`;                
                   $( img ).appendTo( PreviewBeforeUpload );                
                },
                reader.readAsDataURL(input.files[i]);
            }
        }
    };

    $('#gallery-photo-add').on('change', function() {
        imagesPreview(this, 'ul.preview');
    });        
});

$(document).on('click', 'b.remove', function(){    
    $(this).parents('li').remove();
});

$(document).on('submit', 'form', function(){   
    var imgs = [];
    $('ul.preview li').each(function( ){
        imgs.push(  $(this).find('img').attr('src') );
    });
    //console.log( imgs );
   
    $.ajax({
        url: "<?php echo Backend_URL; ?>gallery/multi_action",
        type: "POST",
        data: imgs.toString(),
        dataType: 'html',
        beforeSend: function () {
            $('#ajax_respond')
                    .css('display', 'block')
                    .html('<p class="ajax_processing">Processing...</p>');
        },
        success: function (respond) {
            $('#ajax_respond').html(respond);            
        }
    });
    return false;
});

</script>