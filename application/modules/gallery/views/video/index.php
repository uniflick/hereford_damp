<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<section class="content-header">
    <h1>Video Gallery 
        <?php echo anchor(
            site_url(Backend_URL . 'gallery/video/add'),
            '<i class="fa fa-plus" aria-hidden="true"></i> Upload New Video',
            'class="btn btn-default"'
        ); ?>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo Backend_URL ?>"><i class="fa fa-dashboard"></i> Admin</a></li>
        <li class="active">Video Gallery</li>
    </ol>
</section>

<section class="content">
    <div class="box">

        <div class="box-header">
            <div class="row">
                <form action="<?php echo site_url(Backend_URL . 'gallery/'); ?>" class="form-inline" method="get">

                    <div class="col-md-4">
                        <div class="form-group">
                            <select name="album_id" class="form-control input-md">
                                <option value="0">All Albums</option>
                                <?php echo getDropDownAlbums($this->input->get('album_id'), 'Video'); ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-4 pull-right text-right">
                        <div class="input-group">
                            <?php $q = $this->input->get('q'); ?>
                            <input type="text" class="form-control" name="q" value="<?php echo $q; ?>">
                            <span class="input-group-btn">
                                <?php if ($q <> '') { ?>
                                    <a href="<?php echo site_url(Backend_URL . 'gallery'); ?>" class="btn btn-default">Reset</a>
                                <?php } ?>
                                <button class="btn btn-primary" type="submit">Search</button>
                            </span>
                        </div>
                    </div>

                </form>
            </div>
        </div>
        <div class="box-body">
            <table class="table table-hover table-condensed">
                <thead>
                    <tr>
                        <th width="40">#ID</th>
                        <th width="90">Photo</th>
                        <th>Title/Caption</th>
                        <th>Album</th>
                        <th width="150">Upload By</th>
                        <th width="130">Date</th>
                        <th width="150">Action</th>
                    </tr>
                </thead>
                <?php foreach ($videos as $video) { ?>
                    <tr id="row_<?php echo $video->id; ?>">
                        <td><?php echo $video->id; ?></td>
                        <td><img src="<?php if($video->type == 'Youtube'){
                            echo 'https://img.youtube.com/vi/'.$video->photo.'/0.jpg';
                        } else {
                            echo getPhoto('', $video->title, 70,70);
                        } ?>" width="100" alt=""></td>
                        <td><?php echo $video->title; ?></td>
                        <td><?php echo getAlbum($video->album_id); ?></td>
                        <td><?php echo getUserNameByID($video->user_id); ?></td>
                        <td><?php echo globalDateFormat($video->created) ?></td>
                        <td>
                            <?php echo anchor(
                                site_url(Backend_URL . 'gallery/video/edit/' . $video->id),
                                '<i class="fa fa-fw fa-edit"></i> Update',
                                'class="btn btn-xs btn-default"'
                            ); ?>
                            <span data-id="<?php echo $video->id; ?>" data-name="<?php echo $video->photo; ?>" 
                                data-album_id="<?php echo $video->album_id ?>" 
                                class="btn btn-xs btn-danger js_photo_delete">
                                <i class="fa fa-fw fa-trash"></i> Delete
                            </span>
                        </td>
                    </tr>
                <?php } ?>
            </table>

            <div class="row" style="padding: 10px 0;">
                <div class="col-md-12">
                    <div class="col-md-4">
                        <span class="btn btn-primary">Total Record : <?php echo $total_rows; ?></span>
                    </div>
                    <div class="col-md-8 text-right">
                        <?php echo $pagination; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    $('.js_photo_delete').on('click', function() {
        var id = $(this).data('id');
        var photo = $(this).data('name');
        var album_id = $(this).data('album_id');
        var yes = confirm('Are you sure?');
        if (yes) {
            $.ajax({
                url: "admin/gallery/video/delete",
                type: "POST",
                dataType: "json",
                data: {
                    id: id,
                    photo: photo,
                    album_id: album_id
                },
                beforeSend: function() {
                    $('#row_' + id).css('background-color', '#FF0000');
                },
                success: function(jsonData) {
                    if (jsonData.Status === 'OK') {
                        $('#row_' + id).fadeOut(500);
                    } else {
                        alert(jsonData.Msg);
                    }
                }
            });
        }
    });
</script>