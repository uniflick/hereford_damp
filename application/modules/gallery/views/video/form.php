<?php
defined('BASEPATH') or exit('No direct script access allowed');
load_module_asset('gallery', 'css');
?>

<section class="content-header">
    <h1>Video Gallery <small>Upload Video</small> </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo Backend_URL ?>"><i class="fa fa-dashboard"></i> Admin</a></li>
        <li><a href="<?php echo Backend_URL ?>gallery/video"><i class="fa fa-dashboard"></i> Video Gallery</a></li>
        <li class="active">Add Video</li>
    </ol>
</section>

<section class="content">
    <div class="box">

        <div class="box-header with-border">
            <h3 class="box-title">Upload New Video</h3>
        </div>

        <div class="box-body">
            <div id="ajax_respon"></div>
            <div class="row">
                <div class="col-md-12">
                    <form class="form-horizontal" action="<?php echo $action; ?>" onsubmit="return upload_video();" method="post" id="video-upload" novalidate>
                        <input type="hidden" name="id" value="<?php echo $id; ?>" />

                        <div class="form-group">
                            <div class="col-md-4">
                                <p class="text-right"><b><span>Select Album: </span></b></p>
                            </div>
                            <div class="col-md-5">
                                <select name="album_id" class="col-md-4 form-control" id="powerRanger">
                                    <option value="0">Select Album</option>
                                    <?php echo getDropDownAlbums($album_id, 'Video') ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-4">
                                <p class="text-right"><b><span>Title </span></b></p>
                            </div>
                            <div class="col-md-5">
                                <input type="text" class="form-control" name="title" id="video_id" placeholder="Title" value="<?php echo $title; ?>" data-do-validate="true" class="invalid" data-error-msg="Plz Enter Title" />
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-4">
                                <p class="text-right"><b><span>Type</b></p>
                            </div>
                            <div class="col-md-5">
                                <?php echo htmlRadio('type', $type, ['Youtube' => 'Youtube', 'Vimeo' => 'Vimeo']); ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-4">
                                <p class="text-right"><b><span id="video_url"><i class="fa fa-youtube"></i> https://www.youtube.com/watch?v=</span></b></p>
                            </div>
                            <div class="col-md-5">
                                <input type="text" class="form-control" name="video_id" id="video_id" placeholder="Video ID" value="<?php echo $video_id; ?>" data-do-validate="true" class="invalid" data-error-msg="Please Enter Video ID" />
                            </div>
                        </div>

                        <div class="col-md-12" style="margin:10px;">
                            <a href="<?php echo Backend_URL . 'gallery/video'; ?>" class="btn btn-default"><i class="fa fa-long-arrow-left"></i> Back to List</a>
                            <button type="submit" class="btn btn-primary"> <i class="fa fa-cloud-upload"></i> Upload Video</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</section>

<script>
    $(document).ready(function() {
        urlChange();
    });
    
    $('input[name="type"]').click(function() {
        urlChange();
    });

    function urlChange() {
        var type = $('input[name="type"]:checked').val();
        if (type == 'Youtube') {
            $('#video_url').text('https://www.youtube.com/watch?v=');
        } else {
            $('#video_url').text('https://player.vimeo.com/video/');
        }
    }
</script>

<?php load_module_asset('gallery', 'js'); ?>