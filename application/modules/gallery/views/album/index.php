<?php load_module_asset('gallery', 'css'); ?>

<section class="content-header">
    <h1> Manage Album </h1>
    <ol class="breadcrumb">
        <li><a href="<?php Backend_URL ?>"><i class="fa fa-dashboard"></i> Admin</a></li>
        <li><a href="<?php Backend_URL . 'gallery/' ?>"> Gallery</a></li>
        <li class="active">Manage Album</li>
    </ol>
</section>



<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-sm-8 col-xs-12">                    
            <div class="box">  
                <div class="box-header with-border">
                    <h3 class="panel-title"><i class="fa fa-list"></i> Album List</h3>
                </div>
                <div class="box-body"> 
                    <table class="table table-hover table-striped" id="mytable">
                        <thead>
                            <tr>
                                <th width="40">ID</th>
                                <th width="100">Thumb</th>
                                <th>Album</th>
                                <th>Category</th>
                                <th width="70">Photos</th>
                                <th width="90">Action</th>
                            </tr>
                        </thead>

                        <tbody>
                            <?php foreach ($albums as $album) : ?>
                                <tr class="role_id_<?php echo $album->id; ?>">
                                    <td><?php echo $album->id; ?></td>
                                    <td><img src="<?php echo getPhoto($album->thumb); ?>" width="80" alt=""></td>
                                    <td class="edit_id_<?php echo $album->id; ?>"><?php echo $album->name; ?></td>
                                    <td><?php echo getAlbum($album->parent_id); ?></td>
                                    <td><span class="badge"><?php echo $album->qty; ?></span></td>
                                    <td>
                                        <?php echo anchor(site_url(Backend_URL . 'gallery/album/update/' . $album->id), '<i class="fa fa-fw fa-edit"></i>', 'class="btn btn-xs btn-default"'); ?>
                                        <a onClick="delete_album(<?php echo $album->id; ?>)" class="btn btn-danger btn-xs"> <i class="fa fa-fw fa-trash"></i></a>
                                    </td>                                            
                                </tr>                                        
                            <?php endforeach; ?>                                     
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>

        <div class="col-sm-4 col-xs-12">
            <div class="body-body">

                <div class="box">  
                    <div class="box-header with-border">
                        <h3 class="panel-title"><i class="fa fa-plus"></i> Add New Album</h3>
                    </div>
                    <div class="panel-body" >
                        <div id="ajax_respond"></div>
                        <form role="form" method="post" id="album-upload" onsubmit="return create_album();">
                            <div class="form-group">
                                <label for="album_name" style="margin-top:0;">Name</label>
                                <input type="text" class="form-control" id="album_name" name="album_name" />
                            </div>
                            <div class="form-group">
                                <label for="parent" style="margin-top:0;">Category</label>
                                <select name="parent" class="form-control">
                                    <?php echo getAlbumCategory(); ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="album_slug">Slug</label>
                                <input type="text" class="form-control" name="slug" id="slug" placeholder="Slug" value="" data-do-validate="true" class="invalid" data-error-msg="" />
                            </div>
                            <div class="form-group">
                                <div class="btn btn-default btn-file" style="margin-bottom:10px;">
                                    <i class="fa fa-picture-o"></i> Set Thumbnail 
                                    <input type="file" name="thumb" class="file_select" onchange="photoPreview(this, '.upload_image')">
                                </div>

                                <div class="thumbnail upload_image">
                                    <img src="<?php echo getPhoto(''); ?>" alt="Thumbnail">
                                </div>
                            </div>
                            <button type="submit" class="btn btn-success js_add_album"><i class="fa fa-plus"></i> Create</button>
                        </form>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>

    </div>
</section>
<?php load_module_asset('gallery', 'js'); ?>
