<?php

function getDropDownAlbums($id, $type) {
    $CI = &get_instance();
    $albums = $CI->db->get('gallery_albums')->result();
    $html = '';
    foreach ($albums as $album) {
        $html .= '<option value="' . $album->id . '"';
        $html .= ($id == $album->id ) ? ' selected="selected"' : '';
        $html .= '>' . $album->name . '</option>';
    }
    return $html;
}


function uploadGalleryPhoto( $FILE = array()){                               
    $photo = '';        
    $handle = new upload($FILE);
    if ($handle->uploaded) {
        $handle->file_new_name_body = uniqid('p_');
        $handle->image_resize   = true;
        $handle->image_ratio    = true;
        $handle->image_x        = 850;
        $handle->image_y        = 650;
        $handle->jpeg_quality   = 100;                         
        $handle->file_force_extension = true;
        $handle->file_new_name_ext = 'jpg';           
        $handle->process('uploads/gallery/' . date('y/m/'));
        
        if ($handle->processed) {
            $photo =  stripslashes($handle->file_dst_pathname);
        }
    }        
    return $photo;
}

function getAlbumCategory($selected = null) {
    $CI = & get_instance();
    $categories = $CI->db->select('id,name')->get_where('gallery_albums', ['status' => 'Active'])->result();

    $row = '<option value="0">--- Root ---</option>';
    foreach ($categories as $category) {
        $row .= '<option value="' . $category->id . '"';
        $row .= ($selected == $category->id) ? ' selected' : '';
        $row .= '>' . $category->name . '</option>';
    }
    return $row;
}


function getAlbum($album_id = 0) {
    $CI     = & get_instance();
    $id     = (int) $album_id;
    $album = $CI->db->select('name')->get_where('gallery_albums', ['id' => $id])->row();
    if($album){
        return $album->name;
    } else {
        return '---';
    }
    
}


function albumType($selected = null) {
    $type = ['Photo'];
    $options = '';
    foreach ($type as $row) {
        $options .= "<option value=\"{$row}\"";
        $options .= ($row == $selected ) ? 'selected="selected"' : '';
        $options .= ">{$row}</option>";
    }
    return $options;
}

function updateAlbumQty($album_id, $value) {
    if(!$album_id){ return false; }
    $CI = & get_instance();
    $albums = $CI->db->get_where('gallery_albums', ['id' => $album_id])->row();
    $album_qty = intval($albums->qty);
    
    if ($value == 'inc') {
        $album_qty = $album_qty + 1;
    } else {
        $album_qty = $album_qty - 1;
    }

    $data = ['qty' => $album_qty];
    $CI->db->where('id', $album_id);
    $CI->db->update('gallery_albums', $data);
}
function makeGalleryTab($active_tab) {

    $html = '<ul class="tabsmenu">';
    $tabs = [
        'gallery' => '<i class="fa fa-list"></i> List',           
        'gallery/ordering' => '<i class="fa fa-arrows"></i> Change Ordering',
    ];

    foreach ($tabs as $link => $tab) {
        $html .= '<li><a href="' . Backend_URL . $link . '"';
        $html .= ($link == $active_tab ) ? ' class="active"' : '';
        $html .= '>' . $tab . '</a></li>';
    }

    $html .= '</ul>';
    return $html;
}

// --- Not In Use ----
//function getDropDownUsers($id) {
//    $CI = &get_instance();
//    $users = $CI->db->select('id,first_name,last_name')->get('users')->result();
//    $html = '';
//    foreach ($users as $user) {
//        $html .= '<option value="' . $user->id . '"';
//        $html .= ($id == $user->id ) ? ' selected="selected"' : '';
//        $html .= '>' . $user->first_name .' '. $user->last_name . '</option>';
//    }
//    return $html;
//}