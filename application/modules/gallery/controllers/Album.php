<?php

defined('BASEPATH') or exit('No direct script access allowed');

/* Author: Khairul Azam
 * Date : 2016-10-03
 */

class Album extends Admin_controller
{
    const PhotoCaption = false;
    const AlbumCaption = true;

    // Setting Name         = array( Width, Height);
    public $AlbumThumb      = array(208, 200); // Width x Height
    public $PhotoThumb      = array(205, 170); // Width x Height
    public $LightBoxSize    = array(850, 550); // Width x Height

    function __construct()
    {
        parent::__construct();
        $this->load->model('Album_model');
        $this->load->library('form_validation');
        $this->load->helper('gallery');
    }

    public function index()
    {
        $data['albums'] = $this->Album_model->get_all();;
        $this->viewAdminContent('gallery/album/index', $data);
    }

    public function create_action()
    {
        ajaxAuthorized();
        $name       = $this->input->post('album_name');
        $parent_id  = $this->input->post('parent');
        $slug       = ($this->input->post('slug')) ? $this->input->post('slug') : 'auto_' . time() . '_' . rand(0, 100);
        $created    = date('Y-m-d H:i:s');

        //        if( $this->isDuplicateAlbumSlug( $slug ) ){
        //            $slug = $slug . rand(0,999);
        //        }

        if ($name == null) {
            echo ajaxRespond('Fail', '<p class="ajax_error"> Please Enter Album Name </p>');
            exit;
        }
        if (empty($_FILES['thumb']['name'])) {
            echo ajaxRespond('Fail', '<p class="ajax_error">Please Select Album Thumb</p>');
            exit;
        }

        $data       = ['name' => $name, 'slug' => $slug, 'parent_id' => $parent_id, 'created' => $created];
        $query      = $this->db->insert('gallery_albums', $data);
        $insert_id  = $this->db->insert_id();
        $file_name  = uploadGalleryPhoto($_FILES['thumb'], 'gallery/albums', $insert_id);
        
        
        $this->db->update('gallery_albums', ['thumb' => $file_name], ['id' => $insert_id]);

        echo ajaxRespond('OK', '<p class="ajax_success">New Abum Added Successfully</p>');
    }

    private function isDuplicateAlbumSlug($slug = '')
    {
        $count = $this->db->get_where('gallery_albums')->num_rows();
        return $count;
    }

    public function updateAlbumQty($album_id, $value)
    {
        $albums = $this->db->get_where('gallery_albums', ['id' => $album_id])->row();
        $album_qty = intval($albums->qty);
        //dd($album_qty);
        if ($value == 'inc') {
            $album_qty = $album_qty + 1;
        } else {
            $album_qty = $album_qty - 1;
        }

        $data = ['qty' => $album_qty];
        $this->db->where('id', $album_id);
        $this->db->update('gallery_albums', $data);
    }

    public function update($id)
    {
        $row = $this->db->get_where('gallery_albums', ['id' => $id])->row();
        if ($row) {
            $data = [
                'button' => 'Update',
                'action' => site_url(Backend_URL . 'gallery/album/update_action'),
                'id' => set_value('id', $row->id),
                'album_name' => set_value('album_name', $row->name),
                'parent_id' => set_value('parent_id', $row->parent_id),
                'type' => set_value('type', $row->type),
                'slug' => set_value('slug', $row->slug),
                'thumb' => set_value('thumb', $row->thumb),
                'gallery_albums' => $this->db->get('gallery_albums')->result(),
            ];
            $this->viewAdminContent('gallery/album/form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url(Backend_URL . 'gallery'));
        }
    }

    public function update_action()
    {
        ajaxAuthorized();
        $name       = $this->input->post('album_name', TRUE);
        $parent_id  = $this->input->post('parent', TRUE);
        $slug       = slugify($this->input->post('slug', TRUE));
        //        $slug       = ($slug) ? $slug : 'auto_'.time() .'_'. rand( 0,100);
        //        if( $this->isDuplicateAlbumSlug( $slug ) ){
        //            $slug = $slug . rand(0,999);
        //        }

        if ($name == null) {
            echo ajaxRespond('Fail', '<p class="ajax_error"> Please Enter Album Name </p>');
            exit;
        }

        $photo_id = $this->input->post('id', TRUE);
        if (empty($_FILES['thumb']['name'])) {
            $files_name = $this->input->post('thumb_old', TRUE);
        } else {
            removeImage($this->input->post('thumb_old'));
            $files_name = uploadGalleryPhoto($_FILES['thumb'], '/gallery/albums', $photo_id);
        }

        $data = [
            'name'   => $this->input->post('album_name', TRUE),
            'parent_id'   => $parent_id,
            'slug'   => $slug,
            'thumb'  => $files_name,
        ];
        $this->db->where('id', $this->input->post('id'));
        $this->db->update('gallery_albums', $data);

        echo ajaxRespond('OK', '<p class="ajax_success"><strong>Success!</strong> Albums uploaded</p>');
    }

    public function delete()
    {
        ajaxAuthorized();
        $id     = $this->input->post('id');
        $row    = $this->Album_model->get_by_id($id);
        if ($row) {
            $this->Album_model->delete($id);
            removeImage($row->thumb);
            echo ajaxRespond('OK', '<p class="ajax_success">Photo Deleted Successfully</p>');
        } else {
            echo ajaxRespond('Fail', '<p class="ajax_error">Fail! Photo Not Deleted</p>');
        }
    }
}
