<?php

defined('BASEPATH') or exit('No direct script access allowed');

/* Author: Khairul Azam
 * Date : 2016-10-03
 */

class Video extends Admin_controller
{

    const PhotoCaption = false;
    const AlbumCaption = true;

    function __construct()
    {
        parent::__construct();
        $this->load->model('Video_model');
        $this->load->library('form_validation');
        $this->load->helper('gallery');
    }

    public function index()
    {
        $q      = urldecode($this->input->get('q', TRUE));
        $user_id = intval($this->input->get('user_id', TRUE));
        $album_id = intval($this->input->get('album_id', TRUE));
        $start = intval($this->input->get('page'));

        $config['first_url'] = build_pagination_url(Backend_URL . 'gallery/', 'start');
        $config['base_url'] = build_pagination_url(Backend_URL . 'gallery/', 'start');

        $config['per_page'] = 20;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Video_model->total_rows($user_id, $album_id, $q);
        // dd($this->db->last_query());
        $videos = $this->Video_model->get_limit_data($config['per_page'], $start, $user_id, $album_id, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'user_id' => $user_id,
            'album_id' => $album_id,
            'start' => $start,
            'total_rows' => $config['total_rows'],
            'pagination' => $this->pagination->create_links(),
            'videos' => $videos,
        );

        $this->viewAdminContent('gallery/video/index', $data);
    }

    public function add()
    {
        $data = [
            'button'        => 'Create',
            'action'        => site_url(Backend_URL . 'gallery/video/add_action'),
            'id'            => set_value('id'),
            'album_id'      => set_value('album_id'),
            'title'         => set_value('title'),
            'video_id'      => set_value('video_id'),
            'type'          => set_value('type', 'Youtube'),
            'gallery_albums'    => $this->db->get('gallery_albums')->result()
        ];
        $this->viewAdminContent('gallery/video/form', $data);
    }

    public function add_action()
    {
        ajaxAuthorized();
        if (empty($_POST['video_id'])) {
            echo ajaxRespond('Fail', '<p class="ajax_error">Please Enter Video ID</p>');
            exit;
        }

        $data['user_id']        = $this->user_id;
        $data['photo']          = $this->input->post('video_id');
        $data['title']          = $this->input->post('title');
        $data['album_id']       = $this->input->post('album_id');
        $data['type']           = $this->input->post('type');
        $data['created']        = date('Y-m-d H:i:s');

        $this->Video_model->insert($data);
        echo ajaxRespond('OK', '<p class="ajax_success"><b>Success!</b> Viodeo uploaded.</p>');
    }

    public function edit_video($id)
    {
        $row = $this->Video_model->get_by_id($id);
        if ($row) {
            $data = [
                'button' => 'Update',
                'action' => site_url(Backend_URL . 'gallery/video/edit_action'),
                'id' => set_value('id', $row->id),
                'title' => set_value('title', $row->title),
                'album_id' => set_value('album_id', $row->album_id),
                'video_id' => set_value('video_id', $row->photo),
                'type' => set_value('type', $row->type),
                'gallery_albums' => $this->db->get('gallery_albums')->result(),
            ];
            $this->viewAdminContent('gallery/video/form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url(Backend_URL . 'gallery'));
        }
    }

    public function edit_video_action()
    {
        ajaxAuthorized();
        $photo_id = $this->input->post('id', TRUE);
        $album_id = $this->input->post('album_id', TRUE);
        $video_id = $this->input->post('video_id', TRUE);
        $type   = $this->input->post('type', TRUE);

        if (empty($video_id)) {
            echo ajaxRespond('Fail', '<p class="ajax_error">Please Enter Video ID</p>');
            exit;
        }

        $data = [
            'album_id' => $album_id,
            'photo'    => $video_id,
            'type'     => $type,
            'title'    => $this->input->post('title', TRUE)
        ];
        $this->Video_model->update($photo_id, $data);
        echo ajaxRespond('OK', '<p class="ajax_success"><strong>Success!</strong> Video Updated</p>');
    }

    public function delete()
    {
        ajaxAuthorized();
        $id     = intval($this->input->post('id'));
        $row    = $this->Video_model->get_by_id($id);

        if ($row) {
            $this->Video_model->delete($id);
            echo ajaxRespond('OK', '<p class="ajax_success">Video Deleted Successfully</p>');
        } else {
            echo ajaxRespond('Fail', '<p class="ajax_error">Fail! Video Not Deleted</p>');
        }
    }
}
