<?php

defined('BASEPATH') or exit('No direct script access allowed');

/* Author: Khairul Azam
 * Date : 2016-10-03
 */

class Gallery extends Admin_controller
{
    public $PhotoThumb      = array(850, 650); // Width x Height

    function __construct()
    {
        parent::__construct();
        $this->load->model('Gallery_model');
        $this->load->library('form_validation');
        $this->load->helper('gallery');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $user_id = urldecode($this->input->get('user_id', TRUE));
        $album_id = urldecode($this->input->get('album_id', TRUE));
        $start = intval($this->input->get('start'));
        $type = 'Photo';

        $config['first_url'] = build_pagination_url(Backend_URL  . 'gallery/', 'start');
        $config['base_url'] = build_pagination_url(Backend_URL  . 'gallery/', 'start');

        $config['per_page'] = 15;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Gallery_model->total_rows($user_id, $album_id, $type, $q);

        $gallery_photo = $this->Gallery_model->get_limit_data($config['per_page'], $start, $user_id, $album_id, $type, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'user_id' => $user_id,
            'album_id' => $album_id,
            'start' => $start,
            'total_rows' => $config['total_rows'],
            'pagination' => $this->pagination->create_links(),
            'gallery_photo_data' => $gallery_photo,
        );
        $this->viewAdminContent('gallery/photo/index', $data);
    }
	
    public function ordering() {
        $this->db->order_by('sl','ASC');
        $this->db->where('type','Photo');
        $this->db->limit('500');
        $data['photos'] = $this->db->get('gallery')->result();        
        $this->viewAdminContent('gallery/photo/ordering', $data);
    }
    
    public function save_order(){
        $ids = $this->input->post('ids');
        $batch = [];
        foreach($ids as $sl=>$id ){
            $batch[] = [
                'id' => $id,
                'sl' => $sl
            ];                    
        }       
        if(!empty($batch)){
            $this->db->update_batch('gallery', $batch, 'id');
            echo '<p class="ajax_success">Order Saved Successfully</p>';            
        } else {
            echo '<p class="ajax_error">Data is empty to update!!!</p>';
        }
        
    }

    public function create()
    {
        $data = [
            'button'        => 'Create',
            'action'        => site_url(Backend_URL . 'gallery/create_action'),
            'id'            => set_value('id'),
            'album_id'      => set_value('album_id'),
            'title'         => set_value('title'),
            'photo'         => set_value('photo'),
            'photo_description' => set_value('photo_description'),
            'featured' => set_value('featured', 'No'),
            'gallery_albums'    => $this->db->get('gallery_albums')->result()
        ];
        $this->viewAdminContent('gallery/photo/form', $data);
    }

    public function create_action()
    {
        ajaxAuthorized();

        if (empty($_FILES['photo']['name'])) {
            echo ajaxRespond('Fail', '<p class="ajax_error">Please Select gallery Photo</p>');
            exit;
        }

        $data                   = [];
        $data['photo']          = uploadGalleryPhoto($_FILES['photo'], 'gallery');
        $data['user_id']        = $this->user_id;
        $data['title']          = $this->input->post('title');
        $data['album_id']       = $this->input->post('album_id');
        $data['description']    = $this->input->post('photo_description');
        $data['featured']       = $this->input->post('featured');
        $data['created']        = date('Y-m-d H:i:s');

        $this->Gallery_model->insert($data);

        updateAlbumQty((int) $this->input->post('album_id', TRUE), 'inc');

        echo ajaxRespond('OK', '<p class="ajax_success"><b>Success!</b> Photo uploaded.</p>');
    }

    public function multi()
    {
        $data = [
            'button'        => 'Create',
            'action'        => site_url(Backend_URL . 'gallery/multi_action'),
            'id'            => set_value('id'),
            'album_id'      => set_value('album_id'),
            'title'         => set_value('title'),
            'photo'         => set_value('photo'),
            'photo_description' => set_value('photo_description'),
            'featured'      => set_value('featured'),
            'gallery_albums'    => $this->db->get('gallery_albums')->result()
        ];
        $this->viewAdminContent('gallery/photo/multi', $data);
    }

    public function multi_action()
    {
        dd($_POST);
        dd($_FILES);
        //        dd($_GET);

    }

    public function update($id)
    {
        $row = $this->Gallery_model->get_by_id($id);
        if ($row) {
            $data = [
                'button' => 'Update',
                'action' => site_url(Backend_URL . 'gallery/update_action'),
                'id' => set_value('id', $row->id),
                'album_id' => set_value('album_id', $row->album_id),
                'title' => set_value('title', $row->title),
                'photo' => set_value('photo', $row->photo),
                'photo_description' => set_value('photo_description', $row->description),
                'featured' => set_value('featured', $row->featured),
                'gallery_albums' => $this->db->get('gallery_albums')->result(),
            ];
            $this->viewAdminContent('gallery/photo/form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url(Backend_URL . 'gallery'));
        }
    }

    public function update_action()
    {
        ajaxAuthorized();
        $photo_id           = (int) $this->input->post('id');
        $album_id           = (int) $this->input->post('album_id');
        $title              = $this->input->post('title', TRUE);
        $photo_description  = $this->input->post('photo_description', TRUE);
        $featured  = $this->input->post('featured', TRUE);

        if (empty($_FILES['photo']['name'])) {
            $files_name = $this->input->post('old_img', TRUE);
        } else {
            $files_name = $this->input->post('old_img');
            removeImage($files_name);
            $files_name = uploadGalleryPhoto($_FILES['photo'], '/gallery', $photo_id);
        }

        $data = [
            'album_id'    => $album_id,
            'title'       => $title,
            'photo'       => $files_name,
            'description' => $photo_description,
            'featured' => $featured,
        ];
        $this->Gallery_model->update($photo_id, $data);

        echo ajaxRespond('OK', '<p class="ajax_success"><strong>Success!</strong> Photo uploaded</p>');
    }

    public function delete()
    {
        ajaxAuthorized();
        $id     = $this->input->post('id');
        $photo  = $this->input->post('photo');
        $row    = $this->Gallery_model->get_by_id($id);

        if ($row) {
            $this->Gallery_model->delete($id);
            removeImage($photo);
            echo ajaxRespond('OK', '<p class="ajax_success">Photo Deleted Successfully</p>');
        } else {
            echo ajaxRespond('Fail', '<p class="ajax_error">Fail! Photo Not Deleted</p>');
        }
    }

    public function featured_ajax()
    {
        ajaxAuthorized();
        
        $photo_id  = (int) $this->input->post('id');
        $featured  = $this->input->post('is_featured', TRUE);

        $this->db->set('featured', $featured);
        $this->db->where('id', $photo_id);
        $this->db->update('gallery');

        echo ajaxRespond('OK', 'Success!');
    }

    public function menu()
    {
        return buildMenuForMoudle([
            'module'    => 'Gallery',
            'icon'      => 'fa-camera',
            'href'      => 'gallery',
            'children'  => [
                [
                    'title' => 'Photos',
                    'icon'  => 'fa fa-circle-o',
                    'href'  => 'gallery'
                ], [
                    'title' => 'Videos',
                    'icon'  => 'fa fa-circle-o',
                    'href'  => 'gallery/video'
                ], [
                    'title' => 'Alubms',
                    'icon'  => 'fa fa-circle-o',
                    'href'  => 'gallery/album'
                ]
            ]
        ]);
    }
}
