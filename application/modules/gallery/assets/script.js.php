<script>
    function create_album(){
        var formData = new FormData(document.getElementById("album-upload"));        
        var error   = 0;
        if( !error ){
            $.ajax({
                url: "<?php echo Backend_URL; ?>gallery/album/create",
                type: "POST",
                data: formData,
                dataType: 'json',
                enctype: 'multipart/form-data',
                beforeSend: function () {
                    $('#ajax_respond')
                            .css('display', 'block')
                            .html('<p class="ajax_processing">Processing...</p>');
                },
                success: function (respond) {
                    $('#ajax_respond').html(respond.Msg);
                    if (respond.Status === 'OK') {
                        setTimeout(function () {
                            $('#ajax_respond').slideUp(1000);
                            location.reload();
                        }, 2000);                        
                    }
                },
                processData: false, // tell jQuery not to process the data
                contentType: false   // tell jQuery not to set contentType
            });
        }        
        return false;
    }
    
    function delete_album(id) {
        var yes = confirm('Really Want to Delete?');
        if (yes) {
            $.ajax({
                url: "<?php echo Backend_URL; ?>gallery/album/delete",
                type: "POST",
                dataType: "text",
                data: {id: id},
                beforeSend: function () {
                    $('.role_id_' + id).css('background-color', '#FF0000');
                },
                success: function (respond) {
                    $('.role_id_' + id).fadeOut('slow');
                }
            });
        }
    }

    $(document).ready(function () {
        /*$('#testform').attrvalidate();
        
        $('#resetBtn').click(function () {
            $('#testform').attrvalidate('reset');
        }); */

        $('#expandBtn').click(function () {
            var collapsible = $('#' + $(this).attr('aria-controls'));
            $(collapsible).attr('aria-hidden', ($(collapsible).attr('aria-hidden') === 'false'));
            $(this).attr('aria-expanded', ($(this).attr('aria-expanded') === 'false'));
        });
    });
    
    function upload_photo() {        
        var fd = new FormData(document.getElementById("photo-upload"));
        var form    = jQuery('#photo-upload');               
        var url     = form.attr('action');
        var method  = form.attr('method');  
        $.ajax({
            url: url,
            type: method,
            data: fd,
            dataType: 'json',
            enctype: 'multipart/form-data',
            beforeSend: function () {
                $('#ajax_respon')
                        .html('<p class="ajax_processing">Processing...</p>')
                        .css('display', 'block');
            },
            success: function (respond) {
                $('#ajax_respon').html(respond.Msg);
                if (respond.Status === 'OK') {
                    setTimeout(function () {
                        $('#success_report').hide('slow');
                        location.reload();
                    }, 2000);
                    // document.getElementById("photo-upload").reset();
                }
            },
            processData: false, // tell jQuery not to process the data
            contentType: false   // tell jQuery not to set contentType
        });
        return false;
    }
    
    function upload_video() {        
        var fd = new FormData(document.getElementById("video-upload"));
        var form    = jQuery('#video-upload');               
        var url     = form.attr('action');
        var method  = form.attr('method');  
        
        $.ajax({
            url: url,
            type: method,
            data: fd,
            dataType: 'json',
            beforeSend: function () {
                $('#ajax_respon')
                        .html('<p class="ajax_processing">Processing...</p>')
                        .css('display', 'block');
            },
            success: function (respond) {
                $('#ajax_respon').html(respond.Msg);
                if (respond.Status === 'OK') {
                    setTimeout(function () {
                        $('#success_report').hide('slow')
                    }, 2000);
                    // document.getElementById("video-upload").reset();
                }
            },
            processData: false, // tell jQuery not to process the data
            contentType: false   // tell jQuery not to set contentType
        });
        return false;
    }

    $("#profilePic").change(function () {
        var file        = this.files[0];
        var imagefile   = file.type;
        var match       = ["image/jpeg", "image/png", "image/jpg"];
        if (!((imagefile === match[0]) || (imagefile === match[1]) || (imagefile === match[2]))){
            $('#previewing1').attr('src', 'noimage.png');
            return false;
        } else {
            var reader      = new FileReader();
            reader.onload   = imageIsLoaded1;
            reader.readAsDataURL(this.files[0]);
        }
    });

    function imageIsLoaded1(e) {
        $("#profilePic").css("color", "green");
        $('#image_preview1').css("display", "block");
        $('#previewing1').attr('src', e.target.result);
        $('#previewing1').attr('width', '280px');
    }
</script>