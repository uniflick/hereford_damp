<style type="text/css">
    /* label, */
    fieldset legend {
        display: block;
        font-size: 1em;
        margin: 1.5em 0 0.5em;
    }


    fieldset {
        border: none;
        padding: 0;
        margin: 1.5em 0 0;
    }


    #collapsedSection {
        display: block;
        overflow: hidden;
        height: auto;
        max-height: 110px;
        transition: max-height 0.3s ease-out;
    }

    #collapsedSection[aria-hidden="true"] {
        max-height: 0px;
    }

    #collapsedSection label:first-child {
        margin-top: 0.5em;
    }

    .tandc-wrapper {
        margin: 1.5em 0;
    }

    .invalid {
        border: solid 1px rgba(255, 0, 0, 0.4);
    }

    fieldset.invalid {
        border: none;
    }

    #errorSummary li a {
        color: #333;
    }

    #errorSummary p {
        margin-top: 0;
    }

    .error-indicator {
        display: inline-block;
        position: relative;
        color: #f00;
        margin: 5px 0;
        font-size: medium;

    }

    .tandc-wrapper .error-indicator {
        display: block;
    }

    .error-indicator[aria-hidden="true"] {
        display: none;
    }

    #errorSummary {
        margin-bottom: 1em;
    }

    .error-indicator {
        display: block;
        padding: 0.5em 1em;
        background-color: #eee;
        border-radius: 3px;
        margin-top: 0.8em;
        -webkit-animation: show_indicator 0.2s cubic-bezier(0.42, 0.93, 0.87, 1.31);
        animation: show_indicator 0.2s cubic-bezier(0.42, 0.93, 0.87, 1.31);
        transform-origin: left;
    }

    .error-indicator:before {
        content: '';
        position: absolute;
        border: solid 10px transparent;
        border-bottom-color: #eee;
        top: -19px;
        left: 0.5em;
    }

    @-webkit-keyframes show_indicator {
        0% {
            transform: scale(0.9);
            opacity: 0.6;
        }

        100% {
            transform: scale(1);
            opacity: 1;
        }
    }

    @keyframes show_indicator {
        0% {
            transform: scale(0.9);
            opacity: 0.6;
        }

        100% {
            transform: scale(1);
            opacity: 1;
        }
    }

    .upload_area {
        border: 3px dashed #e0e0e0;
        height: 120px;
        background: #f9f9f9;
        border-radius: 25px;
        text-align: center;
    }

    .upload_area input[type=file] {
        padding: 45px;
        width: 100%;
    }

    ul.preview {
        padding: 0;
        margin: 0;
    }

    ul.preview li {
        list-style: none;
        float: left;
        overflow: hidden;
        width: 180px;
        height: 150px;
        position: relative;
    }

    ul.preview li {
        border: 2px solid #EEE;
        padding: 5px;
        margin-right: 10px;
        margin-bottom: 10px;
    }

    ul.preview li img {
        width: 100%;
    }

    ul.preview li b.remove {
        position: absolute;
        top: 5px;
        right: 5px;
    }



    /* ----- toggle label button ----- */
    .toggle-label {
        position: relative;
        display: block;
        width: 90px;
        height: 25px;
        border: 1px solid #808080;
    }

    .toggle-label input[type=checkbox] {
        opacity: 0;
        position: absolute;
        width: 100%;
        height: 100%;
    }

    .toggle-label input[type=checkbox]+.back {
        position: absolute;
        width: 100%;
        height: 100%;
        background: #ed1c24;
        transition: background 150ms linear;
    }

    .toggle-label input[type=checkbox]:checked+.back {
        background: #00a651;
        /*green*/
    }

    .toggle-label input[type=checkbox]+.back .toggle {
        display: block;
        position: absolute;
        content: ' ';
        background: #fff;
        width: 50%;
        height: 100%;
        transition: margin 150ms linear;
        border-radius: 0;
    }

    .toggle-label input[type=checkbox]:checked+.back .toggle {
        margin-left: 44px;
    }

    .toggle-label .label {
        display: block;
        position: absolute;
        width: 50%;
        color: #ddd;
        line-height: 18px;
        text-align: center;
        font-size: 13px;
    }

    .toggle-label .label.on {
        left: 0px;
    }

    .toggle-label .label.off {
        right: 0px;
    }

    .toggle-label input[type=checkbox]:checked+.back .label.on {
        color: #fff;
    }

    .toggle-label input[type=checkbox]+.back .label.off {
        color: #fff;
    }

    .toggle-label input[type=checkbox]:checked+.back .label.off {
        color: #ddd;
    }

    ul#grid-view {margin: 0;padding: 0;}
    ul#grid-view li {
        display: inline-block;
        width: 120px;
        height: 120px;
        border: 1px solid #DDD;
        margin: 0 2px 5px 0;
        padding: 5px;
        text-align: center;
        box-shadow: 0 0 5px -4px #000;
        overflow: hidden;
        background-color: #FFF;
    }
    ul#grid-view li img { height: 75px; }
</style>