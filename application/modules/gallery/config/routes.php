<?php
/* Photo Routes */
$route['admin/gallery']                 = 'gallery';
$route['admin/gallery/create']          = 'gallery/create';
$route['admin/gallery/create_action']   = 'gallery/create_action';
$route['admin/gallery/delete']          = 'gallery/delete';
$route['admin/gallery/update/(:any)']   = 'gallery/update/$1';
$route['admin/gallery/update_action']   = 'gallery/update_action';
$route['admin/gallery/multi']           = 'gallery/multi';
$route['admin/gallery/multi_action']    = 'gallery/multi_action';
$route['admin/gallery/featured_ajax']   = 'gallery/featured_ajax';


/* Videos Routes */
$route['admin/gallery/video']               = 'gallery/video';
$route['admin/gallery/video/add']           = 'gallery/video/add';
$route['admin/gallery/video/add_action']    = 'gallery/video/add_action';
$route['admin/gallery/video/edit/(:num)']   = 'gallery/video/edit_video/$1';
$route['admin/gallery/video/edit_action']   = 'gallery/video/edit_video_action';
$route['admin/gallery/video/delete']        = 'gallery/video/delete';

/* Album Routes */
$route['admin/gallery/album']               = 'gallery/album';
$route['admin/gallery/album/create']        = 'gallery/album/create_action';
$route['admin/gallery/album/update/(:num)'] = 'gallery/album/update/$1';
$route['admin/gallery/album/update_action'] = 'gallery/album/update_action';
$route['admin/gallery/album/delete']        = 'gallery/album/delete';

/* Save Ordering */
$route['admin/gallery/ordering']        = 'gallery/ordering';
$route['admin/gallery/save_order']      = 'gallery/save_order';