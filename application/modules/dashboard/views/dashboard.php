<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<section class="content-header">
    <h1> Dashboard <small>as Admin</small> </h1>    
    <ol class="breadcrumb">
        <li><a href="<?php echo Backend_URL; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
    </ol>
</section>

<?php load_module_asset('dashboard', 'css'); ?> 


<section class="content">

    <!-- Info boxes -->

    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-navy"><i class="fa fa-edit"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Total Post</span>
                    <span class="info-box-number"><?php echo getTotalPost(); ?></span>
                </div>
            </div>
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-olive"><i class="fa fa-newspaper-o"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Total Page</span>
                    <span class="info-box-number"><?php echo getTotalPage(); ?></span>
                </div>
            </div>
        </div>


        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-blue"><i class="fa fa-pencil"></i></span>                
                <div class="info-box-content">
                    <span class="info-box-text">Total Subscriber</span>
                    <span class="info-box-number"><?php echo getTotalSubscriber(); ?></span>
                </div>
            </div>
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-yellow"><i class="fa fa-wrench"></i></span>                
                <div class="info-box-content">
                    <span class="info-box-text">Total Menu</span>
                    <span class="info-box-number"><?php echo getTotalMenu(); ?></span>
                </div>
            </div>
        </div>
    </div>
    
    
    <div class="row">
        <div class="col-md-6">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Latest Mails</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="table no-margin">
                            <thead>
                                <tr>
                                    <th>Mail Type</th>
                                    <th>Subject</th>
                                    <th>Status</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php echo getRecentEmail(10); ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="box-footer clearfix">
                    <a href="<?php echo base_url('admin/mailbox'); ?>" class="btn btn-sm btn-default btn-flat pull-right">View All Mails</a>
                </div>
            </div>
        </div>
        
        <div class="col-md-6">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Latest Subscribers</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="table no-margin">
                            <thead>
                                <tr>
                                    <th>Email</th>
                                    <th>Name</th>
                                    <th>Status</th>
                                    <th>Created</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php echo getRecentSubscribers(10); ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="box-footer clearfix">
                    <a href="<?php echo base_url('admin/newsletter_subscriber'); ?>" class="btn btn-sm btn-default btn-flat pull-right">View All Subscriber</a>
                </div>
            </div>
        </div>
    </div>

</section>
