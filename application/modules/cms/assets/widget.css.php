<style type="text/css">
    fieldset {
        padding: 10px;
        margin: 10px 10px 25px 10px;
        border: 1px solid #CCC;
        background: #f7f7f7;
    }
    
    legend {
        width: inherit;
        padding: 2px 25px;
        margin-bottom: 0;
        font-size: 15px;
        line-height: inherit;
        color: #555;
        border: 1px solid #CCC;
        background: #eee;
        font-weight: bold;
    }  
    .action-row {
        padding-top: 15px;
        padding-bottom: 5px;
        text-align: right;
    }
</style>