<?php defined('BASEPATH') OR exit('No direct script access allowed'); 
load_module_asset('event','css');
?>
<section class="content-header">
    <h1> Event &nbsp;&nbsp;&nbsp; <?php echo anchor(site_url(Backend_URL . 'event/create'), ' + Make a Event', 'class="btn btn-success"'); ?> </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo site_url(Backend_URL) ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Event</li>
    </ol>
</section>

                                                        
<section class="content"> 
 
    <div class="row hidden">
    <div class="col-md-12 text-right">
        <form action="<?php echo site_url(Backend_URL . 'event'); ?>" class="form-inline" method="get">
            <div class="input-group">
                <input type="text" class="form-control" name="q" value="<?php echo $q; ?>">
                <span class="input-group-btn">
                    <?php if ($q <> '') { ?>
                        <a href="<?php echo site_url(Backend_URL . 'event'); ?>" class="btn btn-default">Reset</a>
                    <?php } ?>
                    <button class="btn btn-primary" type="submit">Search</button>
                </span>
            </div>
        </form>
    </div>
    </div>
    
    
     <?php foreach ($events as $event) { ?> 
    <div class="box  no-border">
        <div class="box-body no-padding" id="row_<?php echo $event->id; ?>">
            <div class="row"> 
                <div class="col-md-2">
                    <img src="<?php echo getPhoto($event->thumb); ?>" alt="Thumbnail" class="img-responsive" style="width:100%;">
                </div>
                <div class="col-md-10">
                    <h2><?php echo getShortContent($event->title, 70); ?></h2>
                    <?php echo getShortContent($event->content, 550); ?>                    
                </div>
            </div>
            
            <div class="row no-margin"> 
                <div class="col-md-12 offer_action_bar">
                    <a class="btn btn-default btn-xs"><i class="fa fa-calendar"></i> Expiry On <?php echo globalDateFormat($event->expire); ?></a>                        
                    <?php echo anchor(site_url(Backend_URL . 'event/update/' . $event->id), '<i class="fa fa-edit"></i> Edit', 'class="btn btn-xs btn-default"'); ?>                                                                                                
                    <a class="btn btn-primary btn-xs"><i class="fa fa-check-square-o"></i> <?php echo $event->status; ?> </a>
                    <span data-id="<?php echo $event->id ?>" data-photo="<?php echo $event->thumb ?>" class="btn btn-xs btn-danger js_photo_delete"><i class="fa fa-fw fa-trash"></i> Delete </span>
                </div>
           </div>                         
        </div>
    </div>    
        <?php } ?>
</section>

<script>

$('.js_photo_delete').on('click', function () {
        
        var id = $(this).data('id');
        var photo = $(this).data('photo');
        var yes = confirm('Are you sure to Delete this Event?');
        if (yes) {
            $.ajax({
                url: "admin/event/delete",
                type: "POST",
                dataType: "json",
                data: {id: id, photo: photo},
                beforeSend: function () {
                    $('#row_' + id).css('background-color', '#FF0000');
                },
                success: function (jsonData) {
                    if (jsonData.Status === 'OK') {
                        $('#row_' + id).fadeOut(500);
                    } else {
                        alert(jsonData.Msg);
                    }
                }
            });
        }

    });

</script>