<?php
defined('BASEPATH') OR exit('No direct script access allowed');
load_module_asset('event','css');
?>
<section class="content-header">
    <h1> Event  <small><?php echo $button ?></small> <a href="<?php echo site_url('admin/event') ?>" class="btn btn-default">Back</a> </h1>
    <ol class="breadcrumb">
        <li><a href="admin/"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="admin/event">event</a></li>
        <li class="active">Add New</li>
    </ol>
</section>

<section class="content"> 
    <div class="row">
        <form class="form-horizontal" action="<?php echo $action; ?>" method="post" onsubmit="return eventForm();" enctype="multipart/form-data"  id="event-upload" novalidate>
            <input type="hidden" name="id" value="<?php echo $id; ?>"/>
            <input type="hidden" name="photo_old" value="<?php echo $thumb; ?>" />

            <div class="col-md-12">
            <div class="row">
                <div class="col-md-12"><div id="ajax_respond"></div></div>

                <div class="col-md-9">
                    <div class="box box-primary">
                        <div class="box-body">
                            <div class="col-md-12">
                                
                                <div class="form-group input-group">
                                    <span class="input-group-addon"><i class="fa fa-pencil-square-o"></i> Event Title</span>
                                    <input required="required" type="text" name="title" class="form-control" id="postTitle" placeholder="Event Title" value="<?php echo $title; ?>">
                                </div>
                                
                                <div class="form-group">
                                    <textarea  class="form-control" rows="3" name="content" id="content" placeholder="Content"><?php echo $content; ?></textarea>                
                                </div> 
                                
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-3">   
                    <div class="box box-primary">
                        <div class="box-body">                            
                            <div class="box-header with-border">
                                <div class="form-group">                    
                                    <label class="col-md-4 control-label" for="event_date">Event Date</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control js_datepicker dp_icon" name="event_date" id="event_date" placeholder="Event Date" value="<?php echo $event_date; ?>" />                       
                                    </div>
                                </div>
                                <div class="form-group">                    
                                    <label class="col-md-4 control-label" for="expire">Expire Date</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control js_datepicker dp_icon" name="expire" id="expire" placeholder="Expire Date" value="<?php echo $expire; ?>" />                       
                                    </div>
                                </div>
                            </div>
                            
                            <div class="box-header with-border">
                                <div class="form-group">                    
                                    <label class="col-md-4 control-label">Status</label>
                                    <div class="col-md-8">
                                        <?php echo htmlRadio('status', 'Active', ['Active' => 'Active', 'Inactive' => 'Inactive']); ?>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="box-header with-border">
                                <div class="form-group no-margin">
                                    <button type="submit" class="btn btn-flat btn-block btn-primary"> <i class="fa fa-save"></i> <?php echo $button ?></button> 
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    
                    <div class="box box-success">
                        <div class="box-header with-border">
                            <div class="form-group  no-margin">
                                <h3 class="box-title">Upload Feature Image</h3>
                            </div>
                            <div class="thumbnail upload_image">
                                <img src="<?php echo getPhoto($thumb, 'full'); ?>" alt="Thumbnail">      
                            </div>
                            <div class="btn btn-default btn-file">
                                <i class="fa fa-picture-o"></i> Set Thumbnail 
                                <input type="file" name="thumb" class="file_select" onchange="photoPreview(this, '.upload_image')">
                            </div>
                        </div>       
                    </div>
                    
                    
                </div>
                <div class="clearfix"></div>
            </div>
            </div>
        </form>
    </div>



</section> 
<script src="https://cdn.ckeditor.com/4.7.3/standard/ckeditor.js"></script>
<script>
    
    CKEDITOR.replace('content', {
        width: ['100%'],
        height: ['400px'],
        customConfig: '<?php echo site_url('assets/lib/plugins/ckeditor/config.js'); ?>'
    });
    
    
    $("#profilePic").change(function () {

        var file        = this.files[0];
        var imagefile   = file.type;
        var match       = ["image/jpeg", "image/png", "image/jpg"];
        if (!((imagefile === match[0]) || (imagefile === match[1]) || (imagefile === match[2]))){
            $('#previewing1').attr('src', 'no-thumb.gif');
            return false;
        } else {
            var reader      = new FileReader();
            reader.onload   = imageIsLoaded1;
            reader.readAsDataURL(this.files[0]);
        }
    });    
    
    function imageIsLoaded1(e) {
        $("#profilePic").css("color", "green");
        $('#image_preview1').css("display", "block");
        $('#previewing1').attr('src', e.target.result);
        $('#previewing1').attr('width', '180px');
    }
    
    function CKupdate() {
        for (instance in CKEDITOR.instances)
        CKEDITOR.instances[instance].updateElement();
    }
    
    function eventForm() {  
        CKupdate();
        var fd = new FormData(document.getElementById("event-upload"));       
        var form    = jQuery('#event-upload');
        var url     = form.attr('action');
        var method  = form.attr('method');  
        
        $.ajax({
            url: url,
            type: method,
            data: fd,
            dataType: 'json',
            enctype: 'multipart/form-data',
            beforeSend: function () {
                $('#ajax_respond')
                        .html('<p class="ajax_processing">Processing...</p>')
                        .css('display', 'block');
            },
            success: function (respond) {

                $('#ajax_respond').html(respond.Msg);

                if (respond.Status === 'OK') {
                    setTimeout(function () {
                        $('#success_report').hide('slow');
                        window.location.href = 'event';
                    }, 3000);
                }
                
            },
            processData: false, // tell jQuery not to process the data
            contentType: false   // tell jQuery not to set contentType
        });

        return false;
    }
    
</script>