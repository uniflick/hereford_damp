<?php

$route['admin/event']                 = 'event';
$route['admin/event/create']          = 'event/create';
$route['admin/event/create_action']   = 'event/create_action';
$route['admin/event/update_action']   = 'event/update_action';
$route['admin/event/read/(:any)']     = 'event/read/$1';
$route['admin/event/update/(:any)']   = 'event/update/$1';
$route['admin/event/delete']          = 'event/delete';
