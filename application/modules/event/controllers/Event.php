<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Event extends Admin_controller{
    function __construct(){
        parent::__construct();
        $this->load->model('Event_model');
        $this->load->library('form_validation');
    }

    public function index(){
        
        
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
      
        $config['base_url']     = build_pagination_url( Backend_URL . 'event/', 'start');
        $config['first_url']    = build_pagination_url( Backend_URL . 'event/', 'start');
        

        $config['per_page'] = 25;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Event_model->total_rows($q);
        $events = $this->Event_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'events' => $events,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $this->viewAdminContent('event/index', $data);
    }

    public function read($id){
        $row = $this->Event_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'user_id' => $row->user_id,
		'title' => $row->title,
		'content' => $row->content,
		'thumb' => $row->thumb,
		'status' => $row->status,
		'event_date' => $row->event_date,
		'expire' => $row->expire,
		'created' => $row->created,
	    );
            $this->viewAdminContent('event/view', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url( Backend_URL. 'event'));
        }
    }

    public function create(){
        $data = array(
            'button'    => 'Post Event',
            'action'    => site_url( Backend_URL . 'event/create_action'),
	    'id'        => set_value('id'),
	    'user_id'   => set_value('user_id'),
	    'title'     => set_value('title'),
	    'content'   => set_value('content'),
	    'thumb'     => set_value('thumb'),
	    'status'    => set_value('status'),
	    'event_date'    => set_value('event_date'),
	    'expire'    => set_value('expire')	    
	);
        $this->viewAdminContent('event/form', $data);
    }
    
    public function create_action(){
        ajaxAuthorized();
        
        $user_id = getLoginUserData('user_id');
        $title   = $this->input->post('title');
        $url     = url_title($this->input->post('title'),'-',TRUE).'-'.time();
        $content = $_POST['content'];
        $search  = array('&lt;', '&gt;');
        $replace = array('<', '>');            
        $content = str_replace($search, $replace, $content);
        
        $photo = uploadPhoto($_FILES['thumb'],'uploads/event_photo/', 'event_' . date('Y-m-d-H-i-s_') . rand(0, 9));
        
        $status  = $this->input->post('status');
        $event_date  = $this->input->post('event_date');
        $expire  = $this->input->post('expire');
                        
        if ( $title == null ) {                               
            echo ajaxRespond('Fail','<p class="ajax_error"> Please Enter Event Title </p>');
            exit;                
        }
        if ( $content == null ) {                               
            echo ajaxRespond('Fail','<p class="ajax_error"> Please Enter Content </p>');
            exit;                
        }
        if ( $event_date == null ) {                               
            echo ajaxRespond('Fail','<p class="ajax_error"> Please Enter Event Date </p>');
            exit;                
        } 
        if ( $expire == null ) {                               
            echo ajaxRespond('Fail','<p class="ajax_error"> Please Enter Expire Date </p>');
            exit;                
        }        
        if (empty($_FILES['thumb']['name'])) {
            echo ajaxRespond('Fail','<p class="ajax_error">Please Select Event Photo</p>');
            exit;
        }
                       
        $data = [
            'user_id' => $user_id,
            'title' => $title,
            'url' => $url,
            'content' => $content,
            'thumb' => $photo,
            'status' => $status,
            'event_date' => $event_date,
            'expire' => $expire,
            'created'       => date('Y-m-d H:i:s'),
            ];
            
        $query      = $this->db->insert('event', $data);
        $insert_id  = $this->db->insert_id();        
//        $file_name = $this->uploadPhoto($_FILES['photo'], 'event', $insert_id);
//        $data = [ 'photo' => $file_name ];
        $this->db->where('id', $insert_id );
        $this->db->update('event', $data);
        
        echo ajaxRespond('OK', '<p class="ajax_success">New Event Added Successfully</p>');
    }
    
    public function update($id){
        $row = $this->Event_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url( Backend_URL . 'event/update_action'),
		'id' => set_value('id', $row->id),
		'user_id' => set_value('user_id', $row->user_id),
		'title' => set_value('title', $row->title),
		'content' => set_value('content', $row->content),
		'thumb' => set_value('thumb', $row->thumb),
		'status' => set_value('status', $row->status),
		'event_date' => set_value('status', $row->event_date),
		'expire' => set_value('expire', $row->expire),
		'created' => set_value('created', $row->created),
	    );
            $this->viewAdminContent('event/form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url( Backend_URL. 'event'));
        }
    }
    
    public function update_action(){
        ajaxAuthorized();
        
        $page_id = $this->input->post('id',TRUE);
        $row = $this->db->get_where('event', ['id' => $page_id])->row();
        
        $offer_id = $this->input->post('id'); 
        $title   = $this->input->post('title');
        $content = $this->input->post('content');
        
        $content = $_POST['content'];
        $search  = array('&lt;', '&gt;');
        $replace = array('<', '>');            
        $content = str_replace($search, $replace, $content);
        
        
        $photo = uploadPhoto($_FILES['thumb'],'uploads/event_photo/', 'event_' . date('Y-m-d-H-i-s_') . rand(0, 9));
        
        if (empty($_FILES['thumb']['name'])) {
            $photo = $row->thumb;
        } else {
            removeImage($row->thumb);
        }
        
//        $photo   = $_FILES['photo']['name'];
        $status  = $this->input->post('status');
        $event_date  = $this->input->post('event_date');
        $expire  = $this->input->post('expire');
                        
        if ( $title == null ) {
            echo ajaxRespond('Fail','<p class="ajax_error"> Please Enter Event Title </p>');
            exit;                
        }
        if ( $content == null ) {                               
            echo ajaxRespond('Fail','<p class="ajax_error"> Please Enter Content </p>');
            exit;                
        }
        if ( $event_date == null ) {                               
            echo ajaxRespond('Fail','<p class="ajax_error"> Please Enter Event Date </p>');
            exit;                
        }
        if ( $expire == null ) {                               
            echo ajaxRespond('Fail','<p class="ajax_error"> Please Enter Expire Date </p>');
            exit;                
        }
        
        
                       
        $data = [
            'title' => $title,
            'content' => $content,
            'thumb' => $photo,
            'status' => $status,
            'event_date' => $event_date,
            'expire' => $expire
            ];
        $this->db->where('id', $offer_id );
        $this->db->update('event', $data);
        
        echo ajaxRespond('OK', '<p class="ajax_success">Event Update Successfully</p>');
    }
    
    private function delete_photo_file( $photo = false){
        $filename = dirname( BASEPATH ) . '/' . SpecialOfferFolder . $photo;
        if($photo != 'no-thumb.gif' && $photo && file_exists($filename)){
            unlink($filename); 
        }
    }
    
    public function delete(){
        ajaxAuthorized();
        
        $id    = $this->input->post('id');
        $photo = $this->input->post('thumb');        
        if ($id) {
            $this->Event_model->delete($id);
            if($photo){
                $this->delete_photo_file( $photo );
            }
            echo ajaxRespond('OK', '<p class="ajax_success">Event Deleted Successfully</p>');
        } else {
            echo ajaxRespond('Fail', '<p class="ajax_error">Fail! Event Not Deleted</p>');
        }
    }

    public function _rules(){
	
	$this->form_validation->set_rules('title', 'title', 'trim|required');
	$this->form_validation->set_rules('content', 'content', 'trim|required');
	$this->form_validation->set_rules('thumb', 'thumb', 'trim|required');	
	$this->form_validation->set_rules('event_date', 'event_date', 'trim|required');	
	$this->form_validation->set_rules('expire', 'expire', 'trim|required');	
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }  
    
    
    public function menu(){
        return buildMenuForMoudle([
            'module'    => 'Event',
            'icon'      => 'fa-gear',
            'href'      => 'event', 
            'children'  => ''
        ]);
    }
}