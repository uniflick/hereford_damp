<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Mail extends MX_Controller {
    
    private $site_title = '--Title Not Setup--';
    private $subject    = 'Someone try to send mail without subject';
    public $send_from   = 'flickmedialtd@gmail.com';
    public $from_name   = '--Flick Media Ltd--';
    public $send_to     = 'flickmedialtd@gmail.com';
    public $body;
    private $ip;

    public function __construct() {
        parent::__construct();
        $this->ip           = $this->input->ip_address();
        $this->send_from    = getSettingItem('OutgoingEmail');
        $this->send_to      = getSettingItem('OutgoingEmail');
        $this->from_name    = getSettingItem('SiteTitle');
        $this->site_title   = $this->from_name;
        $this->return_path  = $this->send_to;
    }
    
    public function index() {
        redirect(site_url());
    }
    
    public function test() {
        $this->subject  = 'Test Mail || ' . $this->site_title;
        
        $this->body     = '<p>Lorem Ipsum is simply dummy text of the printing '
                            . 'and typesetting industry. Lorem Ipsum has '
                            . 'been the industry</p>';

        echo $this->send();
    }
   
    
    public function pwd_mail($array = array()) {
        $email = $array['email'];
        $token = $array['_token'];

        $user = $this->db->get_where('users', ['email' => $email])->row();
        $this->send_to = $email;
        $this->from_name = $user->first_name;

        $templateSender = $this->useEmailTeamplate('onRequestForgotPassword');
        $this->subject = $templateSender->title;

        $this->body = $this->filterEmailBody($templateSender->template, [
            'url' => base_url() . 'auth/reset_password?token=' . $token . '&email=' . $email,
            'fullname' => $user->first_name
        ]);

        $this->log();
        $this->save_in_db('onRequestForgotPassword', $user->id, 1);
        return $this->send();
    }

    public function contact_us() {
        ajaxAuthorized();
        
        $name       = $this->input->post('name');
        $email      = $this->input->post('email');  
        $contact    = $this->input->post('contact');
        $subject    = $this->input->post('subject');
        $message    = $this->input->post('cf_message');
        

        $this->send_from = $email;
        if (!filter_var($this->send_from, FILTER_VALIDATE_EMAIL)) {
            die(ajaxRespond('Fail', '<p class="ajax_error">Invalid Email</p>'));
        }
       
        $this->from_name = $name;
        $templateSender = $this->useEmailTeamplate('onContactUs');
        $this->subject = 'Contact us for a Quote || Main Office';
//        $this->send_to = '';
        $this->body = $this->filterEmailBody($templateSender->template, [
            'name' => $name,
            'email' => $email,
            'contact' => $contact,
            'subject' => $subject,
            'message' => $message
        ]);
        $this->log();
        $this->save_in_db('onContactUs', 1, getLoginUserData('user_id'));
        echo $this->send();
    }
    
    private function useEmailTeamplate($slug = '') {
        $this->db->select('title,template');
        $this->db->where('slug', $slug);
        $data = $this->db->get('email_templates')->row();
        if($data){
            return $data;
        } else {
            return (object) array( 'template' => 'Empty', 'title'=> "Unknown Subject || {$this->from_name}");
        }        
    }

    private function filterEmailBody($template = null, $placeholders = array(0)) {
        if ($template && count($placeholders)) {
            foreach ($placeholders as $key => $value) {
                $template = str_replace("%{$key}%", $value, $template);
            }
        }
        return $template;
    }

    private function log() {
        $log_path = APPPATH . '/logs/mail_log.txt';
        $mail_log = date('Y-m-d H:i:s A') . ' | ' . $this->ip . ' | ' . $this->subject .' | ' . $this->send_from  .' | ' . $this->send_to . "\r\n";
        file_put_contents($log_path, $mail_log, FILE_APPEND);
    }

    private function save_in_db($mail_type = 'general', $receiver_id = 0, $sender_id = 0 ) {
        $data = [
            'mail_type'     => $mail_type,            
            'sender_id'     => $sender_id,
            'receiver_id'   => $receiver_id,
            'mail_from'     => $this->send_from,
            'mail_to'       => $this->send_to,
            'subject'       => $this->subject,
            'body'          => $this->getDefaultLayout($this->body),
            'sent_at'       => date('Y-m-d H:i:s')
        ];
        $this->db->insert('mails', $data);
    }

    private function send() {
        // $send_to, $subject, $body, $cc = false, $bcc = false, $attach = null
        $mail = new PHPMailer;

        $mail->setFrom($this->send_from, $this->from_name);
        $mail->addAddress($this->send_to);
        $mail->addReplyTo($this->send_from, $this->from_name);

        $server = $_SERVER['SERVER_NAME'];

        $mail->HeaderLine('MIME-Version', '1.0');
        $mail->HeaderLine('X-Mailer', 'PHP/' . phpversion());
        $mail->HeaderLine('Return-Path', $this->return_path);
        $mail->HeaderLine('X-Mailer', "Microsoft Office Outlook, Build 11.0.5510");
        $mail->HeaderLine("X-MimeOLE", "Produced By Microsoft MimeOLE V6.00.2800.1441");
        $mail->HeaderLine('Content-Transfer-encoding', '8bit');
        $mail->HeaderLine('Organization', $server);
        $mail->HeaderLine('Message-ID', "<" . md5(uniqid(time())) . "@{$server}>");
        $mail->HeaderLine('X-MSmail-Priority', 'Normal');
        $mail->HeaderLine('X-Sender', $this->send_from);
        $mail->HeaderLine('X-AntiAbuse', "This is a solicited email for - $server mailing list.");
        $mail->HeaderLine('X-AntiAbuse', "Servername - {$server}");
        $mail->HeaderLine('X-AntiAbuse', $this->send_from);

        $mail->isHTML(true);
                
        $mail->Subject  = $this->subject;
        $mail->Body     = $this->getDefaultLayout($this->body);
        $mail->AltBody  = strip_tags($this->body);

        if ($mail->send()) {
            return ajaxRespond('OK', '<p class="ajax_success">Mail sent successfully</p>');
        } else {
            return ajaxRespond('Fail', '<p class="ajax_error">' . $mail->ErrorInfo . '</p>');
        }
    }

    private function getDefaultLayout( $MailBody = '') {
        $template =  $this->load->view('email_templates/layout-active', '', true);
        return str_replace("%MailBody%", $MailBody, $template);
    }
    
    
}

/*
 * 
    public function send_a_complaint() {
        ajaxAuthorized();
        
        $name           = $this->input->post('c_name');
        $email          = $this->input->post('c_email');
        $type           = $this->input->post('c_type');
        $message        = $this->input->post('c_message');

        $this->send_from = $email;
        if (!filter_var($this->send_from, FILTER_VALIDATE_EMAIL)) {
            die(ajaxRespond('Fail', '<p class="ajax_error">Invalid Email</p>'));
        }
       
        $this->from_name = $name;
        $templateSender = $this->useEmailTeamplate('onComplaint');
        $this->subject = $this->filterEmailBody($templateSender->title, ['SiteTitle'=> $this->site_title]);
//        $this->send_to = '';
        $this->body = $this->filterEmailBody($templateSender->template, [
            'name' => $name,
            'email' => $email,
            'type' => $type,
            'message' => $message
        ]);
        $this->log();
        $this->save_in_db('onComplaint', 1, getLoginUserData('user_id'));
        echo $this->send();
    }
    
    public function send_a_feedback() {
        ajaxAuthorized();
        
        $name           = $this->input->post('f_name');
        $email          = $this->input->post('f_email');
        $rating           = $this->input->post('f_rating');
        $message        = $this->input->post('f_message');

        $this->send_from = $email;
        if (!filter_var($this->send_from, FILTER_VALIDATE_EMAIL)) {
            die(ajaxRespond('Fail', '<p class="ajax_error">Invalid Email</p>'));
        }
       
        $this->from_name = $name;
        $templateSender = $this->useEmailTeamplate('onFeedback');
        $this->subject = $this->filterEmailBody($templateSender->title, ['SiteTitle'=> $this->site_title]);
//        $this->send_to = '';
        $this->body = $this->filterEmailBody($templateSender->template, [
            'name' => $name,
            'email' => $email,
            'rating' => $rating,
            'message' => $message
        ]);
        $this->log();
        $this->save_in_db('onFeedback', 1, getLoginUserData('user_id'));
        echo $this->send();
    }
    
 */