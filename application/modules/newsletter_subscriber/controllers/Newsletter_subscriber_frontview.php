<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// Newsletter_frontend_subscriber
class Newsletter_subscriber_frontview extends Frontend_controller {

    function __construct() {
        $this->load->model('Newsletter_subscriber_model');
    }
    
    
    public function create_action_ajax() {
        ajaxAuthorized();
        $fName          = $this->input->post('fName', TRUE);
        $lName          = $this->input->post('lName', TRUE);      
        $email_address  = $this->input->post('newsletter_email', TRUE);
        
        $name = $fName .' '.$lName;
        
        if (!filter_var($email_address, FILTER_VALIDATE_EMAIL)) {
            echo ajaxRespond('Fail', '<span class="text-danger">Invalide Email Address</span>');
            exit;
        }
        
        $exists = $this->Newsletter_subscriber_model->isExists($email_address);
        if ($exists) {
            $subscribed = $this->Newsletter_subscriber_model->check($email_address);
            if ($subscribed != 'Subscribe') {
                $data = array(
                    'status' => 'Subscribe',
                    'modified' => date('Y-m-d H:i:s')
                );
                $subscribed = intval($subscribed);
                $this->Newsletter_subscriber_model->update($subscribed, $data);
            } else {
                echo ajaxRespond('FAIL', '<span class="text-danger">Already Subscribed</span>');
                exit;
            }
        } else {
            $data = array(
                'name' => $name,
                'email' => $email_address,
                'status' => 'Subscribe',
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s')
            );
            $this->Newsletter_subscriber_model->insert($data);
        }

        $encoded_email = base64_encode($email_address);
        $filter = ['To' => $email_address, 'Template' => 'onNewsletterSubscription', 'EmailAddress' => $encoded_email, 'url' => base_url() . 'unsubscribe?e='];
        Modules::run('mail/subscribe', $email_address);
        echo ajaxRespond('OK', '<span class="text-success">Subscribe Successfully! Check Your Email.</span>');
    }

    public function unsubscribe() {
        $email = $this->input->get('e', TRUE);
        $encoded_email = base64_decode($email);
        
        if (!filter_var($encoded_email, FILTER_VALIDATE_EMAIL)) {
            die('<h1>Invalide Email Address</h1>');
        }
        
        $user = $this->Newsletter_subscriber_model->get_by_email($encoded_email);
        if($user){
            if ($user->status == 'Unsubscribe') {
                $message = "You are already Unscubscribed";
                $this->viewFrontContent('newsletter_subscriber/unsubscribe', compact('user', 'message'));
            } else {
                $data = array(
                    'status' => 'Unsubscribe'
                );

                $this->db->set('status', 'Unsubscribe');
                $this->db->where('email', $email);
                $this->db->update('newsletter_subscribers');

                 $this->Newsletter_subscriber_model->update_by_email($encoded_email, $data);

                $message = "Successfully Unscubscribed";
                $this->viewFrontContent('newsletter_subscriber/unsubscribe', compact('user', 'message'));
            }
        } else {
            die('<h1>Invalide Email Address</h1>');
        }
    }

}
