<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Profile_helper {

    public static function isCompanyBrand($array, $id = 0) {
        return (in_array($id, $array)) ? 'checked="checked"' : '';
    }

    public static function makeTab($active_tab) {
        $user_id = getLoginUserData('user_id');

//        $url = getURL($user_id, getUserData($user_id, 'company_name'));
        $html = '<ul class="tabsmenu">';
        $tabs = [
            'profile' => '<i class="fa fa-briefcase"></i> My Profile &nbsp;&nbsp;<i class="fa fa-chevron-right"></i>',
            'profile/services' => '<i class="fa fa-photo"></i> Products and Services &nbsp;&nbsp;<i class="fa fa-chevron-right"></i>',
            'gallery' => '<i class="fa fa-photo"></i> Upload Photo &nbsp;&nbsp;<i class="fa fa-chevron-right"></i>',
        ];

        foreach ($tabs as $link => $tab) {
            $html .= '<li><a href="' . Backend_URL . $link . '"';
            $html .= ($link == $active_tab ) ? ' class="active"' : '';
            $html .= '>' . $tab . '</a></li>';
        }

//        $html .= '<li><a href="' . $url . '" target="_blank">';
//        $html .= '<i class="fa fa-external-link"></i> Preview Page</a>';
//        $html .= '</li>';

        $html .= '</ul>';
        return $html;
    }
   
}
