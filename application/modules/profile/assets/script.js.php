<script>

jQuery('.js_select2').select2();
    
function  update_profile() {
    var formData = jQuery('#update_profile_info').serialize();
    jQuery.ajax({
        url: 'admin/profile/update',
        type: "POST",
        dataType: 'json',
        data: formData,
        beforeSend: function () {
            jQuery('#ajax_respond')
                    .html('<p class="ajax_processing">Loading...</p>')
                    .css('display','block');
        },
        success: function ( jsonRespond ) {
            jQuery('#ajax_respond').html( jsonRespond.Msg );
            if(jsonRespond.Status === 'OK'){                
                setTimeout(function () {
                    jQuery('#ajax_respond').slideUp( );                    
                }, 2000);
            }                                      
        }
    });
    return false;
}

    function addNewAddress(id) {
        var location = 'location'+id;
        var text = {componentRestrictions: {country: 'GB'}};
        var input = document.getElementById(location);
        autocomplete = new google.maps.places.Autocomplete(input, text);
    }

    $('.note').keyup(function () {
        var left = 1000 - $(this).val().length;
        if (left < 0) {
            left = 0;
        }
        $('.counter').text('Characters left: ' + left);
    });
    
    
    
    var i, p;
    $(function () {
        var scntDiv = $('#files');
        i = $('#files div.row').size() + 1;
        p = 2;
        $('#addScnt').on('click', function () {

            var imageClass = "file_" + i;

            var html = '<div class="row" id="file_' + i + '">' +
                    '<input type="hidden" name="file_id[]" value="' + i + '"/>' +
                    '<div class="form-group">' +
                    '<div class="col-md-6">' +
                    '<label>Title</label>' +
                    '<input class="form-control" name="title[]" placeholder="Title" type="text">' +                                        
                    '<span class="btn btn-xs btn-danger" onclick="removeFieldOnly('+ i +');"> <i class="fa fa-times"></i> Remove </span>'+                                                            
                    '</div>' +
                    '<div class="col-md-4">' +
                    '<label>Upload File</label>' +
                    '<input type="file" class="files" name="file[]" onchange="photoPreview(this, \'.' + imageClass + '\')">' +
                    '</div>' +
                    '<div class="col-md-2">' +
                    '<div class="thumblin ' + imageClass + '">' +
                    '<img src="<?php echo base_url('uploads/no-photo.jpg'); ?>" class="img-responsive">' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>';


            $(html).appendTo(scntDiv);
            i++;
            p++;
            return false;
        });
    });

    function removeFile(id, photo) {
        var check = confirm('Do you really want to delete it?');
        if (check === true) {

            $.post('<?php echo base_url(); ?>admin/profile/delete_photo', {
                photo: photo,
                id: id
            });

            $('#file_' + id).remove();
            i--;
            p--;
        }
        return false;
    }
    
    function removeFieldOnly(id) {                          
        $('#file_' + id).remove();
        i--;
        p--;        
        return false;
    }




function isCheck( day ){                                
    var yes = $('[name='+ day +']').is(":checked");     

    if( yes ){
       $('.'+day).css('opacity','0.2'); 
    } else {
       $('.'+day).css('opacity','1');  
    }             
}


</script>

