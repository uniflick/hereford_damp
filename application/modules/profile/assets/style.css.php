<style>
    .input-group .input-group-addon { background-color: #EEE; }

    span.small {
        min-width:110px !important;
    }
    span.small_day {
        min-width:80px !important;
    }
    span.small_map {
        min-width:40px !important;
    }

    .input-group {
        margin-bottom: 10px;
    }    
    .input-group .input-group-addon { background-color: #EEE; }
    .input-group-addon sup {
        font-size: 13pt;
        top: -2px;
    }
    .input-group-addon {
        min-width:180px; 
        text-align:left;
    }
    .input-group-addon {
        padding: 2px 12px;
    }
    label {
        cursor: pointer !important;
    }

    #files > div.row{
        border: 1px solid #f1f1f1;
        padding: 5px 0;
        margin-left: 0px;
        margin-right: 0px;
        margin-bottom: 5px;
        background-color: #f9f9f9;
    }

    .file-preview-frame{
        width: 100% !important;
        height: 100% !important;
    } 
    #extra > div {        
        border: 1px solid #CCC;
        padding-left: 15px;
    }
    
    .remove_inside_padding > div,
    #remove_inside_padding > div {       
        padding-top: 1px !important;
    }
    li.select2-selection__choice {color: #000 !important;}

</style>
