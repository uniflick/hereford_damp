<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php load_module_asset('profile', 'css'); ?>
<?php load_module_asset('profile', 'js'); ?>
<?php load_module_asset('users', 'css'); ?>

<section class="content-header">
    <h1>My Profile <small>Update</small></h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo Backend_URL ?>"><i class="fa fa-user"></i> Admin</a></li>
        <li><a href="<?php echo Backend_URL . 'profile' ?>"><i class="fa fa-dashboard"></i> Profile</a></li>
        <li class="active">Update Profile</li>
    </ol>
</section>

<section class="content">   
    <div class="row">
        <form action="<?php echo base_url(Backend_URL . 'profile/update_action'); ?>" enctype="multipart/form-data" role="form" id="personalForm" method="post">
            <input type="hidden" name="old_photo" value="<?php echo ($profile_photo); ?>"/>
            <div class="col-md-3 pull-right">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title"> Profile Photo </h3>
                    </div>
                    <div class="box-body box-profile">

                        <div class="thumbnail upload_image">                     
                            <img src="<?php echo getPhoto($profile_photo); ?>" class="img-responsive">
                        </div>

                        <div class="btn btn-default btn-file">
                            <i class="fa fa-picture-o"></i> Change Profile Photo 
                            <input type="file" name="photo" class="file_select" onchange="photoPreview(this, '.upload_image')">
                        </div>

                        <p><em><br/>Please click "<strong>Update</strong>" button after change</em></p>


                    </div>

                    <div class="form-group clearfix">
                        <div class="col-md-10 text-right col-md-offset-2">
                            <?php // if ($user->role_id == 4) { ?>
                                <button type="submit" class="btn btn-info btn-lg" id="personalSubmit">Update</button>
                            <?php /* } else { ?>
                                <p class="text-red"><em>Your account is not User</em></p>
                            <?php } */ ?>
                        </div>
                    </div>
                    <div class="clearfix"></div>

                </div>
            </div>



            <div class="col-md-9">
               
                <div class="box">

                    <div class="box-body">

                        <?php echo $this->session->flashdata('messsage'); ?>

                        <div class="form-group clearfix">                        
                            <div class="col-md-6 no-padding">                                
                                <label for="first_name" class="control-label col-md-4">First Name</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" placeholder="First Name" 
                                           name="first_name" value="<?php echo $user->first_name; ?>">                                 
                                </div>
                            </div>
                            <div class="col-md-6 no-padding">                                    
                                <label for="first_name" class="control-label col-md-4">Last Name</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" placeholder="Last Name" 
                                           name="last_name" value="<?php echo $user->last_name; ?>"/>
                                </div>                                    
                            </div>
                        </div>
                        
                        <div class="form-group clearfix">
                            <label for="email" class="control-label col-md-2">Email Address</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" id="email" placeholder="Email" readonly value="<?php echo $user->email; ?>">                                
                                <p class="help-block"><em>Email Not Changeable. If need contact to admin</em></p>
                            </div>
                        </div>
                        <div class="form-group clearfix">
                            <label for="contact" class="control-label col-md-2">Contact Number</label>
                            <div class="col-md-10">
                                <input required="" type="text" class="form-control" id="contact" placeholder="Contact Number" 
                                       name="contact" value="<?php echo $user->contact; ?>" onkeypress="return DigitOnly(event);">
                            </div>
                        </div>
                        <div class="form-group clearfix">
                            <label for="contact" class="control-label col-md-2">Address Line 1</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" id="add_line1" placeholder="Address Line 1" 
                                       name="add_line1" value="<?php echo $user->add_line1; ?>">
                            </div>
                        </div>
                        <div class="form-group clearfix">
                            <label for="contact" class="control-label col-md-2">Address Line 2</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" id="add_line2" placeholder="Address Line 2" 
                                       name="add_line2" value="<?php echo $user->add_line2; ?>">
                            </div>
                        </div>
                        <div class="form-group clearfix">
                            <label for="city" class="control-label col-md-2">City</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" id="city" placeholder="City" 
                                       name="city" value="<?php echo $user->city; ?>">
                            </div>
                        </div>
                        <div class="form-group clearfix">
                            <label for="contact" class="control-label col-md-2">Region/State</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" id="state" placeholder="Region/State" 
                                       name="state" value="<?php echo $user->state; ?>">
                            </div>
                        </div>
                        <div class="form-group clearfix">
                            <label for="contact" class="control-label col-md-2">Postcode</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" id="postcode" placeholder="Postcode" 
                                       name="postcode" value="<?php echo $user->postcode; ?>">
                            </div>
                        </div>
                        <div class="form-group clearfix">
                            <label for="contact" class="control-label col-md-2">Country</label>
                            <div class="col-md-10">
                                <select class="form-control" name="country_id">
                                    <?php echo getDropDownCountries($user->country_id); ?>
                                </select>
                            </div>
                        </div>
                       

                    </div>
                </div>
            </div>
        </form>
    </div>
</section>


<script>
    $('#role_id_1').click(function () {
        $('.seller_use').slideUp();
    });
    $('#role_id_2').click(function () {
        $('.seller_use').slideDown();
    });

    $('#payment_method_1, #payment_method_2').click(function () {
        var type = $(this).val();
        if (type === 'Bank') {
            $('#paypal').addClass('hidden');
            $('#bank').removeClass('hidden');
        } else {
            $('#bank').addClass('hidden');
            $('#paypal').removeClass('hidden');
        }
    });

    $('.js_select2').select2({
        placeholder: {
            id: "-1",
            text: "Search Service",
            selected: 'selected'
        },
        allowClear: true

    });

    function addNewAddress(location) {
        var location = location;
        //var text = {componentRestrictions: {country: 'GB'}};
        var text = {};
        var input = document.getElementById(location);
        autocomplete = new google.maps.places.Autocomplete(input, text);
    }



</script>    