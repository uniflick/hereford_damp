<?php

$route['admin/testimonial']                 = 'testimonial';
$route['admin/testimonial/create']          = 'testimonial/create';
$route['admin/testimonial/create_action']   = 'testimonial/create_action';
$route['admin/testimonial/update_action']   = 'testimonial/update_action';
$route['admin/testimonial/read/(:any)']     = 'testimonial/read/$1';
$route['admin/testimonial/update/(:any)']   = 'testimonial/update/$1';
$route['admin/testimonial/delete/(:any)']   = 'testimonial/delete/$1';
