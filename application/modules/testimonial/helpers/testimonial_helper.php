<?php defined('BASEPATH') OR exit('No direct script access allowed');

// leave blank for module helper
define('TesimonialPhotoDir', 'uploads/testimonials/');

function getTestimonialThumb( $photo = 'no-thumb.jpg' ){
     $filename = dirname( BASEPATH ). '/'. TesimonialPhotoDir . $photo;
    if($photo && file_exists($filename)){
        $thumb = TesimonialPhotoDir . $photo;
    } else {
        $thumb = TesimonialPhotoDir . 'default.jpg';
    }
    return '<img src="'.$thumb.'" width="50">';
}

