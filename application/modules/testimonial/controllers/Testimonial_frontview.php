<?php defined('BASEPATH') OR exit('No direct script access allowed');
 
/* Author: Khairul Azam
 * Date : 2016-11-04
 */

class Testimonial_frontview extends MX_Controller{
    function __construct(){
        parent::__construct();
        $this->load->model('Testimonial_model');
        $this->load->helper('testimonial');
        $this->load->library('form_validation');
    }

    public function index(){ 
        $data['testimonials']   = $this->Testimonial_model->get_all();

        //$this->viewAdminContent('testimonial/index', $data);
        //$this->load->view('testimonial/widget-1',$data);
        $this->load->view('testimonial/widget-2',$data);
        //$this->load->view('testimonial/widget-3',$data);
    }
}