<?php defined('BASEPATH') OR exit('No direct script access allowed');
 
/* Author: Khairul Azam
 * Date : 2016-10-17
 */

class Testimonial extends Admin_controller{
    function __construct(){
        parent::__construct();
        $this->load->model('Testimonial_model');
        $this->load->helper('testimonial');
        $this->load->library('form_validation');
    }

    public function index(){
        $testimonial = $this->Testimonial_model->get_all();

        $data = array(
            'testimonials' => $testimonial
        );

        $this->viewAdminContent('testimonial/index', $data);
    }

    public function read($id){
        $row = $this->Testimonial_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'user_id' => $row->user_id,
		'name' => $row->name,
		'content' => $row->content,
		'photo' => $row->photo,
		'date' => $row->date,
		'status' => $row->status,
		'created' => $row->created,
		'modified' => $row->modified,
	    );
            $this->viewAdminContent('testimonial/view', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url( Backend_URL. 'testimonial'));
        }
    }

    public function create(){
        $data = array(
            'button' => 'Create',
            'action' => site_url( Backend_URL . 'testimonial/create_action'),
	    'id' => set_value('id'),
	    'user_id' => set_value('user_id'),
	    'title' => set_value('title'),
	    'name' => set_value('name'),
	    'content' => set_value('content'),
	    'photo' => set_value('photo'),
	    'date' => date('Y-m-d'),
            'old_photo' => '',
	    'status' => 'Active',
	    'created' => date('Y-m-d H:i:s'),
	    'modified' => date('Y-m-d H:i:s')
	);
        $this->viewAdminContent('testimonial/form', $data);
    }
    
    public function create_action(){
        
        $photo = uploadPhoto($_FILES['thumb'],'uploads/testimonial/', 'testimonial_' . date('Y-m-d-H-i-s_') . rand(0, 9));
        
        $data = array(
        'user_id' => $this->input->post('user_id',TRUE),
        'title' => $this->input->post('title',TRUE),
        'name' => $this->input->post('name',TRUE),
        'content' => $this->input->post('content',TRUE),
        'photo' => $photo,
        'date' => $this->input->post('date',TRUE),
        'status' => $this->input->post('status',TRUE),
        'created' => date('Y-m-d H:i:s'),
        'modified' => date('Y-m-d H:i:s'),
        );

        $this->Testimonial_model->insert($data);
        $this->session->set_flashdata('message', 'Create Record Success');
        redirect(site_url( Backend_URL. 'testimonial'));
        
    }
    
    public function update($id){
        $row = $this->Testimonial_model->get_by_id($id);

        if ($row) {
            $data = array(
            'button' => 'Update',
            'action' => site_url( Backend_URL . 'testimonial/update_action'),
            'id' => set_value('id', $row->id),
            'user_id' => set_value('user_id', $row->user_id),
            'title' => set_value('title', $row->title),
            'name' => set_value('name', $row->name),
            'content' => set_value('content', $row->content),
            'photo' => set_value('photo', $row->photo),
            'date' => set_value('date', $row->date),
            'old_photo' => set_value('photo', $row->photo),
            'status' => set_value('status', $row->status),
            'created' => set_value('created', $row->created),
            'modified' => set_value('modified', $row->modified),
            );
                $this->viewAdminContent('testimonial/form', $data);
        } else {
                $this->session->set_flashdata('message', 'Record Not Found');
                redirect(site_url( Backend_URL. 'testimonial'));
            }
    }
    
    public function update_action(){
        
        $id = $this->input->post('id',TRUE);
        $row = $this->db->get_where('testimonials', ['id' => $id])->row();
        
        $photo = uploadPhoto($_FILES['thumb'],'uploads/testimonial/', 'testimonial_' . date('Y-m-d-H-i-s_') . rand(0, 9));
        
        if (empty($_FILES['thumb']['name'])) {
            $photo = $row->photo;
        } else {
            removeImage($row->photo);
        }
            
        $data = array(
            'user_id' => $this->input->post('user_id',TRUE),
            'title' => $this->input->post('title',TRUE),
            'name' => $this->input->post('name',TRUE),
            'content' => $this->input->post('content',TRUE),
            'photo' => $photo,
            'date' => $this->input->post('date',TRUE),
            'status' => $this->input->post('status',TRUE),		
            'modified' => date('Y-m-d H:i:s'),
	    );
//        pp($data);

            $this->Testimonial_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url( Backend_URL. 'testimonial'));
        
    }
    
    public function delete($id){
        $row = $this->Testimonial_model->get_by_id($id);

        if ($row) {
            $this->Testimonial_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url( Backend_URL. 'testimonial'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url( Backend_URL. 'testimonial'));
        }
    }

    public function _rules(){
	$this->form_validation->set_rules('user_id', 'user id', 'trim');
	$this->form_validation->set_rules('name', 'name', 'trim|required');
	$this->form_validation->set_rules('content', 'content', 'trim|required');
	$this->form_validation->set_rules('photo', 'photo', 'trim');
	$this->form_validation->set_rules('status', 'status', 'trim|required');
	$this->form_validation->set_rules('created', 'created', 'trim|required');
	$this->form_validation->set_rules('modified', 'modified', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }


    public function widget() {
        $testimonial = $this->Testimonial_model->get_all();
        $data = array(
            'testimonial_data' => $testimonial
        );

        //$this->viewAdminContent('testimonial/index', $data);
        //$this->load->view('testimonial/widget-1',$data);
        $this->load->view('testimonial/widget-2',$data);
        //$this->load->view('testimonial/widget-3',$data);
    }
    
    public function _menu(){
        return buildMenuForMoudle([
            'module'    => 'Testimonial',
            'icon'      => 'fa-comment',
            'href'      => 'testimonial',                    
            'children'  => [
                [
                    'title' => 'Testimonials',
                    'icon'  => 'fa fa-circle-o',
                    'href'  => 'testimonial'
                ],[
                    'title' => ' + Add New',
                    'icon'  => 'fa fa-circle-o',
                    'href'  => 'testimonial/create'
                ]                     
            ]        
        ]);
    }
}