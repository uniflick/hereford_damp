<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<section class="content-header">
    <h1> Testimonials  <small>Control panel</small> <?php echo anchor(site_url(Backend_URL . 'testimonial/create'), ' + Add New', 'class="btn btn-default"'); ?> </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
        <li class="active">Testimonials</li>
    </ol>
</section>

<section class="content">
    <div class="box" style="margin-bottom: 10px">

        <div class="box-header">          
            <div class="col-md-4 text-right">

            </div>    
        </div>

        <div class="box-body">
            <div class="-table-responsive-">
                <table class="table table-hover table-striped" id="mytable">
                    <thead>
                        <tr>
                            <th width="20px">No</th>                            
                            <th>Photo</th>
                            <th>Name &AMP; Testimonial</th>                                                        
                            <th>Status</th>                            
                            <th width="160">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($testimonials as $testimonial) { ?>
                            <tr>
                                <td><?php echo $testimonial->id; ?></td>
                                <td><img src="<?php echo getPhoto($testimonial->photo, '', 70, 70) ?>" ></td>
                                <td>
                                    <p><b><?php echo $testimonial->name ?></b> (
                                        <?php echo globalDateFormat($testimonial->created) ?>)<br/>
                                        <em><?php echo getShortContent($testimonial->content) ?></em></p>                                                                
                                </td>                                                               
                                <td><?php echo $testimonial->status ?></td>                                                               
                                <td width="300">
                                    <?php
                                    echo anchor(site_url(Backend_URL . 'testimonial/read/' . $testimonial->id), '<i class="fa fa-fw fa-external-link"></i> View', 'class="btn btn-xs btn-default"');
                                    echo anchor(site_url(Backend_URL . 'testimonial/update/' . $testimonial->id), '<i class="fa fa-fw fa-edit"></i> Edit', 'class="btn btn-xs btn-default"');
                                    echo anchor(site_url(Backend_URL . 'testimonial/delete/' . $testimonial->id), '<i class="fa fa-fw fa-trash"></i> Delete ', 'class="btn btn-xs btn-danger" onclick="javasciprt: return confirm(\'Are You Sure ?\')"');
                                    ?>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>