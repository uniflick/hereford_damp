<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="carousel slide" data-ride="carousel" id="quote-carousel">
                <div class="carousel-inner text-center">

                    <?php
                    foreach ($testimonials_data as $k => $testimonials) {
                    $active = ($k == 0) ? " active" : "";
                    ?>
                    <div class="item <?php echo $active; ?>">
                        <blockquote>
                            <div class="row">
                                <div class="col-sm-8 col-sm-offset-2">
                                    <p><?php  echo getShortContent($testimonials->content,150)?></p>
                                    <small><b><?php echo $testimonials->name ?></b></small>
                                </div>
                            </div>
                        </blockquote>
                    </div>
                    <?php } ?>

                </div>
                <!-- Bottom Carousel Indicators -->
                <ol class="carousel-indicators">
                    <?php
                    $start = 0;
                    foreach ($testimonials_data as $k => $testimonials) {
                    $active = ($k == 0) ? " active" : "";
                    ?>

                    <li data-target="#quote-carousel" data-slide-to="<?php echo $start ?>" class="<?php echo $active; ?>"><img class="img-responsive " src="uploads/testimonials/no-thumb.jpg" alt="">
                    </li>

                    <?php
                        $start++;
                    }
                    ?>

                </ol>

                <!-- Carousel Buttons Next/Prev -->
                <a data-slide="prev" href="#quote-carousel" class="left carousel-control"><i class="fa fa-chevron-left"></i></a>
                <a data-slide="next" href="#quote-carousel" class="right carousel-control"><i class="fa fa-chevron-right"></i></a>
            </div>
        </div>
    </div>
</div>