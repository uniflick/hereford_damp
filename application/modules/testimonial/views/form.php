<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<section class="content-header">
    <h1> Testimonial  <small><?php echo $button ?></small> 
        <a href="<?php echo site_url(Backend_URL . 'testimonial') ?>" class="btn btn-default">Back</a> 
    </h1>
    <ol class="breadcrumb">
        <li><a href="admin/"><i class="fa fa-dashboard"></i> Admin</a></li>
        <li><a href="admin/testimonial">Testimonial</a></li>
        <li class="active">Add New</li>
    </ol>
</section>

<section class="content">       
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Add New</h3>
        </div>

        <div class="box-body">
            <form class="form-horizontal" action="<?php echo $action; ?>" enctype="multipart/form-data" method="post">
                <input type="hidden" name="user_id" value="<?php echo $user_id; ?>" />
                <input type="hidden" name="id" value="<?php echo $id; ?>" />
                <div class="form-group">
                    <label for="title" class="col-sm-2 control-label">Title :</label>
                    <div class="col-sm-10">                    
                        <input type="text" class="form-control" name="title" id="name" placeholder="Title" value="<?php echo $title; ?>" />
                        <?php echo form_error('title') ?>
                    </div>
                </div>

                <div class="form-group">
                    <label for="content" class="col-sm-2 control-label">Content :</label>
                    <div class="col-sm-10">                    
                        <textarea class="form-control" name="content" id="content"><?php echo ($content); ?></textarea>
                        <?php echo form_error('content') ?>
                    </div>
                </div>

                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">Name :</label>
                    <div class="col-sm-10">                    
                        <input type="text" class="form-control" name="name" id="name" placeholder="Name" value="<?php echo $name; ?>" />
                        <?php echo form_error('name') ?>
                    </div>
                </div>
                <div class="form-group">
                    <label for="thumb" class="col-sm-2 control-label">Photo :</label>
                    <div class="col-sm-6"> 
                        <div class="btn btn-default btn-file">
                            <i class="fa fa-picture-o"></i> Set Thumbnail 
                            <input type="file" name="thumb" class="file_select" onchange="photoPreview(this, '.upload_image')">
                        </div>
                        <input type="hidden" name="old_photo" value="<?php echo $old_photo; ?>" />
                        <?php echo form_error('thumb') ?>
                        <div class="thumbnail upload_image">
                            <img src="<?php echo getPhoto($photo, 'full'); ?>" alt="Thumbnail"> 
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="status" class="col-sm-2 control-label">Status :</label>
                    <div class="col-sm-10" style="padding-top: 8px;">
                        <?php echo htmlRadio('status', $status, array('Active' => 'Publish', 'Inactive' => 'Draft')); ?>
                        <?php echo form_error('status') ?>
                    </div>
                </div>

                <div class="form-group">
                    <label for="date" class="col-sm-2 control-label">Date :</label>
                    <div class="col-sm-10">

                        <div class="form-group input-group">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i> </span>                  
                            <input type="text" class="form-control js_datepicker" name="date" id="date" placeholder="Date" value="<?php echo $date; ?>" />
                        </div>


                    </div>
                </div>

                <div class="col-md-12 text-right">    
                    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
                    <a href="<?php echo site_url('admin/testimonial') ?>" class="btn btn-default">Cancel</a>
                </div></form>
        </div>
    </div>
</section>
