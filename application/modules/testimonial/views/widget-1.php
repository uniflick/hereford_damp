<section id="carousel">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="quote"><i class="fa fa-quote-left fa-4x"></i></div>
                <div class="carousel slide" id="fade-quote-carousel" data-ride="carousel" data-interval="3000">                    
                    <ol class="carousel-indicators">
                        <?php
                        $start = 0;
                        foreach ($testimonials as $k => $testimonial) {
                            $active = ($k == 0) ? ' active' : '';
                        ?>
                        <li data-target="#fade-quote-carousel" 
                            data-slide-to="<?php echo ++$start; ?>" 
                            class="<?php echo $active; ?>">
                        </li>
                        <?php } ?>
                    </ol>
                    <!-- Carousel items -->
                    <div class="carousel-inner">
                        <?php
                        foreach ($testimonials as $k => $testimonial) {
                            $active = ($k == 0) ? ' active' : '';
                        ?>
                            <div class="item <?php echo $active; ?>">
                                <div class="profile-circle" style="background-image: url('uploads/testimonials/no-thumb-1.jpg');">
                                </div>
                                <blockquote>
                                    <p> <?php  echo getShortContent($testimonial->content,150)?>
                                        &nbsp;
                                        <b><?php echo $testimonial->name; ?></b>
                                    </p>
                                </blockquote>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>