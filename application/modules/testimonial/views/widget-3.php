
<div class="carousel-reviews broun-block">
    <div class="container">
        <div class="row">
            <div id="carousel-reviews" class="carousel slide" data-ride="carousel">

                <div class="carousel-inner">

                    <?php
                    $total=count($testimonials_data);

                    foreach ($testimonials_data as $k => $testimonials) {
                        $active = ($k == 0) ? " active" : "";
                        if( $k%3 == 0 ){ ?>
                            <div class="item <?php echo $active; ?>">
                        <?php }   ?>


                                <div class="col-md-4 col-sm-6">
                                    <div class="block-text rel zmin">
                                        <p><?php  echo getShortContent($testimonials->content,150)?></p>
                                        <ins class="ab zmin sprite sprite-i-triangle block"></ins>
                                    </div>
                                    <div class="person-text rel">
                                        <img src="uploads/testimonials/no-thumb-1.jpg"/>
                                        <a title="" href="#"><b><?php echo $testimonials->name; ?></b></a>
                                    </div>
                                </div>

                        <?php if(($k%3 ==2 && $k!=0)||($k == $total-1)){ ?>
                            </div>
                        <?php }   ?>

                    <?php } ?>

                </div>


                <a class="left carousel-control" href="#carousel-reviews" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                </a>
                <a class="right carousel-control" href="#carousel-reviews" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                </a>


            </div>
        </div>
    </div>
</div>