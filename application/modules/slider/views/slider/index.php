<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<section class="content-header">
    <h1> Slider  <small>Control panel</small> </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo site_url(Backend_URL) ?>"><i class="fa fa-dashboard"></i> Admin</a></li>
        <li class="active">Slider</li>
    </ol>
</section>

<section class="content">

    <div class="row">
        <div class="col-md-8 col-xs-12">

            <div class="box box-primary">            
                <div class="box-header with-border">                                   
                    <h3 class="box-title">List Slider Show</h3>
                </div>

                <div class="box-body">
                    <?php echo $this->session->flashdata('message'); ?>
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-condensed">
                            <thead>
                                <tr>
                                    <th width="120">Thumb</th>
                                    <th>Name</th>                                    
                                    <th width="120" class="text-center">Action</th>
                                </tr>
                            </thead>

                            <tbody>
                                <?php foreach ($sliders as $slider) { ?>
                                    <tr>
                                        <td><img src="<?php echo getPhoto($slider->thumb); ?>" class="img-responsive" width="120"/></td>
                                        <td>
                                            <p><?php echo $slider->name; ?><br/>
                                                Slide: <?php echo $slider->slide; ?></p>
                                            <code>&lt;?php echo view_slideshow(<?php echo $slider->id; ?>); ?&gt;</code>
                                        
                                        </td>                                        
                                        <td class="text-center">
                                            <?php
                                            echo anchor(site_url(Backend_URL . 'slider/add/?slider_id=' . $slider->id), '<i class="fa fa-fw fa-plus"></i>', 'class="btn btn-xs btn-success" title="Add Slide"');
                                            echo anchor(site_url(Backend_URL . 'slider/update/' . $slider->id), '<i class="fa fa-fw fa-edit"></i>', 'class="btn btn-xs btn-default" title="Edit"');
                                            echo anchor(site_url(Backend_URL . 'slider/delete/' . $slider->id), '<i class="fa fa-fw fa-trash"></i>', 'onclick="return confirm(\'Confirm Delete\')" class="btn btn-xs btn-danger" title="Delete"');
                                            ?>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>


                    <div class="row">                
                        <div class="col-md-6">
                            <span class="btn btn-primary">Total Slider: <?php echo $total_rows ?></span>

                        </div>
                        <div class="col-md-6 text-right">
                            <?php echo $pagination ?>
                        </div>                
                    </div>
                </div>
            </div>
        </div>   
        
        <div class="col-md-4 col-xs-12">            
            <div class="box box-primary">                
                <div class="box-header with-border">
                    <h3 class="box-title">
                        <i class="fa fa-plus" aria-hidden="true"></i> Add New
                    </h3>
                </div>

                <div class="box-body">
                    <div style="padding:15px 0px;">
                        <?php echo form_open(Backend_URL . 'slider/create_action', array('class' => 'form-horizontal', 'method' => 'post')); ?>
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="name">Slide Name</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="name" id="name" placeholder="Name" />
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="width">Width x Height</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="option[width]" id="width" placeholder="width" value="100%" />
                            </div>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="option[height]" id="height" placeholder="height" value="350" />
                            </div>
                        </div>
                        
                        
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="timeout">Timeout</label>                            
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="option[timeout]" id="timeout" value="5000" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="speed">Speed</label>                            
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="option[speed]" id="speed" value="1000" />
                            </div>
                        </div>
                        
                        
                        
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="control">Control</label>
                            <div class="col-md-8">
                                <?php echo htmlRadio('option[control]', 'Yes', ['Yes' => 'Yes', 'No' => 'No']);?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="name">Control Type</label>
                            <div class="col-md-8">
                                <select class="form-control" name="option[control_type]">
                                    <?php 
                                    echo selectOptions('Pre - Next', [
                                            'Numeric' => 'Numeric', 
                                            'Pre - Next' => 'Pre - Next'
                                        ]);
                                    ?>                                
                                </select>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="effect">Select Effect</label>
                            <div class="col-md-8">
                                <select class="form-control" id="effect" name="option[effect]">
                                    <?php echo all_effects(''); ?>
                                </select>
                            </div>
                        </div>
                        
                       
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="name">Show Title</label>
                            <div class="col-md-8">
                                <?php echo htmlRadio('option[show_title]', 'No', ['Yes' => 'Yes', 'No' => 'No']);?>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="caption">Caption</label>
                            <div class="col-md-8">
                                <?php echo htmlRadio('option[caption]', 'No', ['Yes' => 'Yes', 'No' => 'No']);?>
                            </div>
                        </div>
                        
                        <input type="hidden" name="option[js_class]" value="<?php echo 'slider_'.time(); ?>"/>        
                        <button type="submit" class="btn btn-primary">Save New</button> 
                        <button type="reset" class="btn btn-default">Reset</button> 
                        <?php echo form_close(); ?>

                    </div>                    
                </div>
                
                
            </div>
        </div>

    </div>
</section>