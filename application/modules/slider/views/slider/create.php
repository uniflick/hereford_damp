<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<section class="content-header">
    <h1> Slider  <small><?php echo $button ?></small> <a href="<?php echo site_url(Backend_URL . 'slider') ?>" class="btn btn-default">Back</a> </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo Backend_URL ?>"><i class="fa fa-dashboard"></i> Admin</a></li>
        <li><a href="<?php echo Backend_URL ?>slider">Slider</a></li>
        <li class="active">Add New</li>
    </ol>
</section>

<section class="content">  
    <div class="row">
        <div class="col-md-6">
            
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Add New Slider</h3>
        </div>


        <div class="box-body">
            <div style="padding:15px 0px;">
                <?php echo form_open(Backend_URL . 'slider/create_action', array('class' => 'form-horizontal', 'method' => 'post')); ?>
                <div class="form-group">
                    <label class="col-md-4 control-label" for="name">Slide Name</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="name" id="name" placeholder="Name" />
                        <?php echo form_error('name') ?>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label" for="width">Width x Height</label>
                    <div class="col-md-4">
                        <input type="text" class="form-control" name="option[width]" id="width" placeholder="width" />
                    </div>
                    <div class="col-md-4">
                        <input type="text" class="form-control" name="option[height]" id="height" placeholder="height" />
                    </div>
                </div>


                <div class="form-group">
                    <label class="col-md-4 control-label" for="timeout">Timeout</label>                            
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="option[timeout]" id="timeout" value="5000" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label" for="speed">Speed</label>                            
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="option[speed]" id="speed" value="1000" />
                    </div>
                </div>



                <div class="form-group">
                    <label class="col-md-4 control-label" for="control">Control</label>
                    <div class="col-md-8">
                        <?php echo htmlRadio('option[control]', 'Yes', ['Yes' => 'Yes', 'No' => 'No']); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label" for="name">Control Type</label>
                    <div class="col-md-8">
                        <select class="form-control" name="option[control_type]">
                            <?php
                            echo selectOptions('Pre - Next', [
                                'Numeric' => 'Numeric',
                                'Pre - Next' => 'Pre - Next'
                            ]);
                            ?>                                
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label" for="effect">Select Effect</label>
                    <div class="col-md-8">
                        <select class="form-control" id="effect" name="option[effect]">
                            <?php echo all_effects(''); ?>
                        </select>
                    </div>
                </div>


                <div class="form-group">
                    <label class="col-md-4 control-label" for="name">Show Title</label>
                    <div class="col-md-8">
                        <?php echo htmlRadio('option[show_title]', 'No', ['Yes' => 'Yes', 'No' => 'No']); ?>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label" for="caption">Caption</label>
                    <div class="col-md-8">
                        <?php echo htmlRadio('option[caption]', 'No', ['Yes' => 'Yes', 'No' => 'No']); ?>
                    </div>
                </div>
                
                <div class="form-group">
                    <div class="col-md-8 col-md-offset-4">
                        <input type="hidden" name="option[js_class]" value="<?php echo 'slider_' . time(); ?>"/>        
                        <button type="submit" class="btn btn-primary">Save New</button> 
                        <a href="admin/slider" class="btn btn-default">Cancel</a> 
                        <?php echo form_close(); ?>

                    </div>                    
                </div>                    
            </div>                    
        </div>



    </div>

        </div>
    </div>
</section>