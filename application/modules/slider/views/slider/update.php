<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php load_module_asset('users', 'css'); ?>
<section class="content-header">
    <h1>Slider<small><?php echo $button ?></small> </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo Backend_URL ?>"><i class="fa fa-dashboard"></i> Admin</a></li>
        <li><a href="<?php echo Backend_URL ?>slider">Slider</a></li>
        <li class="active">Update</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-4">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Update Slider Setting</h3>
                    <?php echo $this->session->flashdata('message'); ?>
                </div>
                <div class="box-body">
                    <div style="padding:15px 0px;">
                        <?php echo form_open($action, array('class' => 'form-horizontal', 'method' => 'post')); ?>
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="name">Slide Name</label>
                            <div class="col-md-8">
                                <input type="text" value="<?php echo $name; ?>" class="form-control" name="name" id="name" placeholder="Name" />
                                <?php echo form_error('name') ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label" for="width">Width x Height</label>
                            <div class="col-md-4">
                                <input type="text" value="<?php echo $option->width; ?>" class="form-control" name="option[width]" id="width" placeholder="width" />
                            </div>
                            <div class="col-md-4">
                                <input type="text" value="<?php echo $option->height; ?>" class="form-control" name="option[height]" id="height" placeholder="height" />
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-md-4 control-label" for="timeout">Timeout</label>                            
                            <div class="col-md-8">
                                <input type="text" value="<?php echo $option->timeout; ?>"  class="form-control" name="option[timeout]" id="timeout" value="5000" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="speed">Speed</label>                            
                            <div class="col-md-8">
                                <input type="text" value="<?php echo $option->speed; ?>" class="form-control" name="option[speed]" id="speed" value="1000" />
                            </div>
                        </div>



                        <div class="form-group">
                            <label class="col-md-4 control-label" for="control">Control</label>
                            <div class="col-md-8">
                                <?php echo htmlRadio('option[control]', $option->control, ['Yes' => 'Yes', 'No' => 'No']); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="name">Control Type</label>
                            <div class="col-md-8">
                                <select class="form-control" name="option[control_type]">
                                    <?php
                                    echo selectOptions($option->control_type, [
                                        'Numeric' => 'Numeric',
                                        'Pre - Next' => 'Pre - Next'
                                    ]);
                                    ?>                                
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label" for="effect">Select Effect</label>
                            <div class="col-md-8">
                                <select class="form-control" id="effect" name="option[effect]">
                                    <?php echo all_effects($option->effect); ?>
                                </select>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-md-4 control-label" for="name">Show Title</label>
                            <div class="col-md-8">
                                <?php echo htmlRadio('option[show_title]', $option->show_title, ['Yes' => 'Yes', 'No' => 'No']); ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label" for="caption">Caption</label>
                            <div class="col-md-8">
                                <?php echo htmlRadio('option[caption]', $option->caption, ['Yes' => 'Yes', 'No' => 'No']); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="status">Status</label>
                            <div class="col-md-8">
                                <?php echo htmlRadio('option[status]', $option->status, ['Publish' => 'Publish', 'Draft' => 'Draft']); ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <input type="hidden" name="option[js_class]" value="<?php echo $option->js_class; ?>"/>        
                                <input type="hidden" name="id" value="<?php echo $id; ?>"/>        
                                <button type="submit" class="btn btn-primary">Save New</button> 
                                <a href="admin/slider" class="btn btn-default">Cancel</a> 
                                <?php echo form_close(); ?>

                            </div>                    
                        </div>                    
                    </div>                    
                </div>

            </div>
        </div>
        <div class="col-md-8">

            <div class="box box-primary">
                <div class="box-header with-border">
                    <a class="pull-right btn btn-primary" href="admin/slider/add/?slider_id=<?php echo $id;?>">+ Add New Slide</a>
                    <h3 class="box-title">
                        List of Slider 
                        <span class="text-red"><em>( Drag Drop To Reorder Serial )</em></span>
                        
                        
                    </h3>                    
                </div>
                <div class="box-body">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th width="40">S/L</th>
                            <th width="120">Thumb</th>
                            <th>Title & Caption</th>
                            <th width="140" class="text-center">Action</th>
                        </tr>
                        <?php foreach($slides as $slide){?>
                            <tr id="row_<?php echo $slide->id; ?>">
                                <td><?php echo ++$sl; ?></td>
                                <td><img src="<?php echo getPhoto($slide->thumb); ?>" class="img-responsive" width="120"/></td>
                                <td>
                                    <h4 class="no-margin">Title: <?php echo $slide->post_title; ?></h4>
                                    <p>Caption: <?php echo $slide->content; ?></p>
                                </td>
                                <td  class="text-center">
                                    <a href="admin/slider/edit/<?php echo $slide->id; ?>" class="btn btn-primary btn-xs"><i class="fa fa-edit"></i> Edit</a>
                                    <span id="<?php echo $slide->id; ?>" class="btn btn-xs remove btn-danger"><i class="fa fa-times"></i> Remove</span>
                                </td>                                
                            </tr>                        
                        <?php } ?>
                    </table>
                    
                    
                </div>

            </div>
        </div>
    </div>


</section>
<script>
    $('.remove').on('click', function(){
        var id = $(this).attr('id');
        var yes = confirm('Confirm Delete');
        if(yes){
            $.ajax({
                url: 'admin/slider/delete_slide',
                type: "POST",
                dataType: "json",
                data: { id: id },
                beforeSend: function(){
                    $(`#row_${id}`).css('background-color','red');
                },
                success: function( jsonData ){
                    if(jsonData.Status === 'OK'){
                        $(`#row_${id}`).fadeOut('Slow');                    
                    } else {
                        $(`#row_${id}`).css('background-color','none');
                    }                                    
                }
            }); 
        }
    });
</script>