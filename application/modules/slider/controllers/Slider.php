<?php defined('BASEPATH') OR exit('No direct script access allowed');
 
/* Author: Khairul Azam
 * Date : 2019-10-05
 */

class Slider extends Admin_controller{
    function __construct(){
        parent::__construct();
        $this->load->model('Slider_model');
        $this->load->helper('slider');
        $this->load->library('form_validation');
    }

    public function index(){
        
        $start = intval($this->input->get('start'));
        
        $config['base_url'] = build_pagination_url( Backend_URL . 'slider/', 'start');
        $config['first_url'] = build_pagination_url( Backend_URL . 'slider/', 'start');

        $config['per_page'] = 25;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Slider_model->total_rows();
        $sliders = $this->Slider_model->get_limit_data($config['per_page'], $start);
                
        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'sliders' => $sliders,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $this->viewAdminContent('slider/slider/index', $data);
    }
 
    public function create(){
        $data = array(
            'button' => 'Create',
            'action' => site_url( Backend_URL . 'slider/create_action'),
	    'id' => set_value('id'),
	    'parent' => set_value('parent'),
	    'type' => set_value('type'),
	    'name' => set_value('name'),
	    'url' => set_value('url'),
	    'template' => set_value('template'),
	    'description' => set_value('description'),
	    'thumb' => set_value('thumb'),
	);
        $this->viewAdminContent('slider/slider/create', $data);
    }
    
    public function create_action(){
            
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'parent'    => 0,
		'type'      => 'Slider',
		'name'      => $this->input->post('name',TRUE),
		'url'       => uniqid('slider_'),
		'template'  => '',
		'description' => json_encode($this->input->post('option')),
		'thumb'     => '',
	    );

            $this->Slider_model->insert($data);
            $this->session->set_flashdata('message', '<p class="ajax_success">Slider Added Successfully</p>');
            redirect(site_url( Backend_URL. 'slider'));
        }
    }
    
    public function update($id){
        $row = $this->Slider_model->get_by_id($id);
        
        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url( Backend_URL . 'slider/update_action'),
		'id' => set_value('id', $row->id),
		'name' => set_value('name', $row->name),
		'option' => $this->option($row->description),
		'slides' => $this->Slider_model->get_slides($id),
		'sl' => 0
	    );
            
            //dd( $data );
            
            $this->viewAdminContent('slider/slider/update', $data);
        } else {
            $this->session->set_flashdata('message', '<p class="ajax_error">Slider Not Found</p>');
            redirect(site_url( Backend_URL. 'slider'));
        }
    }
    
    private function option($json=''){
        $default_opt = [
            'width' => '1170',
            'height' => '550',
            'timeout' => '50000',
            'speed' => '10000',
            'control' => 'Yes',
            'control_type' => 'Numeric',
            'effect' => '',
            'show_title' => 'No',
            'caption' => 'No',
            'status' => 'Publish',
            'js_class' => '',
        ];
        $saved = json_decode($json, true);
        return (object) array_merge($default_opt, $saved);        
    }

    public function update_action(){                
        
        $this->_rules();

        $id = $this->input->post('id', TRUE);
        if ($this->form_validation->run() == FALSE) {
            $this->update( $id );
        } else {
            $data = array(				
		'name' => $this->input->post('name',TRUE),
                'description' => json_encode($this->input->post('option')),
	    );

            $this->Slider_model->update($id, $data);
            $this->session->set_flashdata('message', '<p class="ajax_success">Slider Updated Successlly</p>');
            redirect(site_url( Backend_URL. 'slider/update/'. $id ));
        }
    }

    public function delete($id){
        $row = $this->Slider_model->get_by_id($id);
        if ($row){
                        
            $this->db->where('parent_id',$id)->where('post_type','slide');
            $slides = $this->db->select('thumb')->get('cms')->result();
            foreach($slides as $slide ){
                removeImage($slide->thumb);
            }
            $this->db->where('parent_id',$id)->where('post_type','slide');
            $this->db->delete('cms');
            
            $this->Slider_model->delete($id);
            
            $this->session->set_flashdata('message', '<p class="ajax_success">Slider Deleted Successfully</p>');
            redirect(site_url( Backend_URL. 'slider'));
        } else {
            $this->session->set_flashdata('message', '<p class="ajax_error">Slider Not Found</p>');
            redirect(site_url( Backend_URL. 'slider'));
        }
    }

    public function _menu(){
         return add_main_menu('Slider', 'slider', 'slider', 'fa-photo');        
    }

    public function _rules(){
	$this->form_validation->set_rules('name', 'name', 'trim|required');
	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }
    

    public function add(){
        $id = (int) $this->input->get('slider_id');
        $data = array(
            'button' => 'Create',
            'action' => site_url( Backend_URL . 'slider/add_action'),
	    'id' => set_value('id'),
	    'parent_id' => set_value('parent_id', $id),
	    'title' => set_value('title'),
	    'caption' => set_value('caption'),
	    'thumb' => set_value('thumb'),
	);
        $this->viewAdminContent('slider/slider/add_slide', $data);
    }
    
    public function add_action(){
       $photo = uploadPhoto($_FILES['thumb'], 'uploads/cms/slider/', date('Y-m-d-H-i-s_') . rand(0, 9));

        $data = array(
            'user_id'       => $this->user_id,
            'parent_id'     => (int) $this->input->post('parent_id'),
            'post_type'     => 'slide',
            'menu_name'     => '',
            'post_title'    => $this->input->post('title'),
            'post_url'      => uniqid('slide_'),
            'content'       => $this->input->post('content'),
            'seo_title'     => '',
            'seo_keyword'   => '',
            'seo_description' => '',
            'thumb'         => $photo,
            'template'      => '',
            'status'        => 'Publish',
            'page_order'    => 0,
            'created'       => date('Y-m-d H:i:s'),
            'modified'      => date('Y-m-d H:i:s'),
        );

        $this->db->insert('cms',$data);
        $this->session->set_flashdata('message', '<p class="ajax_success">Slider Added Successfully</p>');
        redirect(site_url( Backend_URL. 'slider/update/' . $data['parent_id']));

    }

    public function edit( $id ){
        $slide = $this->Slider_model->get_slide_by_id( $id );
        $data = array(
            'button' => 'Update',
            'action' => site_url( Backend_URL . 'slider/edit_action'),
	    'id' => set_value('id', $slide->id ),
	    'parent_id' => set_value('parent_id', $slide->parent_id ),
	    'title' => set_value('title', $slide->post_title ),
	    'content' => set_value('content', $slide->content ),
	    'thumb' => set_value('thumb', $slide->thumb ),
	);
        $this->viewAdminContent('slider/slider/edit_slide', $data);
    }
    
    public function edit_action(){
        $id     = intval($this->input->post('id', TRUE));
        $row    = $this->db->get_where('cms', ['id' => $id])->row();
        
        $photo      = uploadPhoto($_FILES['thumb'], 'uploads/cms/slider/', date('Y-m-d-H-i-s_') . rand(0, 9));
        
        if (empty($_FILES['thumb']['name'])) {
            $photo = $row->thumb;
        } else {
            removeImage($row->thumb);
        }
        
        $id         =  (int) $this->input->post('id');
        $parent_id  =  (int) $this->input->post('parent_id');
        $data = array(
            'parent_id'     => (int) $this->input->post('parent_id'),
            'post_title'    => $this->input->post('title'),
            'content'       => $this->input->post('content'),
            'thumb'         => $photo,
        );

        $this->db->where('id', $id );
        $this->db->update('cms',$data);
        $this->session->set_flashdata('message', '<p class="ajax_success">Slider Updated Successfully</p>');
        redirect(site_url( Backend_URL. 'slider/update/' . $parent_id ));

    }
    
    public function delete_slide(){
        ajaxAuthorized();
        $id = $this->input->post('id');
        $slide = $this->Slider_model->get_slide_by_id( $id );
        if($slide){
            removeImage($slide->thumb);
            $this->db->where('id',$id);
            $this->db->delete('cms');
        }        
        echo ajaxRespond('OK','Deleted');
    }

    
}