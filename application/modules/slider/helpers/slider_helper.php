<?php

defined('BASEPATH') OR exit('No direct script access allowed');

function getSliderNameDropDown($id=0) {
    $ci =& get_instance();
    $ci->db->where('type', 'Slider');
    $sliders = $ci->db->get('cms_options')->result();
    $options = '<option value="0">--Select --</option>';
    foreach($sliders as $slider){
        $options .= '<option value="' . $slider->id . '" ';
        $options .= ($slider->id == $id ) ? ' selected="selected"' : '';
        $options .= '>' . $slider->name . '</option>';
    }
    return $options;
}

function all_effects($selected_effect) {
	$effects = ['fadeout', 'scrollUp', 'scrollDown', 'scrollLeft', 
                    'scrollRight', 'scrollHorz', 'scrollVert', 'soptiondeX',
                    'soptiondeY', 'shuffle', 'turnUp', 'turnDown', 'turnLeft', 
                    'turnRight', 'zoom', 'fadeZoom', 'boptionndX', 'boptionndY',
                    'boptionndZ', 'growX', 'growY', 'curtainX', 'curtainY', 
                    'cover', 'uncover', 'toss', 'wipe'];
        
	$options = '';
	foreach($effects as $effect){
            $options .= '<option value="'.$effect.'"';
            $options .= ($selected_effect == $effect) ? ' selected="selected"' : '';
            $options .= '>'. ucfirst($effect) .'</option>';
	}
	return $options;
}


function get_banner_thumb__new($post_id) {
	$result = mysql_fetch_array(mysql_query("SELECT `thumb` FROM `cms_content` WHERE `post_id` = '$post_id'"));

	if(empty($result['thumb'])) {
		print '<img src="'. ImageResize . BannerDir . 'no-thumb.gif&w=120px&h=100&zc=1" alt="Thumb"/>';
	} else {
		print '<img src="'. ImageResize . BannerDir . $result['thumb']. '&w=120px&h=100&zc=1" alt="Thumb"/>';
	}
}



function slide_show_name($id){

	$result = mysql_fetch_array(mysql_query("SELECT * FROM `options` WHERE `opt_id` = '$id'"));
	return $result['name'];
	
}

function view_slideshow($id){
    $ci =& get_instance();
    $ci->db->select('description as config');
    $ci->db->where('id',$id);
    $slider = $ci->db->get('cms_options')->row();
    if(!$slider){ return "<p style='padding:25px;'><em style='color:#CCC;'>Slider Not Found. Please check slider id#{$id}</em></p>"; }
    
    $option     = json_decode($slider->config);    
    $settings   = options($option, $id);   
    
//    if($option->status == 'Draft'){
//        return false;
//    }
    
        
    $ci->db->select('id,post_title as title, content,thumb');
    $ci->db->where('parent_id',$id);
    $ci->db->where('post_type','slide');
    $slides = $ci->db->get('cms')->result();
    
    $rows = '';

    $rows .= '<script src="assets/lib/plugins/bxslide/jquery.bxslider.min.js" type="text/javascript"></script>';
    $rows .= '<link href="assets/lib/plugins/bxslide/jquery.bxslider.css" rel="stylesheet" />'; 
    $rows .= '<div class="bxslider">'; 
    
    foreach($slides as $slide){ 
        $rows .='<div class="item">';
        $rows .= "<img src=\"{$slide->thumb}\">";
        $rows .= "<div class='container'><div class='slide-text'>";
        $rows .= $slide->content;
        $rows .= '</div></div>';
        $rows .= '</div>';
    }
    $rows .= "</div>";
    $rows .= "<script type='text/javascript'>
        jQuery(function($){
          $('.bxslider').bxSlider({
          mode: 'fade',
          auto: true,
          controls: true,
          autoHover:true,
          pager: false,
          pause: 5000,
          speed:2000,
          adaptiveHeight: true
          });
        });
    </script>";

    return $rows;
}

function options($option){
    $settngs = "data-cycle-fx={$option->effect}";
    $settngs .= " data-cycle-timeout={$option->timeout}";
    
    if($option->caption == 'Yes'){            
        $settngs .= ' data-cycle-caption-plugin=caption2'. "\r\n";
        $settngs .= ' data-cycle-overlay-fx-out="slideUp"' . "\r\n";
        $settngs .= ' data-cycle-overlay-fx-in="slideDown"' . "\r\n";
    }
    if($option->control == 'Yes'){
        $settngs .= ' data-cycle-next="#next"';
        $settngs .= ' data-cycle-prev="#prev"';	
    }
    return $settngs;       
}