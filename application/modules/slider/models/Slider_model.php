<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Slider_model extends Fm_model {

    public $table = 'cms_options';
    public $id = 'id';
    public $order = 'DESC';

    function __construct() {
        parent::__construct();
    }

    // get total rows
    function total_rows() {       
        $this->db->from($this->table);
        $this->db->where('type','Slider');
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0) {
        
        $this->db->select('thumb');        
	$this->db->where('parent_id = s.id');        
	$this->db->limit(1);        
	$this->db->order_by('id','ASC');        
	$thumb = $this->db->get_compiled_select('cms');  
        
        $this->db->select('count(*)');        
	$this->db->where('parent_id = s.id');  
	$qty = $this->db->get_compiled_select('cms');  

        
        $this->db->select("s.id,s.name,");
        $this->db->select("({$thumb}) as thumb, ({$qty}) as slide");
        $this->db->from('cms_options as s');
        
        $this->db->order_by($this->id, $this->order);        
        $this->db->where('type','Slider');
        $this->db->limit($limit, $start);
        return $this->db->get()->result();
    }
    
    function get_slides( $slider_id ) {
                
        $this->db->select('id,post_title,content,thumb');
        $this->db->from('cms');        
        $this->db->order_by('page_order', 'ASC');        
        $this->db->where('post_type','slide');
        $this->db->where('parent_id', $slider_id);
        return $this->db->get()->result();
    }
    
    function get_slide_by_id( $id ) {
                
        $this->db->select('id,parent_id,post_title,content,thumb');
        $this->db->from('cms');       
        $this->db->where('post_type','slide');
        $this->db->where('id', $id );
        return $this->db->get()->row();
    }

}
