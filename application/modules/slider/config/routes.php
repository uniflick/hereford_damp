<?php defined('BASEPATH') OR exit('No direct script access allowed');

$route['admin/slider']                  = 'slider';
$route['admin/slider/create']           = 'slider/create';
$route['admin/slider/update/(:num)']    = 'slider/update/$1';
$route['admin/slider/delete/(:num)']    = 'slider/delete/$1';
$route['admin/slider/create_action']    = 'slider/create_action';
$route['admin/slider/update_action']    = 'slider/update_action';
$route['admin/slider/delete_action/(:num)']    = 'slider/delete_action/$1';

$route['admin/slider/add']           = 'slider/add';
$route['admin/slider/add_action']    = 'slider/add_action';
$route['admin/slider/edit/(:num)']   = 'slider/edit/$1';
$route['admin/slider/edit_action']   = 'slider/edit_action';
$route['admin/slider/delete_slide']  = 'slider/delete_slide';
