<?php

$route['admin/db_sync']                 = 'db_sync';
$route['admin/db_sync/exportTable']     = 'db_sync/exportTable';
$route['admin/db_sync/backup_full_db']  = 'db_sync/backup_full_db';
$route['admin/db_sync/upload_sql_file']  = 'db_sync/upload_sql_file';
$route['admin/db_sync/restore_full_db'] = 'db_sync/restore_full_db';
$route['admin/db_sync/truncateTable']   = 'db_sync/truncateTable';
$route['admin/db_sync/restore']         = 'db_sync/restore';


$route['admin/db_sync/delete']   = 'db_sync/delete';
