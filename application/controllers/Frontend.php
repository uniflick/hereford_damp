<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Frontend extends Frontend_controller {
    // every thing coming form Frontend Controller
       
    public function __construct() {
        parent::__construct();
        $this->load->helper('slider/slider');                       
    }        
    
    public function search() {
        $keyword      = urldecode($this->input->get('keyword', TRUE));
        $this->db->select('post_title,content,post_url');
        if ($keyword) {
            $this->db->like('cms.post_title', $keyword);
            $this->db->or_like('cms.content', $keyword);
        }
        
        $result = $this->db->get('cms');

        $data['results']    = $result->result();
        $data['count']      = $result->num_rows();        
        
        self::viewFrontContent('frontend/template/search',$data);
    }
    
    public function event() {
        $page   = (int) $this->input->get('p');
        $limit  = 5;
        $start  = startPointOfPagination($limit, $page);
        $target = build_pagination_url('upcoming-events', 'p', true);        

        $total = $this->db->get_where('events', ['expire >=' => date('Y-m-d')])->num_rows();
        $start      = startPointOfPagination($limit, $page);
        $paginator  = getPaginator($total, $page, $target, $limit);        
        
        $this->db->limit($limit,$start);        
        $this->db->order_by('event_date', 'ASC');       
        
        $post_data = $this->db->get_where('events', ['expire >=' => date('Y-m-d')])->result();
//        echo $this->db->last_query();
        $data = array(
            'posts_data' => $post_data,
            'pagination' => $paginator,
            'start' => $start
        );
        
        $this->viewFrontContent('frontend/template/events',$data);        
    }
    
    public function event_detail($url) {
        $row = $this->db->get_where('events', array('url' => $url, 'status' => 'Active'))->row();
        if($row){
        $data = array(
            'id' => $row->id,
            'title' => $row->title,
            'url' => $row->url,
            'content' => $row->content,
            'thumb' => $row->thumb,
            'expire' => $row->expire,
        );
        $this->viewFrontContent('frontend/template/event-details', $data);
        }
        else {
            $this->viewFrontContent('frontend/404');
        }
    }
      
    public function news() {
        $limit = 10;
        $target = 'news?p';

        $currentPage = intval($this->input->get('p'));
        $catName = $this->uri->segment(2);
        if ($catName) {
            $catId = $this->db->select('id')->get_where('cms_options', array('url' => $catName))->row();
            $catId = intval($catId->id);
            $target = 'news/' . $catName . '?p';
        } else {
            $catId = 0;
            $target = 'news?p';
        }

        $this->db->select('id');
        $this->db->from('cms');
        $this->db->where('post_type', 'post');
        if ($catId) {
            $this->db->where('parent_id', $catId);
        }

        $total = $this->db->get()->num_rows();
        $start = startPointOfPagination($limit, $currentPage);
        $paginator = getPaginator($total, $currentPage, $target, $limit);

        $this->db->from('cms');
        $this->db->where('post_type', 'post');
        if ($catId) {
            $this->db->where('parent_id', $catId);
        }
        $this->db->order_by('id', 'DESC');
        $this->db->limit($limit, $start);

        $news_posts = $this->db->get()->result();

        $data = array(
            'posts' => $news_posts,
            'pagination' => $paginator,
            'start' => $start,
        );
        $this->viewFrontContent('frontend/template/news', $data);
    }

    public function news_detail() {
        $url = $this->uri->segment(1);
        $row = $this->db->get_where('cms', array('post_url' => $url, 'status' => 'Publish'))->row();

        $search = array('&lt;', '&gt;');
        $replace = array('<', '>');
        $content = str_replace($search, $replace, $row->content);

        $data = array(
            'id' => $row->id,
            'user_id' => $row->user_id,
            'parent_id' => $row->parent_id,
            'post_type' => $row->post_type,
            'post_title' => $row->post_title,
            'post_url' => $row->post_url,
            'content' => $content,
            'seo_title' => $row->seo_title,
            'seo_keyword' => $row->seo_keyword,
            'seo_description' => $row->seo_description,
            'thumb' => $row->thumb,
            'created' => $row->created,
            'status' => $row->status,
            'edit_url' => site_url(Backend_URL.'cms/update_post/'.$row->id),
        );
        $this->viewFrontContent('frontend/template/news-single', $data);
    }

    public function revision() {
        $current_id = $this->uri->segment(2);
        $past_id = $this->uri->segment(3);

        $data['current_data'] = $this->db->get_where('cms', ['id' => $current_id])->row();
        $data['past_data'] = $this->db->get_where('cms', ['id' => $past_id])->row();

        $this->viewFrontContent('frontend/revision', $data);
    }

}