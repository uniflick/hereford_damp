<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Paypal extends Frontend_controller {

    public function __construct() {
        parent::__construct();
    }
    

    public function subscr_ipn(){
        
        $post     = json_encode($this->input->post());
        $get      = json_encode($this->input->get());
        $log_path = APPPATH . '/logs/paypal.txt';        
        $user_id  = (int) $this->input->post('custom');
        $mail_log = date('Y-m-d H:i:s A') . " UserID {$user_id} | PayPal \r\n{$post}\r\n GET: {$get}\r\n ";
        file_put_contents( $log_path, $mail_log, FILE_APPEND);        
        
        $data = [
            'user_id'       => $user_id,
            'timestamp'     => date('Y-m-d H:i:s'),
            'txn_type'      => $this->input->post('txn_type'),
            'subscr_id'     => $this->input->post('subscr_id'),
            'item_name'     => $this->input->post('item_name'),
            'status'        => $this->input->post('payment_status'),
            'json_paypal'   => $post,            
            'mc_gross'      => $this->input->post('mc_gross')            
        ];
        
        file_put_contents( $log_path, json_encode($data), FILE_APPEND);        
        

//        $this->db->insert('user_subscriptions',$data);        
//        $this->db->set('verified', 'Yes')->where('id', $user_id )->update('users');                        
//
//        
//        $this->db->select('email,first_name,last_name,company_name');
//        $this->db->where('id', $user_id );        
//        $user = $this->db->get('users')->row();        
//        
//        
//        
//        $user_data['name']      = "{$user->first_name} {$user->last_name}";
//        $user_data['company']   = $user->company_name;
//        $user_data['email']     = $user->email;        
//        $user_data['user_id']   = $user_id;
//        Modules::run('mail/paypal_subscr_notify', $user_data);
    }   
}