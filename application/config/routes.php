<?php defined('BASEPATH') or exit('No direct script access allowed');

$route['default_controller']    = 'frontend';
$route['admin']                 = 'dashboard';
$route['admin/login']           = 'auth/login_form';
$route['admin/logout']          = 'auth/logout';
$route['404_override']          = 'frontend';

$route['sign-in']           = 'frontend/signin';
$route['reset-password']    = 'frontend/reset_pass';

$route['search']           = 'frontend/search';
$route['news-events']      = 'frontend/news';
$route['testimonial']      = 'frontend/testimonial';

//$route['photo-gallery']        = 'frontend/gallery';
//$route['photo-gallery/(:any)'] = 'frontend/gallery_detail/$1';

$route['revision/(:num)/(:num)'] = 'frontend/revision/$2/$1';

$route['upcoming-events']        = 'frontend/event';
$route['upcoming-events/(:any)'] = 'frontend/event_detail/$1';
$route['translate_uri_dashes']   = FALSE;

define('ModuleRoutePrefix', APPPATH . '/modules/');
define('ModuleRouteSuffix', '/config/routes.php');

require_once(ModuleRoutePrefix . 'settings' . ModuleRouteSuffix);
require_once(ModuleRoutePrefix . 'users' . ModuleRouteSuffix);
require_once(ModuleRoutePrefix . 'profile' . ModuleRouteSuffix);
require_once(ModuleRoutePrefix . 'module' . ModuleRouteSuffix);
require_once(ModuleRoutePrefix . 'db_sync' . ModuleRouteSuffix);
require_once(ModuleRoutePrefix . 'cms' . ModuleRouteSuffix);
require_once(ModuleRoutePrefix . 'slider' . ModuleRouteSuffix);
require_once(ModuleRoutePrefix . 'email_templates' . ModuleRouteSuffix);
require_once(ModuleRoutePrefix . 'countries' . ModuleRouteSuffix);
require_once(ModuleRoutePrefix . 'mailbox' . ModuleRouteSuffix);
require_once(ModuleRoutePrefix . 'gallery' . ModuleRouteSuffix);
require_once(ModuleRoutePrefix . 'newsletter_subscriber' . ModuleRouteSuffix);
require_once(ModuleRoutePrefix . 'event' . ModuleRouteSuffix);
require_once(ModuleRoutePrefix . 'testimonial' . ModuleRouteSuffix);
