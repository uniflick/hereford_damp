$(function () {
    
    var date = new Date();
    var m = date.getMonth(), d = date.getDate(), y = date.getFullYear();
    $(".next_date_only").datepicker({
        dateFormat: 'yy-mm-dd',
        showOn: 'both',
        showButtonPanel: true,
        buttonImage: '',
        buttonImageOnly: true,
        changeMonth: true,
        changeYear: true,
        closeText: 'Close',
        constrainInput: true,
        minDate: new Date(y, m, d),
        //beforeShowDay: $.datepicker.noWeekends
    });


    $(".date_picker").datepicker({
        dateFormat: 'yy-mm-dd',
        showOn: 'both',
        showButtonPanel: true,
        buttonImage: '',
        buttonImageOnly: true,
        changeMonth: true,
        changeYear: true,
        closeText: 'Close',
        constrainInput: true,
        //  beforeShowDay: $.datepicker.noWeekends
    });

    $.datepicker._gotoToday = function (id) {
        $(id).datepicker('setDate', new Date()).datepicker('hide').blur();
    };

});


