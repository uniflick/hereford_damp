$('#signin').on('click', function(e){       
    e.preventDefault();    
    var credential = $('#credential').serialize();
    $.ajax({
        url: 'auth/login',
        type: "POST",
        dataType: "json",
        cache: false,
        data: credential,
        beforeSend: function(){
            $('#respond').html('<p class="ajax_processing">Please Wait... Checking....</p>');
        },
        success: function( jsonData ){
            if(jsonData.Status === 'OK'){
                $('#respond').html( jsonData.Msg );                
                window.location.href = 'admin';
            } else {
                $('#respond').html( jsonData.Msg );
            }                                    
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) { 
            $('#respond').html( '<p> XML: '+ XMLHttpRequest + '</p>' );
            $('#respond').append( '<p>  Status: '+textStatus + '</p>' );
            $('#respond').append( '<p> Error: '+ errorThrown + '</p>' );            
        }  
    });        
});


$('.js_forgot').on('click', function(){
    $('.js_login').slideUp('slow');
    $('.js_forget_password').slideDown('slow');
});

$('.js_back_login').on('click', function(){
    $('.js_forget_password').slideUp('slow');
    $('.js_login').slideDown('slow');
});

$('#forgot_pass').click(function(){
    var formData = $('#forgotForm').serialize();    
    $.ajax({
        url: 'auth/forgot_pass',
        type: "POST",
        dataType: 'json',
        data: formData,
        beforeSend: function () {
            $('.formresponse')
                    .html('<p class="ajax_processing">Checking user...</p>')
                    .css('display','block');
        },
        success: function ( jsonRespond ) {
            if( jsonRespond.Status === 'OK'){
                $('.formresponse').html( jsonRespond.Msg );                
            } else {
                $('.formresponse').html( jsonRespond.Msg );
            }                
        }
    });
    return false;
});